# Age differences in functional network architecture in semantic cognition

This is the project for network architecture analyses in semantic cognition in young and older adults. It contains analysis scripts and raw data for statistical analyses.

## Content
* ICA  
Contains scripts for all analysis steps around group spatial independent component analysis such as first level in SPM12, one-sided t-tests for ICA results, saving significant clusters, running Jaccard spatial similarity analysis, and creating regions of interest (ROI) for subsequent analyses.

* Denoising & Parcellation  
Contains scripts for extraction of time series from preprocessed functional data for (1) whole networks and (2) ROIs.

* cPPI  
Contains scripts for correlational psychophysiological interaction analysis. First, a cell with all relevant time series is created, then cPPI_master is run, and then results can be saved for statistical analyses in R.

* Graph Measures  
Contains scripts for two graph measures that were calculated in Matlab: orthogonal minimum spanning tree (OMST) and normalised participation coefficient. Results of these were then saved and analysed statistically in R.

* Statistical Models  
Contains scripts and raw data for all statistical models.
