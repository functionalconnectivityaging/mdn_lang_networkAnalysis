#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 16 2021

@author: martin
"""

import numpy as np
import pandas as pd
import os
#import json
import re
import glob
import time
import nibabel as nib

from nilearn import image
#from nilearn import plotting
from nilearn.input_data import NiftiLabelsMasker # to parcellate given a template atlas
#from nilearn.input_data import NiftiSpheresMasker # to extract signal from given coordinates
#from nilearn.image import load_img, math_img

# function for natural sorting of acompcor components
def natural_key(string_):
    """See https://blog.codinghorror.com/sorting-for-humans-natural-sort-order/"""
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', string_)]  


base_dir_gt = '/data/pt_02004/MDN_LANG/graph_theory/subjects'
base_dir_fmriprep = '/data/pt_02004/MDN_LANG/fmriprep_20.2.3/output'
base_dir_ICA = '/data/pt_02004/MDN_LANG/graph_theory/group/ICAcomponents'

''' OLDER ADULTS '''
subjects = ['011',
       '012','013','014','015','016','017','019','020',
       '021','022','023','025','027','028','029','030','031',
       'control002','control003','control004','control005','control006','control007','control008',
            'control009','control010','control011','control012','control013','control014','control015','control016',
            'control017','control018','control019','control021','control022','control023','control024',
            'control025','control026','control027','control028','control029','control030']
#'002','003','005','006','007','009',
#subjects = [ '008','018', 'control020'] # '008','018' '004', '024'

''' YOUNG ADULTS '''
#subjects = ['control002','control003','control004','control005','control006','control007','control008',
#            'control009','control010','control011','control012','control013','control014','control015','control016',
#            'control017','control018','control019','control021','control022','control023','control024',
#            'control025','control026','control027','control028','control029','control030']
#subjects = ['control001'] 'control020'


runs = ['1', '2']

#components = ['06', '09', '13', '16', '18', '45', '52']
components = ['13']


nifti_fn = 'sub-{0}_task-SWG_run-{1}_echo-2_space-MNI152NLin6Asym_desc-preproc_bold_trimmed.nii'
gm_fn = 'sub-{0}_space-MNI152NLin6Asym_label-GM_probseg.nii.gz'
gm_bin_fn = 'sub-{0}_space-MNI152NLin6Asym_label-GM_thr0.2_bin.nii.gz'
timeSeries_fn = 'sub-{0}_run-{1}_gm_dTS.txt'
timeSeries_IC_fn = 'IC{0}_sub-{1}_run-{2}_gm_dTS.txt'
subject = 'sub-{0}'
confounds = 'sub-{0}_task-SWG_run-{1}_desc-confounds_timeseries.tsv'
json_file = 'sub-{0}_task-SWG_run-{1}_desc-confounds_timeseries.json'
final_confounds = 'sub-{0}_task-SWG_run-{1}_extracted_confounds_08.2021.csv'
IC_folder = 'IC{0}'
#peaks_IC_fn = 'peak_coords_IC{0}_maskedInclWith_*.txt'

timestr = time.strftime('%Y%m%d')

#atlas_filename = '/data/pt_02004/MDN_LANG/graph_theory/atlases/Wustl_300_ROI_Set/ROIs_300inVol_MNI.nii'
#atlas_filename = '/data/pt_02004/MDN_LANG/graph_theory/atlases/Schaefer2018/Schaefer2018_200Parcels_7Networks_order_FSLMNI152_1mm.nii.gz'
#atlas_filename = '/data/pt_02004/MDN_LANG/graph_theory/atlases/Schaefer2018/Schaefer2018_300Parcels_7Networks_order_FSLMNI152_1mm.nii.gz'
#atlas_filename = '/data/pt_02004/MDN_LANG/graph_theory/atlases/Wustl_300_ROI_Set/ROIs_300inVol_MNI.nii'
#coords_wustl = 'sub-{0}_wustl300_subjectSpace.txt'

#print('Atlas ROIs are located in nifti image (4D) at: %s' %
#      atlas_filename)  # 4D data


# creating/opening a logfile
f = open(os.path.join(base_dir_ICA, "error_" + timestr + ".log"), "a")

total_count = pd.DataFrame()

for sub in subjects:
    for run in runs:
        
        print('This is participant %s and run %s' %(sub, run))        
        
## Read confounds file and create df with relevant confounds. Currently these confounds are included:
## 24 realignment parameters (6 RP + temporal derivatives + squared terms), global signal, first 5 aCompCor components for WM and CSF, respectively (all recommended by Mascali et al., 2021), FD censored volumes (FD > 0.9), cosine regressors
## Also, the first 29 rows are cut here (because they were acquired for dual-echo series)
        
#        # Read confound file for second echo for each participant and cut first 29 rows from df
#        confounds_dir = os.path.join(base_dir_fmriprep, subject.format(sub), 'fmriprep', subject.format(sub), 'func')
#        confound_file = pd.read_csv(os.path.join(confounds_dir, confounds.format(sub, run)), sep = '\t', header = 0)
#        confound_file = confound_file[29:]
#        
#                
###----------------- Extract confound components ------------------------------
#    
#        # 24 realignment parameters        
#        motion_conf = ['trans_x', 'trans_x_derivative1', 'trans_x_derivative1_power2', 'trans_x_power2', 'trans_y', 'trans_y_derivative1', 'trans_y_derivative1_power2', 'trans_y_power2', 'trans_z', 'trans_z_derivative1', 'trans_z_power2', 'trans_z_derivative1_power2', 'rot_x', 'rot_x_derivative1', 'rot_x_power2', 'rot_x_derivative1_power2', 'rot_y', 'rot_y_derivative1', 'rot_y_derivative1_power2', 'rot_y_power2', 'rot_z', 'rot_z_derivative1', 'rot_z_power2', 'rot_z_derivative1_power2']
#    
#    
#        # Global signal
#        gs_conf = ['global_signal']    
#        
#    
#        # aCompCor (first 5 components for CSF and WM each)    
#        with open(os.path.join(confounds_dir,json_file.format(sub, run)), 'r') as f:
#    
#            data = json.load(f)
#        
#            comp_WM = []
#            comp_CSF = []
#            n_comps_total, n_comps_dropped, n_comps_retained = 0, 0, 0
#            
#            
#            for key, dat in data.items():
#                try:
#                    if dat['Method'] == 'aCompCor' and dat['Mask'] == 'CSF':
#                        n_comps_total += 1
#                        
#                        if dat['Retained']:
#                            comp_CSF.append(key)
#                            n_comps_retained +=1
#                        else:
#                            n_comps_dropped +=1
#                    elif dat['Method'] == 'aCompCor' and dat['Mask'] == 'WM':
#                        n_comps_total += 1
#                        
#                        if dat['Retained']:
#                            comp_WM.append(key)
#                            n_comps_retained +=1
#                        else:
#                            n_comps_dropped +=1
#        
#                except KeyError:
#            # just ignore the items that don't have 'Method' or Mask 'key'
#                    pass    
#            
#            acompcor_WM_sorted = sorted(comp_WM, key = natural_key)
#            acompcor_CSF_sorted = sorted(comp_CSF, key = natural_key)   
#            acompcor_WM = acompcor_WM_sorted[:5]
#            acompcor_CSF = acompcor_CSF_sorted[:5]
#            
#            
#        # cosine regressors (instead of high-pass filtering, recommended by fMRIPrep)
#        cosine_conf = [col for col in confound_file if col.startswith('cosine')]
#        
#    
#        # framewise displacement
#        fd = confound_file[confound_file.framewise_displacement >= 0.9] # current threshold is 0.9
#        
#        if not fd.empty:
#        #fd_values = fd[['framewise_displacement']]
#        
#            # extract indices of rows/volumes with FD greater than threshold
#            fd_idx = fd.index.values
#            
#            # create array with amount of zero columns according to number of FD indices
#            for i in range(len(fd_idx)):
#                if sub == '002' and run == '1':
#                    arr = np.zeros((583, i+1), dtype = int)
#                elif (sub == '002' and run == '2') or (sub == '003' and run == '1'):
#                    arr = np.zeros((585, i+1), dtype = int)
#                else:
#                    arr = np.zeros((587, i+1), dtype = int)
#                
#            # loop over zero columns in array arr and fill cells with a 1 according to index in fd_idx (-29 since we cut the first 29 volumes)        
#            for col in range(arr.shape[1]):
#                arr[fd_idx[col]-29,[col]] = 1
#                
#            # make df from this array so we can concatenate it with other df for confounds    
#            fd_df = pd.DataFrame(arr)    
#               
#        
#            # subset confounds df to extract relevant confounds for previously created lists
#            confounds_final = confound_file[motion_conf + gs_conf + acompcor_CSF + acompcor_WM + cosine_conf]
#            
#            # reset indices for both df since they can't be merged properly otherwise
#            confounds_final.reset_index(drop=True, inplace=True)
#            fd_df.reset_index(drop=True, inplace=True)
#            
#            # concatenate confound df with df for fd
#            confounds_df = pd.concat([confounds_final, fd_df], axis=1)
#            
#        else:
#            # subset confounds df to extract relevant confounds for previously created lists
#            confounds_final = confound_file[motion_conf + gs_conf + acompcor_CSF + acompcor_WM + cosine_conf]
#            
#            # just rename df since there is no merge with FD regressors
#            confounds_df = confounds_final
#            
#        # save df to file for later checks
#        # make output dir 
#        outdir_confounds = os.path.join(base_dir_gt, subject.format(sub), 'func')
#        if not os.path.exists(outdir_confounds):
#            os.makedirs(outdir_confounds)
#        
#        output_confounds_fn = os.path.join(outdir_confounds, final_confounds.format(sub, run))
#        confounds_df.to_csv(output_confounds_fn, index = False)
    
        # load pre-configured confounds df
        confounds_df = pd.read_csv(os.path.join(base_dir_gt, subject.format(sub), 'func', final_confounds.format(sub, run)), sep = ',', header = 0, engine = 'python')

##----------------- Parcellation with confound removal ------------------------------
        
        # load nifti file    
        nifti = os.path.join(base_dir_fmriprep, subject.format(sub), 'fmriprep', subject.format(sub), 'func', nifti_fn.format(sub, run))
        
        # load gm mask
#        gm = os.path.join(base_dir_fmriprep, subject.format(sub), 'fmriprep', subject.format(sub), 'anat', gm_fn.format(sub))
#        gm_img = load_img(gm)
#        
#        # binarize gm mask, current threshold: 0.3
#        gm_mask = math_img('img > 0.3', img=gm_img)
#        
#        # save binarized gm mask in graph theory folder      
#        output_folder_gm = os.path.join(base_dir_gt, subject.format(sub), 'anat')
#        if not os.path.exists(output_folder_gm):
#            os.makedirs(output_folder_gm)
#        gm_mask.to_filename(os.path.join(output_folder_gm, gm_bin_fn.format(sub)))

        # load binarized gm mask
        gm_mask = os.path.join(base_dir_gt, subject.format(sub), 'anat', gm_bin_fn.format(sub))
        
        # apply GM mask to functional image
        first_func_img = image.index_img(nifti, 0)
        resampledGM_mask = image.resample_to_img(gm_mask, first_func_img, interpolation = 'nearest')
#        plotting.plot_roi(resampledGM_mask, first_func_img)

        # denoise the data using the confound df as well as detrending  
        # first, transform confounds df into a matrix
        confounds_matrix = confounds_df.to_numpy()
        # second, start denoising -UPDATE: denoising is now done while extracting signal from spheres (see masker.fit_transform)
        #denoised_data = image.clean_img(nifti, confounds=confounds_matrix, ensure_finite = True, detrend = True, standardize = True, mask_img = resampledGM_mask)
       
        
        ### loop over ICA components
        for IC in components:
        
            print('This is IC %s' %(IC))    
            
            # read txt file with MNI coordinates for ROIs/spheres for signal extraction
            #coords_ICA = pd.read_csv(os.path.join(base_dir_ICA, IC_folder.format(IC), 'cerebrumMasked', peaks_IC_fn.format(IC)), sep = ',', skiprows = 1, header = None, engine = 'python')
            
            # Alternative way for reading in file when components are masked with different templates
#            path = os.path.join(base_dir_ICA, IC_folder.format(IC), 'cerebrumMasked')
#            fn = glob.glob(path + '/peak+subpeak_coords_IC%s_maskedInclWith_*.txt' %IC)
#            coords_ICA = pd.read_csv(fn[0], sep = ',', skiprows = 1, header = None, engine = 'python')            
#            coords_ICA_triplets = coords_ICA.values.tolist()
            
            # Alternative: Use predefined ROIs (e.g. via Marsbar) and read them as atlas file. Here, the ROIs were created with spheres of 5mm and then masked by the respective cluster to only contain active cluster voxels.
            atlas_path = os.path.join(base_dir_ICA, IC_folder.format(IC), 'cerebrumMasked/ROIs')
            atlas_fn = glob.glob(atlas_path + '/IC%s_*ROIs.nii' %IC)
                        
            # mask functional data with given atlas and parcellate OR create spheres around given coordinates and extract signal
            # masker = NiftiLabelsMasker(labels_img = atlas_filename, smoothing_fwhm = None, resampling_target = 'data', verbose=5) # this is for a parcellation atlas
            #masker = NiftiSpheresMasker(coords_ICA_triplets, radius = 5, mask_img = resampledGM_mask, detrend = True, standardize = True, verbose = 2) # this is for spheres based on coordinates
            masker = NiftiLabelsMasker(labels_img = atlas_fn[0], mask_img = resampledGM_mask, detrend = True, standardize = True, 
                                       resampling_target = 'data', verbose = 0)
                        
            try:
                time_series_raw = masker.fit_transform(nifti, confounds=confounds_matrix)
                                
#                # check if there are columns in the array that are only zeros meaning that there was no signal in this ROI
#                # If true, delete these columns from array
#                try:
#                    col = np.where(~time_series_raw.any(axis=0))[0]
#                    time_series_raw = np.delete(time_series_raw, col, axis = 1)
#                except:
#                    pass

                # prepare output folders   
                output_folder = os.path.join(base_dir_gt, subject.format(sub), 'func', 'parcellated', 'ICA_masked_Marsbar_121ROIs')
                if not os.path.exists(output_folder):
                    os.makedirs(output_folder)
    
                # save timeseries of components in file
                timeSeries = os.path.join(output_folder, timeSeries_IC_fn.format(IC, sub, run))
                np.savetxt(timeSeries, time_series_raw, fmt='%.4f')
                
                
                # Write ratio and number of voxels per ROI to table                
                atlas_img = nib.load(atlas_fn[0])
                roi_indices, roi_counts = np.unique(atlas_img.get_fdata(), return_counts=True)
                
                # transform to atlas
                masker_count = NiftiLabelsMasker(atlas_img, resampling_target='data')
                voxel_ratios = masker_count.fit_transform(resampledGM_mask)
                
                # multiple by the number of voxels in each ROI
                n_voxels = voxel_ratios[0,:] * roi_counts[1:]
                #voxels.append((sub, run, IC, voxel_ratios, n_voxels))

                voxels = pd.DataFrame()
                for r in range(len(roi_indices[1:])):
                    voxels.loc[r, ['sub']] = sub
                    voxels.loc[r, ['run']] = run
                    voxels.loc[r, ['IC']] = IC
                    voxels.loc[r, ['voxel ratio']] = voxel_ratios[0, r]
                    voxels.loc[r, ['n voxels']] = n_voxels[r]

                total_count = total_count.append(voxels, ignore_index = False)

            except Exception as ex:
                #import logging
                print(ex)
                
                # writing in the file
                f.write(sub + ' ' + run + ' ' + IC + ' ' + str(ex) + '\n')
               
            finally:
                pass
            

# closing the file
f.close()            
            
# write df to file
total_count.to_csv(os.path.join(base_dir_ICA, 'N_voxels_allComponents_allSub_' + timestr + '.txt'))

### Example for plotting timeseries
## colors for each of the clusters
#colors = ['blue', 'navy', 'purple', 'magenta', 'olive', 'teal', 'red', 'green', 'yellow']
## plot the time series and corresponding locations
#fig1, axs1 = plt.subplots(1, 9)
#axs1[0].plot(time_series_raw[:, 0], lw =1, c=colors[0])
#axs1[1].plot(time_series_raw[:, 1], lw =1, c=colors[1])
#axs1[2].plot(time_series_raw[:, 2], lw =1, c=colors[2])
#axs1[3].plot(time_series_raw[:, 3], lw =1, c=colors[3])
#axs1[4].plot(time_series_raw[:, 4], lw =1, c=colors[4])
#axs1[5].plot(time_series_raw[:, 5], lw =1, c=colors[5])
#axs1[6].plot(time_series_raw[:, 6], lw =1, c=colors[6])
#axs1[7].plot(time_series_raw[:, 7], lw =1, c=colors[7])
#axs1[8].plot(time_series_raw[:, 8], lw =1, c=colors[8])


## High-pass filter or cosine regressors?        
# high-pass filter at 0.008 Hz
#from fmriprep documentation:
#    Physiological and instrumental (scanner) noise sources are generally present in fMRI data, typically taking the form of low-frequency signal drifts. 
#    To account for these drifts, temporal high-pass filtering is the immediate option.
#    Alternatively, low-frequency regressors can be included in the statistical model to account for these confounding signals.
#    If your analysis includes separate high-pass filtering, do not include cosine_XX regressors in your design matrix.
