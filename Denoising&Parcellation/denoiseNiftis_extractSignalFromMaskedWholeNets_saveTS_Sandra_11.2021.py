#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 21:02:30 2021

@author: martin
"""

import numpy as np
import pandas as pd
import os
#import json
import re
#import glob
import time
#import nibabel as nib

from nilearn import image
#from nilearn import plotting
from nilearn.input_data import NiftiMapsMasker
#from nilearn.input_data import NiftiLabelsMasker # to parcellate given a template atlas
#from nilearn.input_data import NiftiSpheresMasker # to extract signal from given coordinates
#from nilearn.image import load_img, math_img

# function for natural sorting of acompcor components
def natural_key(string_):
    """See https://blog.codinghorror.com/sorting-for-humans-natural-sort-order/"""
    return [int(s) if s.isdigit() else s for s in re.split(r'(\d+)', string_)]  


base_dir_gt = '/data/pt_02004/MDN_LANG/graph_theory/subjects'
base_dir_fmriprep = '/data/pt_02004/MDN_LANG/fmriprep_20.2.3/output'
base_dir_ICA = '/data/pt_02004/MDN_LANG/graph_theory/group/ICAcomponents'

''' OLDER ADULTS '''
#subjects = ['002','003','005','006','007','009','011',
#       '012','013','014','015','016','017','019','020',
#       '021','022','023','025','027','028','029','030','031',
#       'control002','control003','control004','control005','control006','control007','control008',
#            'control009','control010','control011','control012','control013','control014','control015','control016',
#            'control017','control018','control019','control021','control022','control023','control024',
#            'control025','control026','control027','control028','control029','control030']

subjects = [ '004','024', 'control001'] # '008','018' '004', '024'

''' YOUNG ADULTS '''
#subjects = ['control002','control003','control004','control005','control006','control007','control008',
#            'control009','control010','control011','control012','control013','control014','control015','control016',
#            'control017','control018','control019','control021','control022','control023','control024',
#            'control025','control026','control027','control028','control029','control030']
#subjects = ['control001'] 'control020'


runs = ['1']

components = ['06', '09', '13', '16', '18', '45', '52']


nifti_fn = 'sub-{0}_task-SWG_run-{1}_echo-2_space-MNI152NLin6Asym_desc-preproc_bold_trimmed.nii'
gm_fn = 'sub-{0}_space-MNI152NLin6Asym_label-GM_probseg.nii.gz'
gm_bin_fn = 'sub-{0}_space-MNI152NLin6Asym_label-GM_thr0.2_bin.nii.gz'
timeSeries_fn = 'sub-{0}_run-{1}_gm_dTS.txt'
timeSeries_IC_fn = 'sub-{0}_run-{1}_gm_dTS.txt'
subject = 'sub-{0}'
confounds = 'sub-{0}_task-SWG_run-{1}_desc-confounds_timeseries.tsv'
json_file = 'sub-{0}_task-SWG_run-{1}_desc-confounds_timeseries.json'
final_confounds = 'sub-{0}_task-SWG_run-{1}_extracted_confounds_08.2021.csv'
IC_folder = 'IC{0}'
IC_mask = 'IC{0}_FWE05_k10.nii'
timestr = time.strftime('%Y%m%d')

# creating/opening a logfile
f = open(os.path.join(base_dir_ICA, "error_" + timestr + "_wholeNets.log"), "a")


###----------------- Create list of IC maps for NiftiMapsMasker ----------------########
maps = []
for IC in components:
    mask = os.path.join(base_dir_ICA, IC_folder.format(IC), 'cerebrumMasked', IC_mask.format(IC))
    maps.append(mask)



###----------------- Loop over subjects with NiftiMapsMasker ----------------########
for sub in subjects:
    for run in runs:
        
        print('This is participant %s and run %s' %(sub, run))        
        
        # load pre-configured confounds df
        confounds_df = pd.read_csv(os.path.join(base_dir_gt, subject.format(sub), 'func', final_confounds.format(sub, run)), sep = ',', header = 0, engine = 'python')
#        confounds_df = confounds_df.drop('global_signal', 1)

##----------------- Parcellation with confound removal ------------------------------
        
        # load nifti file    
        nifti = os.path.join(base_dir_fmriprep, subject.format(sub), 'fmriprep', subject.format(sub), 'func', nifti_fn.format(sub, run))       
        
        # load binarized gm mask
        gm_mask = os.path.join(base_dir_gt, subject.format(sub), 'anat', gm_bin_fn.format(sub))
        
        # apply GM mask to functional image
        first_func_img = image.index_img(nifti, 0)
        resampledGM_mask = image.resample_to_img(gm_mask, first_func_img, interpolation = 'nearest')
#        plotting.plot_roi(resampledGM_mask, first_func_img)

        # denoise the data using the confound df as well as detrending  
        # first, transform confounds df into a matrix
        confounds_matrix = confounds_df.to_numpy()
        

        masker = NiftiMapsMasker(maps_img = maps, mask_img = resampledGM_mask, detrend= True, standardize = True, resampling_target = 'data')

        try:
            time_series_raw = masker.fit_transform(nifti, confounds=confounds_matrix)
                            
            # check if there are columns in the array that are only zeros meaning that there was no signal in this ROI
            # If true, delete these columns from array
            try:
                col = np.where(~time_series_raw.any(axis=0))[0]
                time_series_raw = np.delete(time_series_raw, col, axis = 1)
            except:
                pass

            # prepare output folders   
            output_folder = os.path.join(base_dir_gt, subject.format(sub), 'func', 'parcellated', 'ICA_masked_wholeNetworks_7ROIs_NO_GSR')
            if not os.path.exists(output_folder):
                os.makedirs(output_folder)

            # save timeseries of components in file
            timeSeries = os.path.join(output_folder, timeSeries_IC_fn.format(sub, run))
            np.savetxt(timeSeries, time_series_raw, fmt='%.4f')
            
            
#            # Write ratio and number of voxels per ROI to table                
#            atlas_img = nib.load(atlas_fn[0])
#            roi_indices, roi_counts = np.unique(atlas_img.get_fdata(), return_counts=True)
#            
#            # transform to atlas
#            masker_count = NiftiLabelsMasker(atlas_img, resampling_target='data')
#            voxel_ratios = masker_count.fit_transform(resampledGM_mask)
#            
#            # multiple by the number of voxels in each ROI
#            n_voxels = voxel_ratios[0,:] * roi_counts[1:]
#            #voxels.append((sub, run, IC, voxel_ratios, n_voxels))
#
#            voxels = pd.DataFrame()
#            for r in range(len(roi_indices[1:])):
#                voxels.loc[r, ['sub']] = sub
#                voxels.loc[r, ['run']] = run
#                voxels.loc[r, ['IC']] = IC
#                voxels.loc[r, ['voxel ratio']] = voxel_ratios[0, r]
#                voxels.loc[r, ['n voxels']] = n_voxels[r]
#
#            total_count = total_count.append(voxels, ignore_index = False)

        except Exception as ex:
            #import logging
            print(ex)
            
            # writing in the file
            f.write(sub + ' ' + run + ' ' + IC + ' ' + str(ex) + '\n')
           
        finally:
            pass
            

# closing the file
f.close()            
            
# write df to file
#total_count.to_csv(os.path.join(base_dir_ICA, 'N_voxels_allComponents_allSub3.txt'))

