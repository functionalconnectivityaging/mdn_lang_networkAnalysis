#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 23 14:26:59 2021

@author: martin
"""

import pandas as pd
import os
from statistics import mean

base_dir_gt = '/data/pt_02004/MDN_LANG/graph_theory/subjects'
base_dir_fmriprep = '/data/pt_02004/MDN_LANG/fmriprep_20.2.3/output'
base_dir_ICA = '/data/pt_02004/MDN_LANG/graph_theory/group/ICAcomponents'
output_dir = '/data/pt_02004/MDN_LANG/graph_theory/subjects/'

''' OLDER ADULTS '''
subjects = ['002','003','005','006','007','009','011',
       '012','013','014','015','016','017','019','020',
       '021','022','023','025','027','028','029','030','031',
       'control002','control003','control004','control005','control006','control007','control008',
            'control009','control010','control011','control012','control013','control014','control015','control016',
            'control017','control018','control019','control021','control022','control023','control024',
            'control025','control026','control027','control028','control029','control030']

#subjects = [ '004','024', 'control001'] # '008','018' '004', '024'

runs = ['1','2']

subject = 'sub-{0}'
confounds = 'sub-{0}_task-SWG_run-{1}_desc-confounds_timeseries.tsv'
output_fn = 'sub-{0}_task-SWG_run-{1}_conf_RMSD.csv'

#res = []

for sub in subjects:
    for run in runs:
        
        print('This is participant %s and run %s' %(sub, run))        
#        # Read confound file for second echo for each participant and cut first 29 rows from df
        confounds_dir = os.path.join(base_dir_fmriprep, subject.format(sub), 'fmriprep', subject.format(sub), 'func')
        confound_file = pd.read_csv(os.path.join(confounds_dir, confounds.format(sub, run)), sep = '\t', header = 0)
        confound_file = confound_file[29:]

#        fd = confound_file.framewise_displacement
#        mean_fd = mean(fd)
        
        rmsd = confound_file.rmsd
#        mean_rmsd = mean(rmsd)
    
#        res.append((sub,
#                    mean_fd, mean_rmsd))
#    
#cols = ['sub','meanFD', 'meanRMSD']
#result = pd.DataFrame(res, columns = cols)    
#results = result.groupby(['sub']).mean()
#
#
#results.to_csv('/data/pt_02004/MDN_LANG/meanFD_meanRMSD.txt', index = True)
        outdir_confounds = os.path.join(output_dir, subject.format(sub), 'func')
        output_confounds_fn = os.path.join(outdir_confounds, output_fn.format(sub, run))
        rmsd.to_csv(output_confounds_fn, index = False)
