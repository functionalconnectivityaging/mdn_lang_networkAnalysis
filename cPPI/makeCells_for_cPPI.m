% This script makes the below-described mat-files for input into cPPI_master.m in cPPI toolbox: roi timeseries, (confound) task regressors, or confound timeseries, depending on what's not commented out

%  Path and name of .mat file containing the ROI time courses. The time 
%  courses should be stored as a cell structure containing N cells, where 
%  N = number of subjects. Each cell must contain an L x M matrix, where L 
%  is the total number of time points (concenated across sessions) and M is 
%  the number of ROIs. So, for example, if there are 5 sessions each with 
%  200 volumes, L = 1000. The cell structure should be called roi_ts. 
%  If not, simply uncomment the line underneath the load command and change 
%  the right hand side of the '=' to refer to the name you have assigned to 
%  the cell structure.
%load /path/to/roi/time/series/my_roi_timeseries.mat
%roi_ts = timecourse_cell ;  % rename input here if required

clear

%% Paths
% define main folder
subjects_folder = '/data/pt_02004/MDN_LANG/graph_theory/subjects';
group_folder = '/data/pt_02004/MDN_LANG/graph_theory/group/cPPI';

sub = {'002','003','004','005','006','007','008','009','011', ...
          '012','013','014','015','016','017','018','019','020', ...
          '021','022','023','024','025','027','028','029','030','031', ...
          'control001','control002','control003','control004','control005','control006','control007','control008', ...
            'control009','control010','control011','control012','control013','control014','control015','control016', ...
            'control017','control018','control019','control020','control021','control022','control023','control024', ...
            'control025','control026','control027','control028','control029','control030'};
      
parcellation_atlas = 'ICA_masked_wholeNetworks_7ROIs_NO_GSR';
%parcellation_atlas = 'ICA_masked_Marsbar_121ROIs'; 
components = {'06', '09', '13', '16', '18', '45', '52'};

%% Create cell with functional timeseries of selected ROIs/parcels per subject
% roi_ts={}; %create empty cell for timeseries of all subjects
% for isub = 1:numel(sub) % loop over subjects
%     all_ICs = table(); % for ICA: first put all components in one table, then append this to cell
% 
%     %allSes_confTCs = [];
%     current_sub = sub{isub};
%     if strcmp(current_sub, '004') | strcmp(current_sub, '024') | strcmp(current_sub, 'control001') % define subjects that only have one task run
%         run = {'1'};
%     elseif strcmp(current_sub, '008') | strcmp(current_sub, '018') | strcmp(current_sub, 'control020')
%         run = {'2'};
%     else
%         run = {'1', '2'};
%     end
%     for IC = 1:length(components) % loop over components
%         %% Display which participant and IC is currently processed
%         X = ['This is participant ' sub{isub} ' and IC ' components{IC}];
%         disp(X);
%         allSes_confTCs = [];
%         for irun = 1:numel(run) % loop over runs
%             % load txt file with timeseries for ROIs
%             roi_file = dir([subjects_folder filesep 'sub-' sub{isub} filesep 'func' filesep 'parcellated' filesep ...
%                 parcellation_atlas filesep 'IC' components{IC} '_sub-' sub{isub} '_run-' run{irun} '_gm_dTS.txt']);
%             roi_fn = [roi_file.folder filesep roi_file.name];
%             roi_table = readtable(roi_fn, delimiter = 'space');
%             colnames = ['IC' components{IC} '_'] + string(1:size(roi_table, 2)); % add column names with component specified
%             roi_table.Properties.VariableNames = colnames;
%             % concatenate timeseries for both runs in one table
%             allSes_confTCs = [allSes_confTCs; roi_table];
%         end
%         allSes_confTCs.Key = (1:height(allSes_confTCs))'; %create key column for join function
%         all_ICs.Key = (1:height(allSes_confTCs))';
%         all_ICs = join(all_ICs, allSes_confTCs, 'Keys', 'Key'); % join table with all components for one subject
%         all_ICs.Key = []; % delete key column
%     end
%     % add timeseries of one subject to cell structure
%     roi_ts{isub} = all_ICs;
% end
% ts_fn = [group_folder filesep 'ROI_timeseries_cPPI' filesep 'roi_timeseries_' parcellation_atlas '.mat']; % save file in group folder
% save(ts_fn, 'roi_ts')

%% If it's only one timeseries per network
roi_ts={}; %create empty cell for timeseries of all subjects
for isub = 1:numel(sub) % loop over subjects
    all_ICs = table(); % for ICA: first put all components in one table, then append this to cell

    %allSes_confTCs = [];
    current_sub = sub{isub};
    if strcmp(current_sub, '004') | strcmp(current_sub, '024') | strcmp(current_sub, 'control001') % define subjects that only have one task run
        run = {'1'};
    elseif strcmp(current_sub, '008') | strcmp(current_sub, '018') | strcmp(current_sub, 'control020')
        run = {'2'};
    else
        run = {'1', '2'};
    end
%     for IC = 1:length(components) % loop over components
    %% Display which participant and IC is currently processed
    X = ['This is participant ' sub{isub}];
    disp(X);
    allSes_confTCs = [];
    for irun = 1:numel(run) % loop over runs
        % load txt file with timeseries for ROIs
        roi_file = dir([subjects_folder filesep 'sub-' sub{isub} filesep 'func' filesep 'parcellated' filesep ...
            parcellation_atlas filesep 'sub-' sub{isub} '_run-' run{irun} '_gm_dTS.txt']);
        roi_fn = [roi_file.folder filesep roi_file.name];
        roi_table = readtable(roi_fn, delimiter = 'space');
        roi_table.Properties.VariableNames = components;
        
        % concatenate timeseries for both runs in one table
        allSes_confTCs = [allSes_confTCs; roi_table];
%         end
%         allSes_confTCs.Key = (1:height(allSes_confTCs))'; %create key column for join function
%         all_ICs.Key = (1:height(allSes_confTCs))';
%         all_ICs = join(all_ICs, allSes_confTCs, 'Keys', 'Key'); % join table with all components for one subject
%         all_ICs.Key = []; % delete key column
    end
    % add timeseries of one subject to cell structure
    roi_ts{isub} = allSes_confTCs;
end
ts_fn = [group_folder filesep 'ROI_timeseries_cPPI' filesep 'roi_timeseries_' parcellation_atlas '.mat']; % save file in group folder
save(ts_fn, 'roi_ts')


%% Create cell with confounds per subject for cPPI – Sandra
% conf_ts={}; %create empty cell for timeseries of all subjects
% for isub = 1:numel(sub) % loop over subjects
% %     all_ICs = table(); % for ICA: first put all components in one table, then append this to cell
% 
%     %allSes_confTCs = [];
%     current_sub = sub{isub};
%     if strcmp(current_sub, '004') | strcmp(current_sub, '024') | strcmp(current_sub, 'control001') % define subjects that only have one task run
%         run = {'1'};
%     elseif strcmp(current_sub, '008') | strcmp(current_sub, '018') | strcmp(current_sub, 'control020')
%         run = {'2'};
%     else
%         run = {'1', '2'};
%     end
% %     for IC = 1:length(components) % loop over components
%     %% Display which participant and IC is currently processed
%     X = ['This is participant ' sub{isub}];
%     disp(X);
%     allSes_confTCs = [];
%     for irun = 1:numel(run) % loop over runs
%         % load txt file with timeseries for ROIs
%         conf_file = dir([subjects_folder filesep 'sub-' sub{isub} filesep 'func' filesep...
%             'sub-' sub{isub} '_task-SWG_run-' run{irun} '_conf_RMSD.csv']);
%         conf_fn = [conf_file.folder filesep conf_file.name];
%         conf_table = readtable(conf_fn, 'ReadVariableNames', true);
%         
%         % concatenate timeseries for both runs in one table
%         allSes_confTCs = [allSes_confTCs; conf_table];
% %         end
% %         allSes_confTCs.Key = (1:height(allSes_confTCs))'; %create key column for join function
% %         all_ICs.Key = (1:height(allSes_confTCs))';
% %         all_ICs = join(all_ICs, allSes_confTCs, 'Keys', 'Key'); % join table with all components for one subject
% %         all_ICs.Key = []; % delete key column
%     end
%     % add timeseries of one subject to cell structure
%     conf_ts{isub} = allSes_confTCs;
% end
% confound_fn = [group_folder filesep 'ROI_timeseries_cPPI' filesep 'RMSD_confound_timeseries.mat']; % save file in group folder
% save(confound_fn, 'conf_ts')


%% Create cell with confounds per subject for cPPI – Katie
% conf_ts={};
% for sub = 1:22
%     allSes_confTCs = [];
%     for ses = 1:12
%         reg_fn = strcat('/data/pt_01994/fmriprep/output/fmriprep/sub-',num2str(sub,'%02i'),'/ses-',num2str(ses,'%02i'),...
%             '/func/sub-',num2str(sub,'%02i'),'_ses-',num2str(ses,'%02i'),'_task-alltasks_desc-confounds_regressors.tsv');
%         t=readtable(reg_fn,'FileType','text');
%         confoundTCs = [t.csf, t.white_matter, t.global_signal, t.a_comp_cor_00, t.a_comp_cor_01, t.a_comp_cor_02, t.a_comp_cor_03, t.a_comp_cor_04,...
%            t.a_comp_cor_05, t.trans_x, t.trans_y, t.trans_z, t.rot_x, t.rot_y, t.rot_z];
%         %confoundTCs = [t.trans_x, t.trans_y, t.trans_z, t.rot_x, t.rot_y, t.rot_z];
%         allSes_confTCs = [allSes_confTCs; confoundTCs];
%     end
%     conf_ts{sub}=allSes_confTCs;
% end

% task_conf_ts={};
% for sub = 1:22
%     allSes_task_conf = [];
%     taskreg_fn = strcat('/data/pt_01994/user/williams/projects/IPL_func/FASTmodelregressors_alltrialparts/regMat_allRuns_vp',num2str(sub,'%02i'),'.mat');
%     load(taskreg_fn);
%     for ses = 1:12
%             regStack = [];
%         for i = 1:length(regDat.name)
%             sessionCut = strcat('Sn(', num2str(ses), ') constant');
%             if strcmp(sessionCut, regDat.name{i})
%                 sessionLength = regDat.regMat(:,i);
%             end
%             for tsk = 1:length(taskReglist)
%                 takeReg = strcat('Sn(', num2str(ses), ') ', taskReglist{tsk}, '*bf(1)')
%                 regDat.name{i}
%                 if strcmp(takeReg,regDat.name{i})
%                     regStack = [regStack, regDat.regMat(:,i)];
%                     
%                 end
%             end
%         end
%         idx = find(sessionLength);
%         regStack = regStack(idx,:);
%         allSes_task_conf = [allSes_task_conf; regStack];
%     end
%     task_conf_ts{sub} = allSes_task_conf;
% end
%         
% save('my_task_confound_timecourses_socFBvsTB.mat', 'task_conf_ts');
         

