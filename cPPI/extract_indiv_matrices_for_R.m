%% This script load cPPI correlation matrices output and saves participants' matrices in individual txt files so they can be analyzed in R

%% Paths
% define main folder
data_path = '/data/pt_02004/MDN_LANG/graph_theory/group/cPPI/Outputs_cPPI/';
% output_path = '/data/pt_02004/MDN_LANG/graph_theory/group/cPPI/Outputs_cPPI/IndivMatrices_121ROIs/';
output_path = '/data/pt_02004/MDN_LANG/graph_theory/group/cPPI/Outputs_cPPI/IndivMatrices_121ROIs_confRMSD/';

% S = 58; % number of subjects
% N = 126; % number of ROIs
parcel = 'ICA_masked_7ROIs';
% parcel_out = 'ICA_masked_121ROIs';
parcel_out = 'ICA_masked_121ROIs_confRMSD';
% load 3D matrix containing all subjects static connectivity matrices 
%load FCstatic_all   % (size nodes x nodes x subjects)
load([data_path 'cPPI_output_SemFluency_' parcel '_orig.mat']);
FC = ppi_cor;

sub = {'002','003','004','005','006','007','008','009','011','012','013','014','015','016','017','018','019','020','021','022','023','024','025','027','028','029','030','031', ...
          'control001','control002','control003','control004','control005','control006','control007','control008', ...
            'control009','control010','control011','control012','control013','control014','control015','control016', ...
            'control017','control018','control019','control020','control021','control022','control023','control024', ...
            'control025','control026','control027','control028','control029','control030'};

%% Loop over subjects
for isub = 1:numel(sub)
    disp(sub{isub}); % display actual subject working on 
    
    FCi = FC{isub};
%     FCi = squeeze(FC(:,:,isub)); % squeeze FCi, i.e., subject-specific 
    % static connectivity matrix
%     FCi = (FCi+FCi')./2; % use only half of the matrix
%     FCi(isnan(FCi)) = 0; % nans = 0
%     FCi = fisherZTransform(FCi); % fisher Z transform all correlations
%     FC2(:,:,s) = FCi;
    
%     Delete specific ROIs where more than 15% of all subjects had no
%     signal
    ROIs2delete = [5, 12, 85, 110, 112]; % these are the column numbers according to roi_ts file, make sure to delete them simultaneously since otherwise errors occur!
    FCi(ROIs2delete, :) = [];
    FCi(:, ROIs2delete) = [];
    
    % OR %
    % Delete all ROIs where one subject had no signal 
    % This is relevant for efficiency calculations where an equal amount of
    % edges is required
%       ROIs2delete = [5, 6, 12, 27, 30, 36, 37, 42, 50, 85, 86, 87, 88, 89, 100, 104, 105, 110, 112, 115, 117, 119]; % these are the column numbers according to roi_ts file, make sure to delete them simultaneously since otherwise errors occur!
%     FCi(ROIs2delete, :) = [];
%     FCi(:, ROIs2delete) = [];

    % make diagonal values 0
%     FCi = FCi - diag(diag(FCi));
    
    % Fisher-transform correlations
    FCi_Fisher_transformed = atanh(FCi); % fisher Z transform all correlations
    
%     output_fn = [output_path 'sub-' sub{isub} '_cPPI_corr_matrix_' parcel_out '.txt'];
    output_fn_Fisher_transformed = [output_path 'sub-' sub{isub} '_cPPI_corr_matrix_' parcel_out '_FisherTrans.txt'];
%     writematrix(FCi, output_fn);
    writematrix(FCi_Fisher_transformed, output_fn_Fisher_transformed);
end


%% Extract age-averaged matrices
% cor values
Y = cat(3,ppi_cor{:}); % concatenate all cPPI matrices in one table
Y = atanh(Y); % Fisher-z-transform matrices

% Delete specific ROIs from matrices if applicable
ROIs2delete = [5, 12, 85, 110, 112]; % these are the column numbers according to roi_ts file, make sure to delete them simultaneously since otherwise errors occur!
%ROIs2delete = [5, 6, 12, 27, 30, 36, 37, 42, 50, 85, 86, 87, 88, 89, 100, 104, 105, 110, 112, 115, 117, 119]; % these are the column numbers according to roi_ts file, make sure to delete them simultaneously since otherwise errors occur!
Y(ROIs2delete, :, :) = [];
Y(:, ROIs2delete, :) = [];
% out = mean(Y,3, 'omitnan'); % mean matrix across all subjects

OA_cPPImat = squeeze(mean(Y(:,:,1:28),3, 'omitnan')); % create mean matrix for older adults
YA_cPPImat = squeeze(mean(Y(:,:,29:58),3, 'omitnan')); % create mean matrix for young adults


writematrix(OA_cPPImat, 'OA_cPPI_correlation_121ROIs_FisherTrans_confRMSD.txt');
writematrix(YA_cPPImat, 'YA_cPPI_correlation_121ROIs_FisherTrans_confRMSD.txt');


% p values
pvalues = cat(3,ppi_cor_p{:});
% out_pvalues = mean(pvalues, 3, 'omitnan');

% Delete specific ROIs from matrices if applicable
pvalues(ROIs2delete, :, :) = [];
pvalues(:, ROIs2delete, :) = [];

OA_cPPImat_pvalues = squeeze(mean(pvalues(:,:,1:28),3, 'omitnan'));
YA_cPPImat_pvalues = squeeze(mean(pvalues(:,:,29:58),3, 'omitnan'));
allSubs_pvalue = mean(pvalues, 3, 'omitnan'); % mean matrix across all subjects
s
writematrix(OA_cPPImat_pvalues, 'OA_cPPI_cPPI_pvalues_121ROIs_confRMSD.txt');
writematrix(YA_cPPImat_pvalues, 'YA_cPPI_cPPI_pvalues_121ROIs_confRMSD.txt');
writematrix(allSubs_pvalue, 'AllSubs_cPPI_pvalues_7ROIs_confRMSD.txt');


%% plot
load /data/pt_02004/MDN_LANG/graph_theory/group/cPPI/networkList.mat;
nICs = length(networkList.ROIlabels);

Climits = [-0.5,0.5];
MyColorMap = [colormap('jet'); [0.7 0.7 0.7]];
figure(44);subplot(1,2,1);imagesc(OA_cPPImat,Climits);axis square;colormap(MyColorMap);title('OA: Network Interaction');set(gca,'yTick',(1:nICs),'fontsize',10);...
    set(gca,'xTick',(1:nICs),'fontsize',10);colorbar;xtickangle(45);xticklabels(networkList.label);yticklabels(networkList.label)
figure(45);subplot(1,2,1);imagesc(YA_cPPImat,Climits);axis square;colormap(MyColorMap);title('YA: Network Interaction');set(gca,'yTick',(1:nICs),'fontsize',10);...
    set(gca,'xTick',(1:nICs),'fontsize',10);colorbar;xtickangle(45);xticklabels(networkList.label);yticklabels(networkList.label)

pthresh = 0.05; %0.05 Bonferroni-correction: 11 nets x 10 interactions;
OA_cPPImat(OA_cPPImat_pvalues >=  pthresh) = 1;
YA_cPPImat(YA_cPPImat_pvalues >=  pthresh) = 1;

figure(44);imagesc(OA_cPPImat,Climits);axis square;colormap(MyColorMap);title('OA: Network Interaction');set(gca,'yTick',(1:nICs),'fontsize',10);...
    set(gca,'xTick',(1:nICs),'fontsize',10);colorbar;xtickangle(45);xticklabels(networkList.ROIlabels);yticklabels(networkList.ROIlabels)
figure(45);imagesc(YA_cPPImat,Climits);axis square;colormap(MyColorMap);title('YA: Network Interaction');set(gca,'yTick',(1:nICs),'fontsize',10);...
    set(gca,'xTick',(1:nICs),'fontsize',10);colorbar;xtickangle(45);xticklabels(networkList.ROIlabels);yticklabels(networkList.ROIlabels)