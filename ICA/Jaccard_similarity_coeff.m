%% Matlab script to compute Jaccard similarity coefficient between one or 
%% more template images and ICA component results. Written by Claude Bajada %%and Becky Jackson.

clc;clear;

FWE_thr = '05';

% Create temp folders for all ICA components
tempResults = '/data/pt_02004/MDN_LANG/graph_theory/group/ICAcomponents/results2';
if ~exist(tempResults)
    mkdir(tempResults)
end

D = '/data/pt_02004/MDN_LANG/graph_theory/group/ICAcomponents';
ICs = {'06', '09', '13', '16', '18', '45', '52'};
for folders = 1:numel(ICs)
    currD = [D filesep 'IC' ICs{folders} filesep 'cerebrumMasked/'];
%     currD = [D(folders).folder filesep D(folders).name];
    cd(currD)
    copyfile(strcat('IC', ICs{folders}, '_FWE', FWE_thr, '_k10_unmasked_bin.nii'), tempResults);
end

template_folder = '/data/pt_02004/MDN_LANG/graph_theory/scripts/ICA/tempTemplates_Jaccard'; %uigetdir('','Please select folder containing all template images');
results_folder = tempResults; %uigetdir('','Please select folder containing all results images');
output_folder = '/data/pt_02004/MDN_LANG/graph_theory/'; %uigetdir('','Please select folder to save output matrix');
 
templates = dir(strcat(template_folder, '/*.nii'));
results_data = dir(strcat(results_folder, '/*.nii'));
 
my_similarity_matrix_comp = zeros( length(templates) ,  length(results_data) );
 
 
for i = 1 : length(templates)
 
    %% Jaccard similarity (i.e. similarity of voxels that are involved in either component)
    template_image = extract_read_image(strcat(template_folder, '/', templates(i).name));
    bin_template_image = +logical(template_image);
    
    for j = 1 : length(results_data)
 
        results_image = extract_read_image(strcat(results_folder,'/', results_data(j).name));
        bin_results_image = +logical(results_image);
        comp_dif = abs(bin_template_image - bin_results_image);
        comp_dif_sum = sum(sum(sum(comp_dif)));
        comp_dif_total = sum(sum(sum(+logical(bin_template_image + bin_results_image))));
        comp_dif_pcnt = 1 - (comp_dif_sum / comp_dif_total);
        
        my_similarity_matrix_comp(i,j) = comp_dif_pcnt;
        
    end
    
end
 
%add labels to results matrix
templateNames = struct2cell(templates);
templateNames = templateNames(1,:)';
my_similarity_matrix_comp_labelled = horzcat(templateNames, num2cell(my_similarity_matrix_comp))
results_dataNames = struct2cell(results_data);
results_dataNames = results_dataNames(1,:)';
e = zeros(length(results_dataNames)+1, 1);
e=num2cell(e);
for i =2:(length(results_dataNames)+1)
e(i, 1) = results_dataNames(i-1);
end
 
e=e';
my_similarity_matrix_comp_labelled=vertcat(e,my_similarity_matrix_comp_labelled);

% % delete temporary results folder
% rmdir(tempResults, 's');

%save results matrix
cd(output_folder)
save my_similarity_matrix_comp_labelled.mat, my_similarity_matrix_comp_labelled
clearvars -except my_similarity_matrix_comp_labelled my_similarity_matrix_comp


%% extract_read_image function
function [ image, header ] = extract_read_image( image_name )
%EXTRACT_READ_IMAGE A function that uses the spm functions to read in a
%header and an image but wraps both functions up into one
 
%   [ image, header ] = extract_read_image( image_name )
 
header = spm_vol(image_name);
image = spm_read_vols(header);
 
end
