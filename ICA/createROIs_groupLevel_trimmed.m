%% MarsBar – Create ROIs
% This script creates ROIs based on cluster peaks and subpeaks from t-tests
% of ICA components
% ROIs are first extracted for each cluster, then spheres of 5mm radius are
% created for each coordinate, and finally spheres are trimmed by clusters
% so that ROIs only contain voxel locations of activated voxels in group
% analysis

clear all
% close all

%% Paths

%Setup SPM12
addpath('/data/pt_02004/spm12/')

% define main folder
main_folder = '/data/pt_02004/MDN_LANG/graph_theory/group/ICAcomponents/';

% start up SPM12 in fMRI-modecurrent
spm('Defaults','fMRI');
spm_jobman('initcfg');

% Start marsbar to make sure spm_get works
addpath /data/pt_02004/spm12/toolbox/marsbar/
marsbar('on')

%% Set general options

sphereRadius = 5; % mm

ICs = {'06', '09', '13', '16', '18', '45', '52'};

%% Make rois
fprintf('\n');

for IC=1:numel(ICs)
    
    % load file with coordinates of each peak and cluster number
    coord_file = dir([main_folder 'IC' ICs{IC} '/cerebrumMasked/peak+subpeak_coords_IC' ICs{IC} '_maskedInclWith_*_+ClusterNumber_FWE05_k10.txt']);
    coord_path = [coord_file.folder filesep coord_file.name];
    coords = readtable(coord_path);
    coords = table2cell(coords);

    % load file with names of cluster ROIs for transformation of spheres in
    % Marsbar
    c_file = [main_folder 'IC' ICs{IC} '/cerebrumMasked/ROIs/IC' ICs{IC} '_clusters.txt'];
    clusterROIs = readtable(c_file, 'Format', '%s', 'Delimiter', '\n', 'ReadVariableNames', false);

    % load file with additional information like labels of peaks
    cluster_file = dir([main_folder 'IC' ICs{IC} '/cerebrumMasked/MNIcoords_Juelich_HO_atlases_maskedInclWith_*_FWE05_k10.txt']);
    cluster_path = [cluster_file.folder filesep cluster_file.name];
    clusters = readtable(cluster_path, 'Delimiter', ',', 'HeaderLines', 1, 'VariableNamesLine', 1);
    names = clusters.HO_1; % use HO labels for ROI labels
    match = wildcardPattern + '% '; 
    names = erase(names, match); % extract ROI labels from HO description
    
    % Output directory
    out_folder = [main_folder 'IC' ICs{IC} '/cerebrumMasked/ROIs/'];

    % loop over coordinates for each cluster
    for i=1:size(coords, 1)

        this_coord = coords(i, 1:3);
        %name_coord = coords(i, 4);
        current_cluster = coords(i, 4);
        current_name = names{i};

        fprintf('Working on ROI %d/%d: %s ... ', i, size(coords,1), current_name);

        % Load cluster ROI for trimming
        cluster_roi = string(clusterROIs.Var1(current_cluster{1}));
        act_roi = maroi('load', cluster_roi); % FWE-corrected at peak level (p<0.05) for one-sided t-test per IC

        % Make sphere ROI to do trimming
        sphere_centre = cell2mat(this_coord);
        sphere_radius = sphereRadius;
        sphere_roi = maroi_sphere(struct('centre', sphere_centre, 'radius', sphere_radius));

        % Combine for trimmed ROI
        trim_stim = sphere_roi & act_roi;
        trim_stim = label(trim_stim, current_name);

        outName = fullfile(out_folder, ['IC' ICs{IC} '_' sprintf('%s_transformed_roi', num2str(i))]);

        % save MarsBaR ROI (.mat) file
        saveroi(trim_stim, [outName '.mat']);
        
        % save the Nifti (.nii) file
        save_as_image(trim_stim, [outName '.nii']); 

        fprintf('done.\n');

    end
end

fprintf('\nAll done. %d ROIs written to %s.', size(coords,1), out_folder);