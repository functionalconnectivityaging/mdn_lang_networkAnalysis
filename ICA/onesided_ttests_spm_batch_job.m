%-----------------------------------------------------------------------
% Job saved on 06-Oct-2021 17:43:34 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7487)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.spm.stats.factorial_design.dir = {'/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/cerebrumMasked'};
%%
matlabbatch{1}.spm.stats.factorial_design.des.t1.scans = {
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control01_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control02_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control03_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control04_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control05_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control06_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control07_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control08_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control09_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control10_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control11_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control12_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control13_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control14_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control15_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control16_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control17_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control18_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control19_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control20_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control21_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control22_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control23_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control24_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control25_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control26_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control27_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control28_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control29_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub-control30_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub02_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub03_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub04_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub05_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub06_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub07_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub08_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub09_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub11_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub12_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub13_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub14_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub15_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub16_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub17_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub18_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub19_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub20_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub21_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub22_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub23_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub24_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub25_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub27_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub28_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub29_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub30_component8_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC09/AgeSemanticCog_agg_sub31_component8_ica.nii,1'
                                                          };
%%
matlabbatch{1}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{1}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{1}.spm.stats.factorial_design.masking.em = {'/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/ds_yeo_cerebrum.nii,1'};
matlabbatch{1}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.stats.factorial_design.globalm.glonorm = 1;
matlabbatch{2}.spm.stats.fmri_est.spmmat(1) = cfg_dep('Factorial design specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;
matlabbatch{3}.spm.stats.factorial_design.dir = {'/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/cerebrumMasked'};
%%
matlabbatch{3}.spm.stats.factorial_design.des.t1.scans = {
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control01_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control02_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control03_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control04_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control05_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control06_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control07_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control08_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control09_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control10_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control11_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control12_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control13_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control14_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control15_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control16_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control17_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control18_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control19_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control20_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control21_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control22_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control23_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control24_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control25_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control26_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control27_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control28_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control29_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub-control30_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub02_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub03_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub04_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub05_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub06_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub07_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub08_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub09_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub11_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub12_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub13_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub14_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub15_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub16_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub17_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub18_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub19_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub20_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub21_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub22_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub23_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub24_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub25_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub27_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub28_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub29_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub30_component12_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC13/AgeSemanticCog_agg_sub31_component12_ica.nii,1'
                                                          };
%%
matlabbatch{3}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{3}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{3}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{3}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{3}.spm.stats.factorial_design.masking.em = {'/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/ds_yeo_cerebrum.nii,1'};
matlabbatch{3}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{3}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{3}.spm.stats.factorial_design.globalm.glonorm = 1;
matlabbatch{4}.spm.stats.fmri_est.spmmat(1) = cfg_dep('Factorial design specification: SPM.mat File', substruct('.','val', '{}',{3}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{4}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{4}.spm.stats.fmri_est.method.Classical = 1;
matlabbatch{5}.spm.stats.factorial_design.dir = {'/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/cerebrumMasked'};
%%
matlabbatch{5}.spm.stats.factorial_design.des.t1.scans = {
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control01_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control02_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control03_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control04_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control05_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control06_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control07_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control08_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control09_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control10_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control11_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control12_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control13_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control14_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control15_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control16_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control17_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control18_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control19_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control20_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control21_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control22_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control23_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control24_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control25_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control26_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control27_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control28_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control29_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub-control30_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub02_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub03_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub04_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub05_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub06_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub07_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub08_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub09_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub11_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub12_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub13_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub14_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub15_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub16_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub17_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub18_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub19_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub20_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub21_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub22_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub23_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub24_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub25_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub27_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub28_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub29_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub30_component15_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC16/AgeSemanticCog_agg_sub31_component15_ica.nii,1'
                                                          };
%%
matlabbatch{5}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{5}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{5}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{5}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{5}.spm.stats.factorial_design.masking.em = {'/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/ds_yeo_cerebrum.nii,1'};
matlabbatch{5}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{5}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{5}.spm.stats.factorial_design.globalm.glonorm = 1;
matlabbatch{6}.spm.stats.fmri_est.spmmat(1) = cfg_dep('Factorial design specification: SPM.mat File', substruct('.','val', '{}',{5}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{6}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{6}.spm.stats.fmri_est.method.Classical = 1;
matlabbatch{7}.spm.stats.factorial_design.dir = {'/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/cerebrumMasked'};
%%
matlabbatch{7}.spm.stats.factorial_design.des.t1.scans = {
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control01_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control02_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control03_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control04_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control05_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control06_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control07_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control08_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control09_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control10_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control11_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control12_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control13_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control14_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control15_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control16_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control17_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control18_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control19_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control20_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control21_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control22_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control23_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control24_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control25_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control26_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control27_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control28_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control29_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub-control30_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub02_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub03_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub04_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub05_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub06_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub07_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub08_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub09_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub11_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub12_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub13_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub14_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub15_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub16_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub17_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub18_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub19_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub20_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub21_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub22_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub23_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub24_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub25_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub27_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub28_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub29_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub30_component17_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC18/AgeSemanticCog_agg_sub31_component17_ica.nii,1'
                                                          };
%%
matlabbatch{7}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{7}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{7}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{7}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{7}.spm.stats.factorial_design.masking.em = {'/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/ds_yeo_cerebrum.nii,1'};
matlabbatch{7}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{7}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{7}.spm.stats.factorial_design.globalm.glonorm = 1;
matlabbatch{8}.spm.stats.fmri_est.spmmat(1) = cfg_dep('Factorial design specification: SPM.mat File', substruct('.','val', '{}',{7}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{8}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{8}.spm.stats.fmri_est.method.Classical = 1;
matlabbatch{9}.spm.stats.factorial_design.dir = {'/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/cerebrumMasked'};
%%
matlabbatch{9}.spm.stats.factorial_design.des.t1.scans = {
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control01_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control02_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control03_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control04_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control05_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control06_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control07_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control08_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control09_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control10_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control11_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control12_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control13_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control14_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control15_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control16_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control17_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control18_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control19_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control20_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control21_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control22_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control23_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control24_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control25_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control26_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control27_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control28_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control29_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub-control30_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub02_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub03_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub04_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub05_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub06_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub07_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub08_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub09_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub11_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub12_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub13_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub14_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub15_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub16_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub17_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub18_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub19_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub20_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub21_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub22_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub23_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub24_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub25_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub27_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub28_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub29_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub30_component44_ica.nii,1'
                                                          '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC45/AgeSemanticCog_agg_sub31_component44_ica.nii,1'
                                                          };
%%
matlabbatch{9}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{9}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{9}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{9}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{9}.spm.stats.factorial_design.masking.em = {'/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/ds_yeo_cerebrum.nii,1'};
matlabbatch{9}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{9}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{9}.spm.stats.factorial_design.globalm.glonorm = 1;
matlabbatch{10}.spm.stats.fmri_est.spmmat(1) = cfg_dep('Factorial design specification: SPM.mat File', substruct('.','val', '{}',{9}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{10}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{10}.spm.stats.fmri_est.method.Classical = 1;
matlabbatch{11}.spm.stats.factorial_design.dir = {'/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/cerebrumMasked'};
%%
matlabbatch{11}.spm.stats.factorial_design.des.t1.scans = {
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control01_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control02_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control03_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control04_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control05_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control06_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control07_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control08_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control09_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control10_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control11_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control12_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control13_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control14_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control15_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control16_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control17_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control18_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control19_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control20_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control21_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control22_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control23_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control24_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control25_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control26_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control27_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control28_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control29_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub-control30_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub02_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub03_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub04_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub05_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub06_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub07_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub08_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub09_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub11_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub12_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub13_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub14_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub15_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub16_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub17_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub18_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub19_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub20_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub21_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub22_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub23_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub24_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub25_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub27_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub28_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub29_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub30_component51_ica.nii,1'
                                                           '/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/IC52/AgeSemanticCog_agg_sub31_component51_ica.nii,1'
                                                           };
%%
matlabbatch{11}.spm.stats.factorial_design.cov = struct('c', {}, 'cname', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{11}.spm.stats.factorial_design.multi_cov = struct('files', {}, 'iCFI', {}, 'iCC', {});
matlabbatch{11}.spm.stats.factorial_design.masking.tm.tm_none = 1;
matlabbatch{11}.spm.stats.factorial_design.masking.im = 1;
matlabbatch{11}.spm.stats.factorial_design.masking.em = {'/data/pt_02004/MDN_LANG/graph_theory/analyses/ICA/gift/bothruns_smoothed_wDesignMat_mask/averagedComponents/ds_yeo_cerebrum.nii,1'};
matlabbatch{11}.spm.stats.factorial_design.globalc.g_omit = 1;
matlabbatch{11}.spm.stats.factorial_design.globalm.gmsca.gmsca_no = 1;
matlabbatch{11}.spm.stats.factorial_design.globalm.glonorm = 1;
matlabbatch{12}.spm.stats.fmri_est.spmmat(1) = cfg_dep('Factorial design specification: SPM.mat File', substruct('.','val', '{}',{11}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{12}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{12}.spm.stats.fmri_est.method.Classical = 1;
