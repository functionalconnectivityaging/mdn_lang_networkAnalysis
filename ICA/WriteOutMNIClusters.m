function WriteOutMNIClusters(SPM_table)

%% This function saves peak cluster coordinates from SPM results table in txt file

if nargin < 1
    error('You have to provide an SPM results table called "TabDat" as first argument')
elseif nargin == 1
    disp('SPM table specified');
    SPM_table = SPM_table;
end

coords_cell = SPM_table.dat(:, 12); % Column 12 holds MNI coordinates
cluster_size = SPM_table.dat(:, 5); % Column 4 holds cluster size info
%t_values = SPM_table.dat(:, 9); % Column 9 holds t value info

%% Extract global peak cluster coordinates and write to txt file for each IC
prompt = 'Which component number?';
IC = input(prompt);

% If you used an inclusive/exclusive mask, use this prompt to write this
% information to the name of the output file
prompt2 = 'Which inclusive image?';
mask = input(prompt2, 's');

% Opens GUI to select output folder
% outdir = uigetdir('/data/pt_02004/MDN_LANG/graph_theory/group/ICAcomponents', 'Select output folder');
base_dir = '/data/pt_02004/MDN_LANG/graph_theory/group/ICAcomponents/';
outdir = [base_dir 'IC' IC '/cerebrumMasked'];

%% Extract global peak cluster coordinates and write to txt file for each IC

% % Extract only global peaks to cell array
% peak_coords = {};
% peaks = find(~cellfun(@isempty,cluster_size)); % find cluster peaks
% 
% for peak = [peaks], i = 1:length(peaks); % loop over these indices to extract coordinates
%         peak_coords(i,1) = coords_cell(peak);
% end
% 
% table_peak_coords = cell2table(peak_coords); % Convert cell to table
% 
% % Save file in specified output folder
% outfile = [outdir, '/peak_coords_IC' num2str(IC) '_.txt'];
% writetable(table_peak_coords, outfile);
% disp('Output table was saved');


% Write all peaks to cell and the table but also create second column with
% cluster number which we need for creation of transformed ROIs with
% MarsBar
peak_coords = {};
p = find(~cellfun(@isempty,cluster_size)); % find cluster peaks

for coord = 1:length(coords_cell)
    if ismember(coord, p)
        peak_coords{coord, 1} = coords_cell(coord);
        cluster = ismember(p, coord);
        peak_coords{coord, 2} = find(cluster);
    else 
        peak_coords{coord, 1} = coords_cell(coord);
        peak_coords{coord, 2} = find(cluster);
    end
end
    
% % Simply write all peaks to cell array
% peak_coords = coords_cell;

% Convert cell to table for easier saving
table_peak_coords = cell2table(peak_coords); % Convert cell to table

% Save file in specified output folder
outfile = [outdir, '/peak+subpeak_coords_IC' IC '_maskedInclWith_' mask '_+ClusterNumber_FWE05_k10.txt'];
writetable(table_peak_coords, outfile);
disp('Output table with peak coordinates was saved');