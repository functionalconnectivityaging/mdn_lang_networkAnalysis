%%  1st level batch script: Segmentation, Coregistration, Normalisation & Smoothing
% This script performs first level analyses on single subject level

%% Specify paths & folders
%  Data folder
script_path =  '/data/pt_02004/Scripts/Matlab/Preprocessing';
data_path = '/data/pt_02004/MDN_LANG/fmriprep_20.2.3/output/';
spm_path = '/data/pt_02004/spm12';

% Subject_folders

sub = {'002','003','004','005','006','007','008','009','011', ...
        '012','013','014','015','016','017','018','019','020' ...
         '021','022','023','024','025','027','028','029','030','031', ...
         'control001','control002','control003','control004','control005','control006','control007','control008', ...
            'control009','control010','control011','control012','control013','control014','control015', ...
            'control016','control017','control018','control019','control020','control021','control022', ...
            'control023','control024','control025','control026','control027','control028','control029','control030'};
        
%run = {'1'};
      
%% Initialise SPM defaults
  spm('defaults', 'FMRI');
  spm_jobman('initcfg');


%% 
for i = 1:numel(sub)
   current_sub = sub{i};
    if strcmp(current_sub, '004') | strcmp(current_sub, '024') | strcmp(current_sub, 'control001')
        run = {'1'};
    elseif strcmp(current_sub, '008') | strcmp(current_sub, '018')
        run = {'2'};
    else
        run = {'1', '2'};
    end
    
    for x = 1:numel(run)

        % Display which participant and which run is currently processed
        X = ['This is participant ' sub{i} ' and run ' run{x}];
            disp(X);

            %% Get volumes
        epi_mainfolder = dir([data_path 'sub-' sub{i} '/fmriprep/sub-' sub{i} '/func']);
        curr_epi_subfolder = epi_mainfolder.folder;
        nifti_files = dir([curr_epi_subfolder '/sub-' sub{i} '_task-SWG_run-' run{x} '_echo-2_space-MNI152NLin6Asym_desc-preproc_bold_trimmed.nii']);
        func_images_path = [nifti_files.folder '/' nifti_files.name];
            func_vols = spm_vol(func_images_path);
            Vols = {};
            for iVol = 1:numel(func_vols)
                Vols{iVol} = [func_images_path ',' num2str(iVol)];
            end
            Vols = Vols';
        %-----------------------------------------------------------------------
        clear matlabbatch
        matlabbatch{1}.spm.spatial.smooth.data = Vols;
        matlabbatch{1}.spm.spatial.smooth.fwhm = [5 5 5];
        matlabbatch{1}.spm.spatial.smooth.dtype = 0;
        matlabbatch{1}.spm.spatial.smooth.im = 0;
        matlabbatch{1}.spm.spatial.smooth.prefix = 's';

        %% RUN THE BATCH
        spm_jobman('run', matlabbatch)
    end
end