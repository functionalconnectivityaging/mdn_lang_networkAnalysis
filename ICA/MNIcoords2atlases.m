function MNIcoords2atlases(SPM_table)

%% This function maps MNI coordinates to regions in different atlases
% It uses the function mni2atlas which can be downloaded here https://github.com/dmascali/mni2atlas 
% The atlases can be selected and are based on atlases from FSL
% Currently, coordinates are mapped to Juelich and Harvard Oxford cortical
% atlases

if nargin < 1
    error('You have to provide an SPM results table called "TabDat" as first argument')
elseif nargin == 1
    disp('SPM table specified');
    SPM_table = SPM_table;
end

% Prompt IC
prompt = 'Which component number?';
IC = input(prompt, 's');

% If you used an inclusive/exclusive mask, use this prompt to write this
% information to the name of the output file
prompt2 = 'Which inclusive image?';
mask = input(prompt2, 's');

% Opens GUI to select output folder
% outdir = uigetdir('/data/pt_02004/MDN_LANG/graph_theory/group/ICAcomponents', 'Select output folder');
base_dir = '/data/pt_02004/MDN_LANG/graph_theory/group/ICAcomponents/';
outdir = [base_dir 'IC' IC '/cerebrumMasked'];

coords_cell = SPM_table.dat(:, 12); % Column 12 holds MNI coordinates
cluster_size = SPM_table.dat(:, 5); % Column 4 holds cluster size info
t_values = SPM_table.dat(:, 9); % Column 9 holds t value info

% Create empty cells for atlas labels
Juelich = {};
HO = {};

addpath /data/pt_02004/MDN_LANG/graph_theory/scripts/mni2atlas/;
addpath /data/pt_02004/MDN_LANG/graph_theory/toolboxes/Nifti_Analyze_Toolbox

%% Extract atlas information for MNI coordinates
for entry = 1:length(coords_cell) % Loop over coordinates in SPM results table
    [atlas] = mni2atlas(cell2mat(coords_cell(entry, 1)), [1,2]); % Call mni2atlas function, look into atlases 1 & 2
    
    for i = 1:size(atlas(1).label, 2) % create cell structure for J�lich atlas info
        Juelich(entry, i) = atlas(1).label(i); 
    end
    if entry == length(coords_cell) & isempty(atlas(1).label)
            Juelich(entry, 1) = {'Nan'};
    end
    
    if ~isempty(atlas(2).label) % create cell structure for HO atlas info
        for j = 1:size(atlas(2).label, 2)
            HO(entry, j) = atlas(2).label(j);
        end
    end       
end

% Create one output table with cluster size, t values, coordinates and atlas labels
atlases = table(cluster_size, t_values, coords_cell, HO, Juelich);

% Save file in specified output folder
outfile = [outdir, '/MNIcoords_Juelich_HO_atlases_maskedInclWith_' mask '_FWE05_k10.txt'];
writetable(atlases, outfile);
disp('Output table was saved');