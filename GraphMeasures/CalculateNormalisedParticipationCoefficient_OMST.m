% function participation_coef_norm from: https://github.com/omidvarnia/Dynamic_brain_connectivity_analysis 

%% Paths
% define main folder
data_path = '/data/pt_02004/MDN_LANG/graph_theory/group/GraphMetrics/OMST/';
output_path = '/data/pt_02004/MDN_LANG/graph_theory/group/GraphMetrics/ParticipationCoefficientNorm/';

% add BCT path
addpath(genpath('/data/pt_02004/MDN_LANG/graph_theory/toolboxes/2019_03_03_BCT'));

% load OMSTs
load([data_path 'OMST_all_subjects_2022.mat']);
FC = OMST;

% Community Membership
load('/data/pt_02004/MDN_LANG/graph_theory/group/GraphMetrics/CommunityMembership104ROIs.mat');
load('/data/pt_02004/MDN_LANG/graph_theory/group/GraphMetrics/104ROIs_labels.mat');

% Subjects
subs = 58;

PC_NORM = table();

for isub = 1:subs
    disp(int2str(isub)); % display actual subject working on 
    
    % extract subject-specific matrix
    FCi = FC{1, isub}; % squeeze FCi, i.e., subject-specific 
    
    Tnew = table();
    memb_104_new = memb_104 + 1;
    
    % run PC function
    [PC_norm, PC_residual, PC, between_mod_k] = participation_coef_norm(FCi, memb_104_new, 100, 1);
    
    % Save output in array
    Tnew = table(PC_norm');
    Tnew = splitvars(Tnew);
    Tnew.Properties.VariableNames = ROI_labels104';
    PC_NORM = [PC_NORM; Tnew];
    
%     % save individual txt file per subject
%     output_fn = [output_path 'sub-' num2str(isub) '_participation_coeff_norm_wei.txt'];
%     writetable(PC_NORM, output_fn);
%     
%     % write results for all subjects to cell array
%     PCnorm{isub} = PC_NORM;
    
end