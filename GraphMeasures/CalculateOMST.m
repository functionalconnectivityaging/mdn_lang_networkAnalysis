% Calculate orthogonal minimum spanning tree (OMST)
% requires scripts from https://github.com/BIAPT/topological_filtering_networks
% Reference: Dimitriadis et al, 2017. Front. Neuroinform. 11:28. doi: 10.3389/fninf.2017.00028

%% Paths
% define main folder
data_path = '/data/pt_02004/MDN_LANG/graph_theory/group/NBS/allSubs/';
output_path = '/data/pt_02004/MDN_LANG/graph_theory/group/GraphMetrics/OMST/';

% add BCT path
addpath(genpath('/data/pt_02004/MDN_LANG/graph_theory/scripts/topological_filtering_networks_OMST'));
% addpath(genpath('/data/pt_02004/MDN_LANG/graph_theory/scripts/edgefiltering_0.1'));

% load 3D matrix containing all subjects' Fisher-transformed static connectivity matrices 
%load([data_path 'cPPI_matrices_121ROIs_allSubs.mat']);
load([data_path 'cPPI_matrices_104ROIs_allSubs.mat']);
FC = Y;

% Subjects
subs = 58;

OMST = {};
Res = zeros(58,4);

for isub = 1:subs
    disp(int2str(isub)); % display actual subject working on 
        
    % extract subject-specific matrix
    FCi = squeeze(FC(:,:,isub)); % squeeze FCi, i.e., subject-specific 
%     memb_sub = memb;
%     ROIs_sub = ROIs;
   
    % delete columns and rows with NaN (because participant didn't have
    % signal in the specific ROI)
%     out = FCi - diag(diag(FCi)); 
%     ROIs2delete = find(all(isnan(out),2));
%     FCi(ROIs2delete, :) = [];
%     FCi(:, ROIs2delete) = [];
%     memb_sub(ROIs2delete, :) = [];
%     ROIs_sub(ROIs2delete, :) = [];
    
    % make diagonal values 0
    FCi(1:1+size(FCi,1):end) = 0; 
        
    % make negative values 0
    FCi(FCi < 0) = 0;
    
    % rescale values to range between 0 and 1 (then create distance matrix)
%     FCi = rescale(FCi, 0, 0.99);
%     FCi_inv = FCi;
%     FCi_inv = 1.-FCi_inv;
%     FCi_inv(FCi_inv == 1) = 0;
    
    % Cost-normalize matrix
%     FCi_norm = FCi/sum(FCi, 'all');
        
    
    [nCIJtree, OMST_sub, mdeg, globalcosteffmax, costmax, EffGlobal] = threshold_omst_gce_wu(FCi, 0);

%     [OMST_sub,ind] = jgraph_threshold_max_gce_omst(FCi, 1);
%     [OMST_sub,ind, mdeg, globalcosteffmax, costmax, EffGlobal] = jgraph_threshold_max_gce_omst(FCi, 0);
        
 
    % save individual txt file per subject
%     output_fn = [output_path 'sub-' num2str(isub) '_OMST.txt'];
%     writematrix(OMST_sub, output_fn);
   

    % write results for all subjects to cell array
    Res(isub, 1) = EffGlobal;
    Res(isub, 2) = mdeg;
    Res(isub, 3) = globalcosteffmax;
    Res(isub, 4) = costmax;
        
    OMST{isub} = OMST_sub;
end

% Save efficiency and GCE results in file
% output_fn2 = [output_path 'Efficiency.txt'];
% writematrix(Res, output_fn2);

% Save OMSTs to mat file
save([output_path 'OMST_all_subjects_2022'], 'OMST');
    
% % Save matrices for each age group
% OA = [];
% for i = 1:28
%     temp_norm = EffGlob{i}.E_norm;
%     temp_E = EffGlob{i}.E;
%     temp_thr = EffGlob{i}.ithr;
%     OA = [OA; temp_norm temp_E temp_thr];
% end
% writematrix(OA, [output_path 'GlobalEff_norm_wei_104ROIs_1000NUllModels_OA.txt']);
% 
% YA = [];
% for i = 29:58
%     temp_norm = EffGlob{i}.E_norm;
%     temp_E = EffGlob{i}.E;
%     temp_thr = EffGlob{i}.ithr;
%     YA= [YA; temp_norm temp_E temp_thr];
% end
% writematrix(YA, [output_path 'GlobalEff_norm__wei_104ROIs_1000NullModels_YA.txt']);