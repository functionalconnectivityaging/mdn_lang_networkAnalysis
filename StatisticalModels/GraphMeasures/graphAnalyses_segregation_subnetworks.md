# Load packages

``` r
library(psych)
library(reshape2)
library(plyr)
library(tidyverse)
library(rstatix)
source(here::here("GraphMeasures/CalculateSegregration.R"))
library(lme4)
library(emmeans)
library(ggeffects)
library(stringr)
library(ppcor)
library(ggpubr)
library(see)
library(egg)
library(here)
```

# Load data

``` r
# Load individual matrices for 121 ROIs for segregation analysis
load(here("cPPI/RData/all_subjects_cPPI_indivMatrices_3DArray.RData"))

# Load ROI labels
load(here("cPPI/RData/ROI_labels_121ROIs.RData"))
load(here("cPPI/RData/ROI_labels_121ROIs_sorted.RData"))

# Load df with segregation results, all subs, individual networks
load(here("GraphMeasures/RData/SegResult_allSubs_indivNets_12_2021.RData"))
load(here("GraphMeasures/RData/SegResult_allSubs_indivNets_long_12_2021.RData")) # df in long format

# Merged df with segregation values and behavior
load(here("GraphMeasures/RData/behav_df_indivNets_02_2022.RData"))

# Merged df with segregation values and NPsy data
load(here("GraphMeasures/RData/NP_df_indivNets_12_2021.RData"))

# Color palette for spring-embedded plots
load(here("GraphMeasures/RData/colorPalette_springEmbeddedPlots.RData"))
```

# Segregation Analysis - individual segregation/integration per network

``` r
## The analysis requires a function called CalculateSegregation.R, it should be in the same folder like this script. The function comes from M. Chan's Github and was adapted for my analysis.

#### Segregation of for each network individually ####
all_subjects <- all_subjects_indivNets_121ROIs

seg_all_subjects_indivNets <- NULL # create empty variable 

# Loop over individual cPPI matrices, call segregation function, and write results to a list
for(sub in 1:dim(all_subjects)[3]){
  matrix <- all_subjects[,,sub]
  matrix <- `colnames<-`(matrix, ROI_labels[,4])
  matrix <- `rownames<-`(matrix, ROI_labels[,4])
  
  result <- segregation_indivNets(M = matrix, Ci = ROI_labels[,2], diagzero = T, negzero = T)
  result$sub <- sub
  seg_all_subjects_indivNets <- rbind(seg_all_subjects_indivNets, result)
}

segregation_results <- as.data.frame(seg_all_subjects_indivNets)
segregation_results <- segregation_results %>% 
  unnest(cols = colnames(segregation_results))

segregation_results$age <- "OA" # add column for age to df
segregation_results[29:58,]$age <- "YA"
segregation_results$age <- as.factor(segregation_results$age)

segregation_results$sub[1:28] <- c(2:9,11:25,27:31)
segregation_results$sub[29:58] <- 1:30
segregation_results$sub <- str_pad(segregation_results$sub, 3, pad = "0")

segregation_results <- merge(segregation_results, motion, by = c("age", "sub"))


# Make long df
segregation_results_long <- segregation_results %>% 
  gather("connectivity_within", "value_within", ends_with("within")) %>% 
  gather("connectivity_between", "value_between", ends_with("between")) %>% 
  gather("connectivity_segregation", "value_segregation", ends_with("segregation")) %>% 
  filter(str_extract(connectivity_within, "^(\\w+_)") == str_extract(connectivity_between, "^(\\w+_)")) %>% 
  filter(str_extract(connectivity_within, "^(\\w+_)") == str_extract(connectivity_segregation, "^(\\w+_)"))

segregation_results_long <- segregation_results_long[,-c(7,9)]
segregation_results_long <- segregation_results_long %>% 
  rename(network = connectivity_within)
segregation_results_long$network <- gsub("_within$", "", segregation_results_long$network)
segregation_results_long$network <- factor(segregation_results_long$network, levels = c("DMN_A",  "DMN_A_C",  "DMN_B", "SemNet", "Control_B", "VAN_B", "DAN_A"))
segregation_results_long$age <- factor(segregation_results_long$age, levels = c("YA", "OA"))
```

# Plots

``` r
# Within-network connectivity
within_connectivity <- ggplot(segregation_results_long, aes(x = network, y = value_within, fill = network)) +
  geom_violin() +
  stat_summary(fun.data = "mean_sdl",  fun.args = list(mult = 1), 
    geom = "pointrange", color = "#4b4b4b") +
  ylab("Within-network connectivity") +
  scale_y_continuous(limits = c(0, 0.3)) +
  scale_fill_manual(values = pal) +
  facet_wrap(~ age) +
  apatheme +
  theme(legend.position=("none"),
        legend.title=element_blank(), 
        axis.title.x = element_blank(),
        axis.text.x = element_text(angle = 45, vjust = 1, hjust=1))
within_connectivity
```

![](graphAnalyses_segregation_subnetworks_files/figure-markdown_github/segregation%20analysis%20-%20plotting-1.png)

``` r
# Between-network connectivity
between_connectivity <- ggplot(segregation_results_long, aes(x = network, y = value_between, fill = network)) +
  geom_violin() +
  stat_summary(fun.data = "mean_sdl",  fun.args = list(mult = 1), 
    geom = "pointrange", color = "#4b4b4b") +
  ylab("Between-network connectivity") +
  scale_y_continuous(limits = c(0, 0.15)) +
  scale_fill_manual(values = pal) +
  facet_wrap(~ age) +
  apatheme +
  theme(legend.position=("none"),
        legend.title=element_blank(), 
        axis.title.x = element_blank(),
        axis.text.x = element_text(angle = 45, vjust = 1, hjust=1))
between_connectivity
```

![](graphAnalyses_segregation_subnetworks_files/figure-markdown_github/segregation%20analysis%20-%20plotting-2.png)

``` r
# facet wrap by network
networks = c(
  "DMN_A" = "Default A", 
  "DMN_A_C" = "Default A+C", 
  "DMN_B" = "Default B", 
  "SemNet" = "Semantic", 
  "Control_B" = "Control B", 
  "VAN_B" = "VentAtt B", 
  "DAN_A" = "DorsAtt A")
segregation_indivNets_network <- ggplot(segregation_results_long, aes(x = age, y = value_segregation, fill = age, color = age)) +
  geom_violinhalf(flip = 1, trim = F) +
  stat_summary(fun.data = "mean_sdl",  fun.args = list(mult = 1), 
    geom = "pointrange", color = "grey", size = .5, shape = 20, position = position_dodge(width = 1)) +
  ylab("Segregation") +
  scale_fill_manual(values = c("#018571", "#52257a")) +
  scale_color_manual(values = c("#018571", "#52257a")) +
  coord_flip() +
  apatheme +
  theme(axis.text.y = element_blank(),
        axis.ticks.y = element_blank(), 
        axis.line.y = element_blank(),
        axis.title.y = element_blank(),
        legend.position = "none") +
  facet_wrap(~ network, ncol = 1, labeller = as_labeller(networks), strip.position = "left")
segregation_indivNets_network
```

![](graphAnalyses_segregation_subnetworks_files/figure-markdown_github/segregation%20analysis%20-%20plotting-3.png)

``` r
#### Linear model ####
# segregation ~ age + network
m_segregation <- lmer(value_segregation ~ age + network + meanRMSD + (1|sub), data = segregation_results_long, REML = F)
summary(m_segregation)
```

    ## Linear mixed model fit by maximum likelihood  ['lmerMod']
    ## Formula: value_segregation ~ age + network + meanRMSD + (1 | sub)
    ##    Data: segregation_results_long
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##   -576.8   -532.8    299.4   -598.8      395 
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -4.1292 -0.5705  0.1203  0.6622  2.3425 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev.
    ##  sub      (Intercept) 0.001423 0.03773 
    ##  Residual             0.012500 0.11180 
    ## Number of obs: 406, groups:  sub, 31
    ## 
    ## Fixed effects:
    ##                  Estimate Std. Error t value
    ## (Intercept)       0.50467    0.02278  22.154
    ## ageOA            -0.10510    0.01487  -7.070
    ## networkDMN_A_C    0.21939    0.02076  10.567
    ## networkDMN_B      0.05779    0.02076   2.784
    ## networkSemNet     0.01982    0.02076   0.955
    ## networkControl_B  0.13250    0.02076   6.382
    ## networkVAN_B      0.17057    0.02076   8.216
    ## networkDAN_A      0.30783    0.02076  14.827
    ## meanRMSD         -0.66186    0.15703  -4.215
    ## 
    ## Correlation of Fixed Effects:
    ##             (Intr) ageOA  nDMN_A nDMN_B ntwrSN ntwC_B nVAN_B nDAN_A
    ## ageOA        0.253                                                 
    ## ntwrDMN_A_C -0.456  0.000                                          
    ## netwrkDMN_B -0.456  0.000  0.500                                   
    ## networkSmNt -0.456  0.000  0.500  0.500                            
    ## ntwrkCntr_B -0.456  0.000  0.500  0.500  0.500                     
    ## netwrkVAN_B -0.456  0.000  0.500  0.500  0.500  0.500              
    ## netwrkDAN_A -0.456  0.000  0.500  0.500  0.500  0.500  0.500       
    ## meanRMSD    -0.663 -0.652  0.000  0.000  0.000  0.000  0.000  0.000

``` r
drop1(m_segregation, test = "Chisq")
```

    ## Single term deletions
    ## 
    ## Model:
    ## value_segregation ~ age + network + meanRMSD + (1 | sub)
    ##          npar     AIC     LRT               Pr(Chi)    
    ## <none>        -576.84                                  
    ## age         1 -531.78  47.055     0.000000000006903 ***
    ## network     6 -342.19 246.642 < 0.00000000000000022 ***
    ## meanRMSD    1 -561.48  17.361     0.000030903674344 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
emm_seg <- emmeans(m_segregation, ~ age)
contrast(emm_seg, "pairwise", adjust = "holm")
```

    ##  contrast estimate     SE  df t.ratio p.value
    ##  YA - OA     0.105 0.0151 401 6.939   <.0001 
    ## 
    ## Results are averaged over the levels of: network 
    ## Degrees-of-freedom method: kenward-roger

``` r
emms_seg <- ggpredict(m_segregation, terms = c("age", "network"))
plot(emms_seg) +
          apatheme +
          theme(plot.title = element_blank())
```

![](graphAnalyses_segregation_subnetworks_files/figure-markdown_github/segregation%20analysis%20-%20plotting-4.png)

# Merge segregation df with behavioral df for Accuracy and RT data

``` r
# Merge seg df with behavioral df
behav_df <- merge(behav_df, segregation_results, by = c("age", "sub"))

# Center all connectivity columns + motion RMS
scaled <- sapply(behav_df[,15:35], scale, scale = F)
behav_df <- cbind(behav_df, scaled)
behav_df <- behav_df[-c(15:35)]

contrasts(behav_df$AgeSum) <- contr.sum(2)/2
```

# Prepare df for response time

``` r
# prepare df
behav_df.RT <- behav_df[!is.na(behav_df$Onset),] # Remove NAs from Onset column and create df
behav_df.RT <- behav_df.RT[behav_df.RT$Correct != 0,] # Remove incorrect answers 
behav_df.RT <- droplevels(behav_df.RT) # drop unused factor levels
contrasts(behav_df.RT$AgeSum) <- contr.sum(2)/2 # re-apply sum coding to factor
```

# Behavioral relevance of segregation for individual networks

## Accuracy

``` r
seg_acc <- glmer(Correct ~ (DMN_A_segregation + DMN_A_C_segregation + DMN_B_segregation + SemNet_segregation + Control_B_segregation + VAN_B_segregation + DAN_A_segregation) * AgeSum + Education + meanRMSD + (1|sub) + (1|Category), data = behav_df, family = binomial(link = "logit"))
summary(seg_acc)
```

    ## Generalized linear mixed model fit by maximum likelihood (Laplace
    ##   Approximation) [glmerMod]
    ##  Family: binomial  ( logit )
    ## Formula: 
    ## Correct ~ (DMN_A_segregation + DMN_A_C_segregation + DMN_B_segregation +  
    ##     SemNet_segregation + Control_B_segregation + VAN_B_segregation +  
    ##     DAN_A_segregation) * AgeSum + Education + meanRMSD + (1 |  
    ##     sub) + (1 | Category)
    ##    Data: behav_df
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##   4799.4   4943.3  -2379.7   4759.4     9817 
    ## 
    ## Scaled residuals: 
    ##      Min       1Q   Median       3Q      Max 
    ## -12.5085   0.1040   0.2053   0.3507   0.6828 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev.
    ##  sub      (Intercept) 0.06943  0.2635  
    ##  Category (Intercept) 1.70994  1.3076  
    ## Number of obs: 9837, groups:  sub, 31; Category, 20
    ## 
    ## Fixed effects:
    ##                               Estimate Std. Error z value      Pr(>|z|)    
    ## (Intercept)                    2.37944    0.38984   6.104 0.00000000104 ***
    ## DMN_A_segregation              0.51155    0.38418   1.332      0.183012    
    ## DMN_A_C_segregation           -0.30041    0.70421  -0.427      0.669678    
    ## DMN_B_segregation              1.07289    0.60252   1.781      0.074966 .  
    ## SemNet_segregation            -0.25889    0.48088  -0.538      0.590329    
    ## Control_B_segregation          1.67351    0.81590   2.051      0.040254 *  
    ## VAN_B_segregation              1.24441    0.55800   2.230      0.025738 *  
    ## DAN_A_segregation             -2.21674    0.64004  -3.463      0.000533 ***
    ## AgeSum1                       -0.15837    0.18439  -0.859      0.390381    
    ## Education                     -0.10510    0.07172  -1.465      0.142810    
    ## meanRMSD                       4.40446    1.77216   2.485      0.012942 *  
    ## DMN_A_segregation:AgeSum1      1.30080    0.72979   1.782      0.074681 .  
    ## DMN_A_C_segregation:AgeSum1   -0.93688    1.35473  -0.692      0.489211    
    ## DMN_B_segregation:AgeSum1     -2.96577    1.20207  -2.467      0.013617 *  
    ## SemNet_segregation:AgeSum1     2.18133    1.20555   1.809      0.070387 .  
    ## Control_B_segregation:AgeSum1  1.93652    1.37348   1.410      0.158558    
    ## VAN_B_segregation:AgeSum1     -5.02191    1.13389  -4.429 0.00000947070 ***
    ## DAN_A_segregation:AgeSum1     -0.19102    1.39789  -0.137      0.891310    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
drop1(seg_acc, test = "Chisq")
```

    ## Single term deletions
    ## 
    ## Model:
    ## Correct ~ (DMN_A_segregation + DMN_A_C_segregation + DMN_B_segregation + 
    ##     SemNet_segregation + Control_B_segregation + VAN_B_segregation + 
    ##     DAN_A_segregation) * AgeSum + Education + meanRMSD + (1 | 
    ##     sub) + (1 | Category)
    ##                              npar    AIC     LRT    Pr(Chi)    
    ## <none>                            4799.4                       
    ## Education                       1 4799.5  2.0991    0.14739    
    ## meanRMSD                        1 4803.4  5.9702    0.01455 *  
    ## DMN_A_segregation:AgeSum        1 4800.6  3.1660    0.07519 .  
    ## DMN_A_C_segregation:AgeSum      1 4797.9  0.4716    0.49227    
    ## DMN_B_segregation:AgeSum        1 4803.2  5.7546    0.01645 *  
    ## SemNet_segregation:AgeSum       1 4800.3  2.8474    0.09152 .  
    ## Control_B_segregation:AgeSum    1 4799.5  2.0164    0.15561    
    ## VAN_B_segregation:AgeSum        1 4815.7 18.2229 0.00001965 ***
    ## DAN_A_segregation:AgeSum        1 4797.5  0.0183    0.89235    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
emms_acc_DMN_B <- ggpredict(seg_acc, terms = c("DMN_B_segregation[all]", "AgeSum"))
plot_acc_DMN_B <- plot(emms_acc_DMN_B) +
          scale_fill_manual(values = c("#52257a", "#018571")) +
          scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
          coord_cartesian(ylim = c(0.4, 1)) +
          xlab("Default B") +
          ylab("Accuracy") +
          apatheme

emms_acc_VAN_B <- ggpredict(seg_acc, terms = c("VAN_B_segregation[all]", "AgeSum"))
plot_acc_VAN_B <- plot(emms_acc_VAN_B) +
          scale_fill_manual(values = c("#52257a", "#018571")) +
          scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
          coord_cartesian(ylim = c(0.4, 1)) +
          xlab("VentAtt") +
          ylab("Accuracy") +
          apatheme

## Arrange all accuracy plots in one plot
plot_Acc_seg_indivNets <- egg::ggarrange(plot_acc_DMN_B +
            theme(legend.position = "none"),
          plot_acc_VAN_B +
            theme(legend.position = "none",
                    axis.text.y = element_blank(),
                    axis.ticks.y = element_blank(),
                    axis.title.y = element_blank()),
          nrow = 1)
```

![](graphAnalyses_segregation_subnetworks_files/figure-markdown_github/accuracy-1.png)

## RT

``` r
seg_RT <- lmer(log(Onset) ~ (DMN_A_segregation + DMN_A_C_segregation + DMN_B_segregation + SemNet_segregation + Control_B_segregation + VAN_B_segregation + DAN_A_segregation) * AgeSum + Education + meanRMSD + (1|sub) + (1|Category), data = behav_df.RT, REML = F)
summary(seg_RT)
```

    ## Linear mixed model fit by maximum likelihood  ['lmerMod']
    ## Formula: 
    ## log(Onset) ~ (DMN_A_segregation + DMN_A_C_segregation + DMN_B_segregation +  
    ##     SemNet_segregation + Control_B_segregation + VAN_B_segregation +  
    ##     DAN_A_segregation) * AgeSum + Education + meanRMSD + (1 |  
    ##     sub) + (1 | Category)
    ##    Data: behav_df.RT
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##   5538.3   5687.7  -2748.2   5496.3     9048 
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -9.9261 -0.6458 -0.1212  0.5086  5.1655 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev.
    ##  sub      (Intercept) 0.043511 0.20859 
    ##  Category (Intercept) 0.002374 0.04873 
    ##  Residual             0.105063 0.32413 
    ## Number of obs: 9069, groups:  sub, 31; Category, 20
    ## 
    ## Fixed effects:
    ##                               Estimate Std. Error t value
    ## (Intercept)                    6.60518    0.05628 117.353
    ## DMN_A_segregation              0.61955    0.05114  12.114
    ## DMN_A_C_segregation           -0.25392    0.11129  -2.282
    ## DMN_B_segregation              0.22653    0.08686   2.608
    ## SemNet_segregation            -0.60028    0.06446  -9.313
    ## Control_B_segregation         -0.38355    0.13009  -2.948
    ## VAN_B_segregation             -0.17826    0.08108  -2.199
    ## DAN_A_segregation             -0.31929    0.06899  -4.628
    ## AgeSum1                       -0.10207    0.02812  -3.630
    ## Education                     -0.02451    0.00995  -2.463
    ## meanRMSD                      -0.46899    0.26126  -1.795
    ## DMN_A_segregation:AgeSum1     -0.83556    0.09335  -8.951
    ## DMN_A_C_segregation:AgeSum1   -0.33419    0.19786  -1.689
    ## DMN_B_segregation:AgeSum1      0.17857    0.17125   1.043
    ## SemNet_segregation:AgeSum1     0.39368    0.25244   1.560
    ## Control_B_segregation:AgeSum1  0.80990    0.17325   4.675
    ## VAN_B_segregation:AgeSum1     -0.08367    0.16265  -0.514
    ## DAN_A_segregation:AgeSum1      1.67550    0.20182   8.302

``` r
drop1(seg_RT, test = "Chisq")
```

    ## Single term deletions
    ## 
    ## Model:
    ## log(Onset) ~ (DMN_A_segregation + DMN_A_C_segregation + DMN_B_segregation + 
    ##     SemNet_segregation + Control_B_segregation + VAN_B_segregation + 
    ##     DAN_A_segregation) * AgeSum + Education + meanRMSD + (1 | 
    ##     sub) + (1 | Category)
    ##                              npar    AIC    LRT               Pr(Chi)    
    ## <none>                            5538.3                                 
    ## Education                       1 5542.3  6.014               0.01419 *  
    ## meanRMSD                        1 5539.2  2.894               0.08893 .  
    ## DMN_A_segregation:AgeSum        1 5615.7 79.380 < 0.00000000000000022 ***
    ## DMN_A_C_segregation:AgeSum      1 5539.2  2.851               0.09132 .  
    ## DMN_B_segregation:AgeSum        1 5537.4  1.077               0.29928    
    ## SemNet_segregation:AgeSum       1 5538.7  2.317               0.12794    
    ## Control_B_segregation:AgeSum    1 5557.5 21.160           0.000004226 ***
    ## VAN_B_segregation:AgeSum        1 5536.6  0.243               0.62235    
    ## DAN_A_segregation:AgeSum        1 5605.0 68.623 < 0.00000000000000022 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
emms_RT_DMN_A <- ggpredict(seg_RT, terms = c("DMN_A_segregation[all]", "AgeSum"))
plot_RT_DMN_A <- plot(emms_RT_DMN_A) +
          scale_fill_manual(values = c("#52257a", "#018571")) +
          scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
          coord_cartesian(ylim = c(400, 1200)) +
          xlab("Default A") +
          ylab("Response time") +
          apatheme

emms_RT_ControlB <- ggpredict(seg_RT, terms = c("Control_B_segregation[all]", "AgeSum"))
plot_RT_ControlB <- plot(emms_RT_ControlB) +
          scale_fill_manual(values = c("#52257a", "#018571")) +
          scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
          coord_cartesian(ylim = c(400, 1200)) +
          xlab("Control B") +
          apatheme

emms_RT_DAN_A <- ggpredict(seg_RT, terms = c("DAN_A_segregation[all]", "AgeSum"))
plot_RT_DAN_A <- plot(emms_RT_DAN_A) +
          scale_fill_manual(values = c("#52257a", "#018571")) +
          scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
          coord_cartesian(ylim = c(400, 1200)) +
          xlab("Dorsal attention A") +
          apatheme


## Arrange all RT plots in one plot
plot_RT_seg_indivNets <- egg::ggarrange(plot_RT_DMN_A +
            theme(legend.position = "none"),
          plot_RT_ControlB +
            theme(axis.text.y = element_blank(),
                    axis.ticks.y = element_blank(),
                    axis.title.y = element_blank(),
                    legend.position = "none"),
          plot_RT_DAN_A +
            theme(axis.text.y = element_blank(),
                    axis.ticks.y = element_blank(),
                    axis.title.y = element_blank(),
                    legend.position = "none"),
          nrow = 1)
```

![](graphAnalyses_segregation_subnetworks_files/figure-markdown_github/RT-1.png)

# NPsy data

## Merge segregation df with Npsy df

``` r
# Merge seg df with behavioral df
NP_df <- merge(NP_df, segregation_results, by = c("age", "sub"))

motion <- behav_df %>% 
  group_by(age, sub) %>% 
  summarise(rms = mean(rms))
NP_df <- merge(NP_df, motion, by = c("age", "sub"))

# Center all connecitvity columns + motion RMS & Education
scaled <- sapply(NP_df[,c(6:10,12,13,15:37)], scale, scale = F)
NP_df <- cbind(NP_df, scaled)
NP_df <- NP_df[-c(6:10,12,13,15:37)]

contrasts(NP_df$AgeSum) <- contr.sum(2)/2
```

## Analyses with NPsy data and segregation values

``` r
#### Factor analysis ####
NP_factors <- NP_df[,c(6:10,12,13)] # df only with behavioral tests
NP_factors$TMT_A <- ifelse(NP_factors$TMT_A > 0, NP_factors$TMT_A * (-1), abs(NP_factors$TMT_A))
NP_factors$TMT_B <- ifelse(NP_factors$TMT_B > 0, NP_factors$TMT_B * (-1), abs(NP_factors$TMT_B))
NP.fa <- factanal(NP_factors, factors = 2, scores = "regression", rotation = "varimax") # run factor analysis, assume two factors --> looks good, third factor has eigenvalue < 1
NP.fa
```

    ## 
    ## Call:
    ## factanal(x = NP_factors, factors = 2, scores = "regression",     rotation = "varimax")
    ## 
    ## Uniquenesses:
    ##         Reading_span                  WST RWT_Vornamen_korrekt 
    ##                0.798                0.745                0.005 
    ##   RWT_Hobbys_korrekt                 DSST                TMT_A 
    ##                0.714                0.226                0.314 
    ##                TMT_B 
    ##                0.462 
    ## 
    ## Loadings:
    ##                      Factor1 Factor2
    ## Reading_span          0.449         
    ## WST                          -0.501 
    ## RWT_Vornamen_korrekt  0.158   0.985 
    ## RWT_Hobbys_korrekt    0.300   0.443 
    ## DSST                  0.734   0.485 
    ## TMT_A                 0.798   0.219 
    ## TMT_B                 0.714   0.168 
    ## 
    ##                Factor1 Factor2
    ## SS loadings      2.006   1.729
    ## Proportion Var   0.287   0.247
    ## Cumulative Var   0.287   0.534
    ## 
    ## Test of the hypothesis that 2 factors are sufficient.
    ## The chi square statistic is 14.04 on 8 degrees of freedom.
    ## The p-value is 0.0807

``` r
plot(NP.fa$loadings[,1], # plot factors
     NP.fa$loadings[,2],
     xlab = "Factor 1",
     ylab = "Factor 2",
     ylim = c(-1,1),
     xlim = c(-1,1),
     main = "Varimax rotation")
text(NP.fa$loadings[,1]-0.08, 
     NP.fa$loadings[,2]-0.08,
      colnames(NP_factors),
      col="blue")
abline(h = 0, v = 0)
```

![](graphAnalyses_segregation_subnetworks_files/figure-markdown_github/Analyses%20with%20NPsy%20data%20and%20segregation%20values-1.png)

``` r
NP_df <- cbind(NP_df, NP.fa$scores) # bind factor score columns with df for correlation analyses

NP_df$AgeBin <- 1 # create binarised column for age to run partial correlation controlling for age group
NP_df$AgeBin <- ifelse(NP_df$age == "YA", 0, 1)
NP_df$AgeBin <- as.numeric(NP_df$AgeBin)

#### Partial correlation controlling for age #### 
# No significant partial correlations

## DMN_A_segregation
pcc <- with(NP_df, pcor.test(DMN_A_segregation, Factor1, AgeBin))
pcc
```

    ##    estimate   p.value statistic  n gp  Method
    ## 1 0.1342297 0.3195055  1.004565 58  1 pearson

``` r
pcc2 <- with(NP_df, pcor.test(DMN_A_segregation, Factor2, AgeBin))
pcc2
```

    ##      estimate   p.value  statistic  n gp  Method
    ## 1 -0.09299172 0.4914458 -0.6926464 58  1 pearson

``` r
## DMN_A_C_segregation
pcc <- with(NP_df, pcor.test(DMN_A_C_segregation, Factor1, AgeBin)) 
pcc
```

    ##    estimate   p.value statistic  n gp  Method
    ## 1 0.1795492 0.1814105   1.35357 58  1 pearson

``` r
pcc2 <- with(NP_df, pcor.test(DMN_A_C_segregation, Factor2, AgeBin))
pcc2
```

    ##      estimate   p.value  statistic  n gp  Method
    ## 1 0.003981661 0.9765496 0.02952902 58  1 pearson

``` r
## DMN_B_segregation
pcc <- with(NP_df, pcor.test(DMN_B_segregation, Factor1, AgeBin)) 
pcc
```

    ##    estimate   p.value statistic  n gp  Method
    ## 1 0.2309617 0.0838903  1.760455 58  1 pearson

``` r
pcc2 <- with(NP_df, pcor.test(DMN_B_segregation, Factor2, AgeBin))
pcc2 # sig w/o MCC
```

    ##     estimate    p.value statistic  n gp  Method
    ## 1 -0.2626344 0.04841408 -2.018612 58  1 pearson

``` r
p.adjust(pcc2$p.value, method = "holm", n = 7)
```

    ## [1] 0.3388986

``` r
x_resid <- resid(lm(DMN_B_segregation ~ AgeBin, NP_df)) # prepare resid for partial correlation plot for MDN and Factor 1
y_resid <- resid(lm(Factor2 ~ AgeBin, NP_df)) # prepare resid for partial correlation plot for MDN and Factor 1
pcc_plot <- ggplot(NP_df, aes(x = x_resid, y = y_resid)) + # plot partial correlation
  geom_smooth(method=lm) +
  geom_point() +
  labs(y = "Semantic memory (factor 2) | Age", x = "DMN_B_segregation | Age") +
  apatheme
pcc_plot
```

![](graphAnalyses_segregation_subnetworks_files/figure-markdown_github/Analyses%20with%20NPsy%20data%20and%20segregation%20values-2.png)

``` r
## SemNet_segregation
pcc <- with(NP_df, pcor.test(SemNet_segregation, Factor1, AgeBin)) 
pcc # sig w/o MCC
```

    ##    estimate    p.value statistic  n gp  Method
    ## 1 0.2659218 0.04557114  2.045788 58  1 pearson

``` r
p.adjust(pcc$p.value, method = "holm", n = 7)
```

    ## [1] 0.318998

``` r
x_resid <- resid(lm(SemNet_segregation ~ AgeBin, NP_df)) # prepare resid for partial correlation plot for SemNet and Factor 1
y_resid <- resid(lm(Factor1 ~ AgeBin, NP_df)) # prepare resid for partial correlation plot
pcc_plot2 <- ggplot(NP_df, aes(x = x_resid, y = y_resid)) + # plot partial correlation
  geom_smooth(method=lm) +
  geom_point() +
  labs(y = "Executive functions (factor 1) | Age", x = "Semantic network segregation | Age") +
  apatheme
pcc_plot2
```

![](graphAnalyses_segregation_subnetworks_files/figure-markdown_github/Analyses%20with%20NPsy%20data%20and%20segregation%20values-3.png)

``` r
pcc2 <- with(NP_df, pcor.test(SemNet_segregation, Factor2, AgeBin))
pcc2
```

    ##   estimate   p.value statistic  n gp  Method
    ## 1 0.040225 0.7664022 0.2985582 58  1 pearson

``` r
## Control_B_segregation
pcc <- with(NP_df, pcor.test(Control_B_segregation, Factor1, AgeBin)) 
pcc
```

    ##    estimate  p.value statistic  n gp  Method
    ## 1 0.1474856 0.273594  1.105876 58  1 pearson

``` r
pcc2 <- with(NP_df, pcor.test(Control_B_segregation, Factor2, AgeBin))
pcc2
```

    ##    estimate   p.value statistic  n gp  Method
    ## 1 0.0853804 0.5277257 0.6355187 58  1 pearson

``` r
## VAN_B_segregation
pcc <- with(NP_df, pcor.test(VAN_B_segregation, Factor1, AgeBin)) 
pcc
```

    ##    estimate    p.value statistic  n gp  Method
    ## 1 0.2286704 0.08709471  1.742022 58  1 pearson

``` r
pcc2 <- with(NP_df, pcor.test(VAN_B_segregation, Factor2, AgeBin))
pcc2
```

    ##     estimate   p.value statistic  n gp  Method
    ## 1 0.02447904 0.8565675 0.1815958 58  1 pearson

``` r
## DAN_A_segregation
pcc <- with(NP_df, pcor.test(DAN_A_segregation, Factor1, AgeBin)) 
pcc
```

    ##      estimate  p.value  statistic  n gp  Method
    ## 1 -0.08153186 0.546562 -0.6066762 58  1 pearson

``` r
pcc2 <- with(NP_df, pcor.test(DAN_A_segregation, Factor2, AgeBin))
pcc2
```

    ##      estimate   p.value  statistic  n gp  Method
    ## 1 -0.02250779 0.8680103 -0.1669645 58  1 pearson

``` r
#### Correlations only OA ####
NP_df_OA <- NP_df[NP_df$age == "OA", ] # filter df for OA
NP_df_OA = droplevels(NP_df_OA) # drop unused factor levels

DMN_A_fac1_OA <- with(NP_df_OA, cor.test(Factor1, DMN_A_segregation)) 
DMN_A_fac2_OA <- with(NP_df_OA, cor.test(Factor2, DMN_A_segregation)) 

DMN_A_C_fac1_OA <- with(NP_df_OA, cor.test(Factor1, DMN_A_C_segregation)) 
DMN_A_C_fac2_OA <- with(NP_df_OA, cor.test(Factor2, DMN_A_C_segregation)) 

DMN_B_fac1_OA <- with(NP_df_OA, cor.test(Factor1, DMN_B_segregation)) 
DMN_B_fac2_OA <- with(NP_df_OA, cor.test(Factor2, DMN_B_segregation)) #sig

SemNet_fac1_OA <- with(NP_df_OA, cor.test(Factor1, SemNet_segregation)) 
SemNet_fac2_OA <- with(NP_df_OA, cor.test(Factor2, SemNet_segregation)) 

Control_B_fac1_OA <- with(NP_df_OA, cor.test(Factor1, Control_B_segregation)) 
Control_B_fac2_OA <- with(NP_df_OA, cor.test(Factor2, Control_B_segregation)) 

VAN_B_fac1_OA <- with(NP_df_OA, cor.test(Factor1, VAN_B_segregation)) 
VAN_B_fac2_OA <- with(NP_df_OA, cor.test(Factor2, VAN_B_segregation))

DAN_A_fac1_OA <- with(NP_df_OA, cor.test(Factor1, DAN_A_segregation)) 
DAN_A_fac2_OA <- with(NP_df_OA, cor.test(Factor2, DAN_A_segregation))

cor_plot_OA <- ggscatter(NP_df, x = "DMN_B_segregation", y = "Factor2", # plot for significant correlation for factor 1 and segregation in YA
          color = "age", palette = c("#52257a", "#018571"),
          add = "reg.line", conf.int = TRUE, 
          cor.coef = F, cor.method = "pearson",
          xlab = "Default B Segregation", ylab = "Semantic memory") +
  stat_cor(aes(color = age)) +
  apatheme
cor_plot_OA <- ggpar(cor_plot_OA, ylim = c(-3,3))
cor_plot_OA
```

![](graphAnalyses_segregation_subnetworks_files/figure-markdown_github/Analyses%20with%20NPsy%20data%20and%20segregation%20values-4.png)

``` r
#### Correlations only YA ####
# One significant correlation
NP_df_YA <- NP_df[NP_df$age == "YA", ] # filter df for YA
NP_df_YA = droplevels(NP_df_YA) # drop unused factor levels

DMN_A_fac1_YA <- with(NP_df_YA, cor.test(Factor1, DMN_A_segregation)) 
DMN_A_fac2_YA <- with(NP_df_YA, cor.test(Factor2, DMN_A_segregation)) 

DMN_A_C_fac1_YA <- with(NP_df_YA, cor.test(Factor1, DMN_A_C_segregation)) 
DMN_A_C_fac2_YA <- with(NP_df_YA, cor.test(Factor2, DMN_A_C_segregation)) 

DMN_B_fac1_YA <- with(NP_df_YA, cor.test(Factor1, DMN_B_segregation)) 
DMN_B_fac2_YA <- with(NP_df_YA, cor.test(Factor2, DMN_B_segregation)) 

SemNet_fac1_YA <- with(NP_df_YA, cor.test(Factor1, SemNet_segregation)) 
SemNet_fac2_YA <- with(NP_df_YA, cor.test(Factor2, SemNet_segregation)) 

Control_B_fac1_YA <- with(NP_df_YA, cor.test(Factor1, Control_B_segregation)) 
Control_B_fac2_YA <- with(NP_df_YA, cor.test(Factor2, Control_B_segregation)) 

VAN_B_fac1_YA <- with(NP_df_YA, cor.test(Factor1, VAN_B_segregation))
VAN_B_fac2_YA <- with(NP_df_YA, cor.test(Factor2, VAN_B_segregation))

DAN_A_fac1_YA <- with(NP_df_YA, cor.test(Factor1, DAN_A_segregation)) 
DAN_A_fac2_YA <- with(NP_df_YA, cor.test(Factor2, DAN_A_segregation))


cor_plot_YA <- ggscatter(NP_df, x = "VAN_B_segregation", y = "Factor1", # plot for significant correlation for factor 1 and segregation in YA
          color = "age", palette = c("#52257a", "#018571"),
          add = "reg.line", conf.int = TRUE, 
          cor.coef = F, cor.method = "pearson",
          xlab = "Ventral attention B Segregation", ylab = "Executive functions") +
  stat_cor(aes(color = age)) +
  apatheme
cor_plot_YA <- ggpar(cor_plot_YA, ylim = c(-3,3))
cor_plot_YA
```

![](graphAnalyses_segregation_subnetworks_files/figure-markdown_github/Analyses%20with%20NPsy%20data%20and%20segregation%20values-5.png)
