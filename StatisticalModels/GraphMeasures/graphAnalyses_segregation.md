# Load packages

``` r
library(psych)
library(reshape2)
library(plyr)
library(tidyverse)
library(rstatix)
source(here::here("GraphMeasures/CalculateSegregration.R"))
library(lme4)
library(emmeans)
library(ggeffects)
library(stringr)
library(igraph)
library(ppcor)
library(ggpubr)
library(see)
```

    ## Warning: package 'see' was built under R version 4.1.2

``` r
library(here)
```

# Load data

``` r
# Load individual matrices for 121 ROIs for segregation analysis
load(here("cPPI/RData/all_subjects_cPPI_indivMatrices_3DArray.RData"))

# Load ROI labels
load(here("cPPI/RData/ROI_labels_121ROIs.RData"))
load(here("cPPI/RData/ROI_labels_121ROIs_sorted.RData"))

# Load averaged age matrices for spring-embedded plot
load(here("cPPI/RData/OA_cPPI_correlation_121ROIs_FisherTransform_confRMSD.RData"))
load(here("cPPI/RData/YA_cPPI_correlation_121ROIs_FisherTransform_confRMSD.RData"))

# Color palette for spring-embedded plots
load(here("GraphMeasures/RData/colorPalette_springEmbeddedPlots.RData"))

# Load df with segregation results, all subs
load(here("GraphMeasures/RData/SegResult_allSubs_12_2021.RData"))

# Merged df with segregation values and behavior
load(here("GraphMeasures/RData/behav_df_02_2022.RData")) 

# Merged df with segregation values and NPsy data
load(here("GraphMeasures/RData/NP_df_02_2022.RData"))
```

# Segregation Analysis

``` r
## The analysis requires a function called CalculateSegregation.R, it should be in the same folder like this script. The function comes from M. Chan's Github and was adapted for my analysis.

all_subjects <- all_subjects_indivNets_121ROIs

seg_all_subjects_121ROIs <- NULL # create empty variable

# Loop over individual cPPI matrices, call segregation function, and write results to a list
for(sub in 1:dim(all_subjects)[3]){
  matrix <- all_subjects[,,sub]
  matrix <- `colnames<-`(matrix, ROI_labels[,4])
  matrix <- `rownames<-`(matrix, ROI_labels[,4])
  
  result <- segregation(matrix, Ci = ROI_labels[,2], diagzero = T, negzero = T)
  result$sub <- sub
  seg_all_subjects_121ROIs <- rbind(seg_all_subjects_121ROIs, result)
}

segregation_results <- as.data.frame(seg_all_subjects_121ROIs)
segregation_results <- segregation_results %>% 
  unnest(cols = c(W, B, S, sub))

mean(segregation_results$S[1:28]) # Mean segregation OA
mean(segregation_results$S[29:58]) # Mean segregation YA

segregation_results$age <- "OA" # add column for age to df
segregation_results[29:58,]$age <- "YA"
segregation_results$age <- as.factor(segregation_results$age)
```

# Linear model to test effect of age

``` r
m1 <- lmer(S ~ age + meanRMSD + (1|sub), data = segregation_results, REML = F)
summary(m1)
```

    ## Linear mixed model fit by maximum likelihood  ['lmerMod']
    ## Formula: S ~ age + meanRMSD + (1 | sub)
    ##    Data: segregation_results
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##   -133.0   -122.7     71.5   -143.0       53 
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -4.2675 -0.4047  0.0734  0.6150  2.1247 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance  Std.Dev.
    ##  sub      (Intercept) 0.0003652 0.01911 
    ##  Residual             0.0046240 0.06800 
    ## Number of obs: 58, groups:  sub, 31
    ## 
    ## Fixed effects:
    ##             Estimate Std. Error t value
    ## (Intercept)  0.65984    0.02422  27.242
    ## ageOA       -0.08311    0.02250  -3.694
    ## meanRMSD    -0.76320    0.21505  -3.549
    ## 
    ## Correlation of Fixed Effects:
    ##          (Intr) ageOA 
    ## ageOA     0.229       
    ## meanRMSD -0.847 -0.605

``` r
drop1(m1, test = "Chisq")
```

    ## boundary (singular) fit: see ?isSingular

    ## Single term deletions
    ## 
    ## Model:
    ## S ~ age + meanRMSD + (1 | sub)
    ##          npar     AIC    LRT   Pr(Chi)    
    ## <none>        -132.98                     
    ## age         1 -123.19 11.791 0.0005950 ***
    ## meanRMSD    1 -123.68 11.298 0.0007759 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
emms <- ggpredict(m1, terms = c("age"))
plot_m1 <- plot(emms) +
          apatheme +
          theme(plot.title = element_blank())
plot_m1
```

![](graphAnalyses_segregation_files/figure-markdown_github/segregation%20analysis%20-%20lm-1.png)

# Violin plot segregation - age effect

``` r
segregation_results$age <- factor(segregation_results$age, levels = c("YA", "OA"))

seg_age <- ggplot(segregation_results, aes(x = age, y = S, fill = age, color = age)) +
  geom_violinhalf(flip = 1, trim = F) +
  stat_summary(fun.data = "mean_sdl",  fun.args = list(mult = 1), 
    geom = "pointrange", color = "grey", size = 1, shape = 20, position = position_dodge(width = 1)) +
  ylab("Segregation") +
  scale_fill_manual(values = c("#018571", "#52257a")) +
  scale_color_manual(values = c("#018571", "#52257a")) +
  apatheme +
  theme(axis.text.x = element_blank(),
        axis.ticks.x = element_blank())
seg_age
```

![](graphAnalyses_segregation_files/figure-markdown_github/unnamed-chunk-1-1.png)

# Merge segregation df with behavioral df for Accuracy and RT data

``` r
# Prepare df for merge
segregation_results$sub[1:28] <- c(2:9,11:25,27:31)
segregation_results$sub[29:58] <- 1:30
segregation_results$sub <- str_pad(segregation_results$sub, 3, pad = "0")

# Merge seg df with behavioral df
df_categories <- df_categories[-c(4:6, 9:14, 21:23)]
df_categories <- df_categories %>% 
  rename(age = Age, 
         sub = Subj)
behav_df <- merge(df_categories, segregation_results, by = c("age", "sub"))

behav_df$Education <- scale(behav_df$Education, scale = F)
behav_df$S <- scale(behav_df$S, scale = F)
contrasts(behav_df$AgeSum) <- contr.sum(2)/2
behav_df$meanFD <- scale(behav_df$meanFD, scale = F)
behav_df$meanRMSD <- scale(behav_df$meanRMSD, scale = F)
```

# Prepare df for response time

``` r
# prepare df
behav_df.RT = behav_df[!is.na(behav_df$Onset),] # Remove NAs from Onset column and create df
behav_df.RT <- behav_df.RT[behav_df.RT$Correct != 0,] # Remove incorrect answers 
behav_df.RT <- droplevels(behav_df.RT) # drop unused factor levels
contrasts(behav_df.RT$AgeSum) <- contr.sum(2)/2 # re-apply sum coding to factor
behav_df.RT$S <- scale(behav_df.RT$S, scale = F)
```

# Behavioral relevance of segregation

## Accuracy

``` r
seg_acc <- glmer(Correct ~ S * AgeSum + Education + meanRMSD + (1|sub) + (1|Category), data = behav_df, family = binomial(link = "logit"))
summary(seg_acc)
```

    ## Generalized linear mixed model fit by maximum likelihood (Laplace
    ##   Approximation) [glmerMod]
    ##  Family: binomial  ( logit )
    ## Formula: Correct ~ S * AgeSum + Education + meanRMSD + (1 | sub) + (1 |  
    ##     Category)
    ##    Data: behav_df
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##   4813.5   4871.0  -2398.7   4797.5     9829 
    ## 
    ## Scaled residuals: 
    ##      Min       1Q   Median       3Q      Max 
    ## -13.1885   0.1086   0.2067   0.3510   0.6505 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev.
    ##  sub      (Intercept) 0.172    0.4147  
    ##  Category (Intercept) 1.706    1.3063  
    ## Number of obs: 9837, groups:  sub, 31; Category, 20
    ## 
    ## Fixed effects:
    ##             Estimate Std. Error z value           Pr(>|z|)    
    ## (Intercept)  2.86580    0.36712   7.806 0.0000000000000059 ***
    ## S            2.61676    1.01086   2.589            0.00964 ** 
    ## AgeSum1      0.08177    0.13605   0.601            0.54784    
    ## Education   -0.13963    0.06813  -2.049            0.04042 *  
    ## meanRMSD     0.79957    1.50220   0.532            0.59454    
    ## S:AgeSum1   -5.06167    1.60897  -3.146            0.00166 ** 
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Correlation of Fixed Effects:
    ##           (Intr) S      AgeSm1 Eductn mnRMSD
    ## S         -0.303                            
    ## AgeSum1    0.082  0.382                     
    ## Education  0.070 -0.274  0.417              
    ## meanRMSD  -0.522  0.532 -0.201 -0.165       
    ## S:AgeSum1  0.171 -0.402 -0.298 -0.066 -0.103

``` r
drop1(seg_acc, test = "Chisq")
```

    ## Single term deletions
    ## 
    ## Model:
    ## Correct ~ S * AgeSum + Education + meanRMSD + (1 | sub) + (1 | 
    ##     Category)
    ##           npar    AIC    LRT  Pr(Chi)   
    ## <none>         4813.5                   
    ## Education    1 4815.6 4.1387 0.041914 * 
    ## meanRMSD     1 4811.8 0.2769 0.598742   
    ## S:AgeSum     1 4821.0 9.5350 0.002016 **
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
seg_acc_mainEffect <- glmer(Correct ~ S + AgeSum + Education + meanRMSD + (1|sub) + (1|Category), data = behav_df, family = binomial(link = "logit"))
summary(seg_acc_mainEffect)
```

    ## Generalized linear mixed model fit by maximum likelihood (Laplace
    ##   Approximation) [glmerMod]
    ##  Family: binomial  ( logit )
    ## Formula: Correct ~ S + AgeSum + Education + meanRMSD + (1 | sub) + (1 |  
    ##     Category)
    ##    Data: behav_df
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##   4821.0   4871.4  -2403.5   4807.0     9830 
    ## 
    ## Scaled residuals: 
    ##      Min       1Q   Median       3Q      Max 
    ## -13.7514   0.1078   0.2035   0.3500   0.6363 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev.
    ##  sub      (Intercept) 0.2103   0.4586  
    ##  Category (Intercept) 1.7073   1.3066  
    ## Number of obs: 9837, groups:  sub, 31; Category, 20
    ## 
    ## Fixed effects:
    ##             Estimate Std. Error z value            Pr(>|z|)    
    ## (Intercept)  3.08641    0.36586   8.436 <0.0000000000000002 ***
    ## S            1.30548    0.92923   1.405              0.1601    
    ## AgeSum1     -0.04564    0.13097  -0.348              0.7275    
    ## Education   -0.15349    0.06892  -2.227              0.0259 *  
    ## meanRMSD     0.20225    1.52799   0.132              0.8947    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Correlation of Fixed Effects:
    ##           (Intr) S      AgeSm1 Eductn
    ## S         -0.270                     
    ## AgeSum1    0.146  0.294              
    ## Education  0.096 -0.338  0.422       
    ## meanRMSD  -0.521  0.549 -0.252 -0.195

``` r
drop1(seg_acc_mainEffect, test = "Chisq")
```

    ## Single term deletions
    ## 
    ## Model:
    ## Correct ~ S + AgeSum + Education + meanRMSD + (1 | sub) + (1 | 
    ##     Category)
    ##           npar    AIC    LRT Pr(Chi)  
    ## <none>         4821.0                 
    ## S            1 4821.0 1.9388 0.16380  
    ## AgeSum       1 4819.1 0.1194 0.72964  
    ## Education    1 4823.9 4.8700 0.02733 *
    ## meanRMSD     1 4819.0 0.0174 0.89497  
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
emms <- ggpredict(seg_acc, terms = c("S[all]", "AgeSum"))
plot_acc <- plot(emms) +
          scale_fill_manual(values = c("#52257a", "#018571")) +
          scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
          xlab("Segregation") +
          ylab("Accuracy") +
          apatheme +
          theme(plot.title = element_blank(), 
                legend.position = "none",
                text=element_text(family='sans',size=10))
plot_acc
```

![](graphAnalyses_segregation_files/figure-markdown_github/accuracy-1.png)

## RT

``` r
seg_RT <- lmer(log(Onset) ~ S * AgeSum + Education + meanRMSD + (1|sub) + (1|Category), data = behav_df.RT, REML = F)
summary(seg_RT)
```

    ## Linear mixed model fit by maximum likelihood  ['lmerMod']
    ## Formula: log(Onset) ~ S * AgeSum + Education + meanRMSD + (1 | sub) +  
    ##     (1 | Category)
    ##    Data: behav_df.RT
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##   6111.1   6175.2  -3046.6   6093.1     9060 
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -9.9041 -0.6507 -0.1185  0.5357  4.9863 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev.
    ##  sub      (Intercept) 0.017853 0.13362 
    ##  Category (Intercept) 0.002422 0.04922 
    ##  Residual             0.112582 0.33553 
    ## Number of obs: 9069, groups:  sub, 31; Category, 20
    ## 
    ## Fixed effects:
    ##              Estimate Std. Error t value
    ## (Intercept)  6.683512   0.035835 186.506
    ## S           -1.340249   0.116801 -11.475
    ## AgeSum1     -0.025197   0.014276  -1.765
    ## Education    0.023548   0.006884   3.421
    ## meanRMSD    -1.088948   0.173708  -6.269
    ## S:AgeSum1    1.551789   0.179474   8.646
    ## 
    ## Correlation of Fixed Effects:
    ##           (Intr) S      AgeSm1 Eductn mnRMSD
    ## S         -0.492                            
    ## AgeSum1    0.085  0.370                     
    ## Education  0.259 -0.383  0.406              
    ## meanRMSD  -0.655  0.682 -0.196 -0.409       
    ## S:AgeSum1  0.360 -0.620 -0.347  0.055 -0.362

``` r
drop1(seg_RT, test = "Chisq")
```

    ## Single term deletions
    ## 
    ## Model:
    ## log(Onset) ~ S * AgeSum + Education + meanRMSD + (1 | sub) + 
    ##     (1 | Category)
    ##           npar    AIC    LRT               Pr(Chi)    
    ## <none>         6111.1                                 
    ## Education    1 6120.6 11.458             0.0007118 ***
    ## meanRMSD     1 6145.4 36.248        0.000000001737 ***
    ## S:AgeSum     1 6180.3 71.150 < 0.00000000000000022 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
seg_RT_mainEffect <-  lmer(log(Onset) ~ S * AgeSum + Education + meanRMSD + (1|sub) + (1|Category), data = behav_df.RT, REML = F)
summary(seg_RT_mainEffect)
```

    ## Linear mixed model fit by maximum likelihood  ['lmerMod']
    ## Formula: log(Onset) ~ S * AgeSum + Education + meanRMSD + (1 | sub) +  
    ##     (1 | Category)
    ##    Data: behav_df.RT
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##   6111.1   6175.2  -3046.6   6093.1     9060 
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -9.9041 -0.6507 -0.1185  0.5357  4.9863 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev.
    ##  sub      (Intercept) 0.017853 0.13362 
    ##  Category (Intercept) 0.002422 0.04922 
    ##  Residual             0.112582 0.33553 
    ## Number of obs: 9069, groups:  sub, 31; Category, 20
    ## 
    ## Fixed effects:
    ##              Estimate Std. Error t value
    ## (Intercept)  6.683512   0.035835 186.506
    ## S           -1.340249   0.116801 -11.475
    ## AgeSum1     -0.025197   0.014276  -1.765
    ## Education    0.023548   0.006884   3.421
    ## meanRMSD    -1.088948   0.173708  -6.269
    ## S:AgeSum1    1.551789   0.179474   8.646
    ## 
    ## Correlation of Fixed Effects:
    ##           (Intr) S      AgeSm1 Eductn mnRMSD
    ## S         -0.492                            
    ## AgeSum1    0.085  0.370                     
    ## Education  0.259 -0.383  0.406              
    ## meanRMSD  -0.655  0.682 -0.196 -0.409       
    ## S:AgeSum1  0.360 -0.620 -0.347  0.055 -0.362

``` r
drop1(seg_RT_mainEffect, test = "Chisq")
```

    ## Single term deletions
    ## 
    ## Model:
    ## log(Onset) ~ S * AgeSum + Education + meanRMSD + (1 | sub) + 
    ##     (1 | Category)
    ##           npar    AIC    LRT               Pr(Chi)    
    ## <none>         6111.1                                 
    ## Education    1 6120.6 11.458             0.0007118 ***
    ## meanRMSD     1 6145.4 36.248        0.000000001737 ***
    ## S:AgeSum     1 6180.3 71.150 < 0.00000000000000022 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
emms <- ggpredict(seg_RT, terms = c("S[all]", "AgeSum"))
plot_RT <- plot(emms) +
          scale_fill_manual(values = c("#52257a", "#018571")) +
          scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
          #coord_cartesian(ylim = c(500, 1400)) +
          xlab("Segregation") +
          ylab("Response time") +
          apatheme +
          theme(plot.title = element_blank(), 
                legend.position = "none")
plot_RT
```

![](graphAnalyses_segregation_files/figure-markdown_github/RT-1.png)

# NPsy data

## Merge segregation df with Npsy df

``` r
# Merge seg df with behavioral df
NP_Conn_df <- NP_Conn_df[-c(4:6, 17, 18, 20:29)]
NP_Conn_df <- NP_Conn_df %>% 
  rename(age = Age, 
         sub = Subject,
         Education = Bildung)
NP_df <- merge(NP_Conn_df, segregation_results, by = c("age", "sub"))

NP_df$Education <- scale(NP_df$Education, scale = F)
NP_df$S <- scale(NP_df$S, scale = F)
contrasts(NP_df$AgeSum) <- contr.sum(2)/2

# center all Npsy tests
NP_df$Reading_span <- scale(NP_df$Reading_span, scale = F)
NP_df$WST <- scale(NP_df$WST, scale = F)
NP_df$RWT_Vornamen_korrekt <- scale(NP_df$RWT_Vornamen_korrekt, scale = F)
NP_df$RWT_Hobbys_korrekt <- scale(NP_df$RWT_Hobbys_korrekt, scale = F)
NP_df$DSST <- scale(NP_df$DSST, scale = F)
NP_df$TMT_A <- scale(NP_df$TMT_A, scale = F)
NP_df$TMT_B <- scale(NP_df$TMT_B, scale = F)
```

## Analyses with NPsy data and segregation values

``` r
#### Factor analysis ####
NP_factors <- NP_df[,c(6,7,8,9,10,12,13)] # df only with behavioral tests
NP_factors$TMT_A <- ifelse(NP_factors$TMT_A > 0, NP_factors$TMT_A * (-1), abs(NP_factors$TMT_A)) # recode TMT results so that faster RTs (lower numbers) are associated with a better (higher) number
NP_factors$TMT_B <- ifelse(NP_factors$TMT_B > 0, NP_factors$TMT_B * (-1), abs(NP_factors$TMT_B))
NP.fa <- factanal(NP_factors, factors = 2, scores = "regression", rotation = "varimax") # run factor analysis, assume two factors --> looks good, third factor has eigenvalue < 1
NP.fa
```

    ## 
    ## Call:
    ## factanal(x = NP_factors, factors = 2, scores = "regression",     rotation = "varimax")
    ## 
    ## Uniquenesses:
    ##         Reading_span                  WST RWT_Vornamen_korrekt 
    ##                0.798                0.745                0.005 
    ##   RWT_Hobbys_korrekt                 DSST                TMT_A 
    ##                0.714                0.226                0.314 
    ##                TMT_B 
    ##                0.462 
    ## 
    ## Loadings:
    ##                      Factor1 Factor2
    ## Reading_span          0.449         
    ## WST                          -0.501 
    ## RWT_Vornamen_korrekt  0.158   0.985 
    ## RWT_Hobbys_korrekt    0.300   0.443 
    ## DSST                  0.734   0.485 
    ## TMT_A                 0.798   0.219 
    ## TMT_B                 0.714   0.168 
    ## 
    ##                Factor1 Factor2
    ## SS loadings      2.006   1.729
    ## Proportion Var   0.287   0.247
    ## Cumulative Var   0.287   0.534
    ## 
    ## Test of the hypothesis that 2 factors are sufficient.
    ## The chi square statistic is 14.04 on 8 degrees of freedom.
    ## The p-value is 0.0807

``` r
plot(NP.fa$loadings[,1], # plot factors
     NP.fa$loadings[,2],
     xlab = "Factor 1",
     ylab = "Factor 2",
     ylim = c(-1,1),
     xlim = c(-1,1),
     main = "Varimax rotation")
text(NP.fa$loadings[,1]-0.08, 
     NP.fa$loadings[,2]-0.08,
      colnames(NP_factors),
      col="blue")
abline(h = 0, v = 0)
```

![](graphAnalyses_segregation_files/figure-markdown_github/Analyses%20with%20NPsy%20data%20and%20segregation%20values-1.png)

``` r
NP_df <- cbind(NP_df, NP.fa$scores) # bind factor score columns with df for correlation analyses

NP_df$AgeBin <- 1 # create binarised column for age to run partial correlation controlling for age group
NP_df$AgeBin <- ifelse(NP_df$age == "YA", 0, 1)
NP_df$AgeBin <- as.numeric(NP_df$AgeBin)

#### Partial correlation controlling for age ####
pcc <- with(NP_df, pcor.test(S, Factor1, AgeBin)) # r = 0.2507774, p = 0.05989429   
pcc
```

    ##    estimate    p.value statistic  n gp  Method
    ## 1 0.2507774 0.05989429  1.921207 58  1 pearson

``` r
pcc2 <- with(NP_df, pcor.test(S, Factor2, AgeBin)) # r = -0.04438246, p = 0.7430491 
pcc2
```

    ##      estimate   p.value  statistic  n gp  Method
    ## 1 -0.04438246 0.7430491 -0.3294738 58  1 pearson

``` r
############# Only OA
NP_df_OA <- NP_df[NP_df$age == "OA", ] # filter df for OA
NP_df_OA = droplevels(NP_df_OA) # drop unused factor levels

fac1_OA <- with(NP_df_OA, cor.test(Factor1, S)) 
fac1_OA
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  Factor1 and S
    ## t = 0.83547, df = 26, p-value = 0.4111
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.2249535  0.5043462
    ## sample estimates:
    ##       cor 
    ## 0.1616936

``` r
fac2_OA <- with(NP_df_OA, cor.test(Factor2, S)) 
fac2_OA
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  Factor2 and S
    ## t = -0.85959, df = 26, p-value = 0.3979
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.5078170  0.2205192
    ## sample estimates:
    ##        cor 
    ## -0.1662339

``` r
############# only YA
NP_df_YA <- NP_df[NP_df$age == "YA", ] # filter df for YA
NP_df_YA = droplevels(NP_df_YA) # drop unused factor levels

fac1_YA <- with(NP_df_YA, cor.test(Factor1, S)) 
fac1_YA
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  Factor1 and S
    ## t = 2.6387, df = 28, p-value = 0.01344
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  0.1024634 0.6948198
    ## sample estimates:
    ##       cor 
    ## 0.4462592

``` r
fac2_YA <- with(NP_df_YA, cor.test(Factor2, S)) 
fac2_YA
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  Factor2 and S
    ## t = 0.86185, df = 28, p-value = 0.3961
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.2117791  0.4925012
    ## sample estimates:
    ##       cor 
    ## 0.1607553

``` r
cor_plot_YA <- ggscatter(NP_df, x = "S", y = "Factor1", # plot for significant correlation for factor 1 and segregation in YA
          color = "age", palette = c("#52257a", "#018571"),
          add = "reg.line", conf.int = TRUE, 
          cor.coef = F, cor.method = "pearson",
          xlab = "Segregation", ylab = "Executive functions") +
  stat_cor(aes(color = age)) +
  apatheme
cor_plot_YA
```

    ## `geom_smooth()` using formula 'y ~ x'

![](graphAnalyses_segregation_files/figure-markdown_github/Analyses%20with%20NPsy%20data%20and%20segregation%20values-2.png)

# Spring-embedded plots

``` r
#### OA ####
diag(OA) <- 0
OA[OA < 0] <- 0 # make neg values 0

OA_thr <- OA
OA_thr <- as.matrix(OA_thr)
OA_thr[OA_thr < quantile(OA_thr, 0.95)] <- 0

net_OA <- graph.adjacency(adjmatrix = OA_thr, mode = "undirected", diag = F, weighted = T) # make graph from matrix
V(net_OA)$community <- ROI_labels_sorted[,2] # set network labels for plotting as attribute of nodes
net_OA <- delete.vertices(net_OA, igraph::degree(net_OA) == 0) # remove nodes that have no edges
net_OA <- set_edge_attr(net_OA, "curved", value = 0.2)

# Set layout for network in advance so nodes stay in the same place across plotting runs
l_OA <- layout_with_fr(net_OA)

plot.igraph(net_OA, layout = l_OA, vertex.label = NA, vertex.color = pal[factor(vertex_attr(net_OA, "community"), levels = c("DMN_A", "DMN_A_C", "DMN_B", "SemNet", "Control_B", "VAN_B", "DAN_A"))], vertex.size = 7)

legend(x="bottomleft", y="bottomleft", c(unique(ROI_labels_sorted[,2])), pch = 21,
       pt.bg=pal, pt.cex=2, cex=.8, bty="n", ncol=1)
```

![](graphAnalyses_segregation_files/figure-markdown_github/spring-embedded%20plot-1.png)

``` r
#### YA ####
diag(YA) <- 0
YA[YA < 0] <- 0 # make neg values 0

YA_thr <- YA
YA_thr <- as.matrix(YA_thr)
YA_thr[YA_thr < quantile(YA_thr, 0.95)] <- 0


net_YA <- graph.adjacency(adjmatrix = YA_thr, mode = "undirected", diag = F, weighted = T) # make graph from matrix
V(net_YA)$community <- ROI_labels_sorted[,2] # set network labels for plotting as attribute of nodes
net_YA <- delete.vertices(net_YA, igraph::degree(net_YA) == 0) # remove nodes that have no edges
net_YA <- set_edge_attr(net_YA, "curved", value = 0.2)

# Set layout for network in advance so nodes stay in the same place across plotting runs
l_YA <- layout_with_fr(net_YA)

plot.igraph(net_YA, layout = l_YA, vertex.label = NA, vertex.color = pal[factor(vertex_attr(net_YA, "community"), levels = c("DMN_A", "DMN_A_C", "DMN_B", "SemNet", "Control_B", "VAN_B", "DAN_A"))], vertex.size = 7)

legend(x="bottomleft", y="bottomleft", c(unique(ROI_labels_sorted[,2])), pch = 21,
       col="#777777", pt.bg = pal, pt.cex=2, cex=.8, bty="n", ncol=1)
```

![](graphAnalyses_segregation_files/figure-markdown_github/spring-embedded%20plot-2.png)
