# Load packages

``` r
library(psych)
library(reshape2)
library(plyr)
library(tidyverse)
library(rstatix)
library(lme4)
library(emmeans)
library(ggeffects)
library(stringr)
library(ppcor)
library(ggpubr)
library(see)
library(igraph)
library(RColorBrewer)
library(here)
```

# Load data

``` r
# Load individual matrices for 104 ROIs for efficiency analysis
load(here::here("cPPI/RData/all_subjects_104ROIs_cPPI_indivMatrices_3DArray.RData")) # 104 ROIs

# Load OMSTs
load(here::here("GraphMeasures/RData/all_subjects_OMST_104ROIs_indivMatrices_3DArray.RData")) # OMST calculation in Matlab acc. to Dimitriadis et al., 2017

# Load efficiency results
load(here::here("GraphMeasures/RData/eff_OMST_104ROIs.RData"))

# Load ROI labels
load(here::here("cPPI/RData/ROI_labels104.RData"))
load(here::here("cPPI/RData/ROI_labels104_sorted.RData"))

# Merged df with efficiency values and behavior
load(here::here("GraphMeasures/RData/behav_df_efficiency_bin_01_2022.RData"))

# Merged df with mean efficiency values and NPsy data
load(here::here("GraphMeasures/RData/NP_df_efficiency_bin_01_2022.RData"))
```

# Read in data

``` r
# define path for individual cPPI matrices
path = here::here("GraphAnalyses/Data/OMST/") 

# list files
OMST_files <- list.files(path = path, pattern = "*OMST.txt", full.names = T) 

# read files into list of dataframes
my_list <- lapply(OMST_files, read.table, header = F, sep = ",")

# array bind dfs along third dimension
all_subjects_OMST_104ROIs = do.call(abind, c(my_list, list(along = 3)))
#arr <- abind(my_list, along = 3) # alternative way

save(all_subjects_OMST_104ROIs, file = here::here("GraphMeasures/RData/all_subjects_OMST_104ROIs_indivMatrices_3DArray.RData"))

##
eff_OMST <- read.table(paste0(path, "Efficiency.txt"), header = F, sep = ",")
eff_OMST <- eff_OMST %>% 
  rename(EffGlobal = V1,
         mean_degree = V2,
         global_cost_eff_max = V3,
         cost_max = V4)
eff_OMST_norm$age <- "OA"
eff_OMST_norm[29:58,]$age <- "YA"

n <- eff_OMST %>% 
  group_by(age) %>% 
  tally()
sub_0A <- c(2:9,11:25,27:31)
sub_0A <- rep(sub_0A, each = (n$n[n$age =="OA"]/28))
sub_YA <- c(1:30)
sub_YA <- rep(sub_YA, each = (n$n[n$age =="YA"]/30))

eff_OMST_norm$sub[eff_OMST_norm$age == "OA"] <- sub_0A
eff_OMST_norm$sub[eff_OMST_norm$age == "YA"] <- sub_YA
eff_OMST_norm$sub <- str_pad(eff_OMST_norm$sub, 3, pad = "0")

save(eff_OMST_norm, file = here::here("GraphMeasures/RData/eff_OMST_norm_104ROIs.RData"))
```

# Global efficiency - OMST

``` r
#### Linear model to test effect of age on efficiency values ####
m_eg <- lm(EffGlobal ~ age + meanRMSD, data = eff_OMST)
summary(m_eg)
```

    ## 
    ## Call:
    ## lm(formula = EffGlobal ~ age + meanRMSD, data = eff_OMST)
    ## 
    ## Residuals:
    ##       Min        1Q    Median        3Q       Max 
    ## -0.037861 -0.008382  0.001922  0.007241  0.025582 
    ## 
    ## Coefficients:
    ##              Estimate Std. Error t value             Pr(>|t|)    
    ## (Intercept)  0.112089   0.006551  17.110 < 0.0000000000000002 ***
    ## ageYA        0.020169   0.004117   4.899           0.00000887 ***
    ## meanRMSD    -0.074813   0.038426  -1.947               0.0567 .  
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.01261 on 55 degrees of freedom
    ## Multiple R-squared:  0.5235, Adjusted R-squared:  0.5062 
    ## F-statistic: 30.21 on 2 and 55 DF,  p-value: 0.000000001402

``` r
drop1(m_eg, test = "Chisq")
```

    ## Single term deletions
    ## 
    ## Model:
    ## EffGlobal ~ age + meanRMSD
    ##          Df Sum of Sq       RSS     AIC   Pr(>Chi)    
    ## <none>                0.0087407 -504.41               
    ## age       1 0.0038138 0.0125546 -485.41 0.00000459 ***
    ## meanRMSD  1 0.0006024 0.0093431 -502.55    0.04928 *  
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
emms <- ggpredict(m_eg, terms = c("age"))
plot(emms) +
  apatheme +
  theme(plot.title = element_blank())
```

![](OMST_files/figure-markdown_github/global%20efficiency-1.png)

``` r
#### Plot ####
eff_OMST$age <- factor(eff_OMST$age, levels = c("YA", "OA"))
eff_plot <- ggplot(eff_OMST, aes(x = age, y = EffGlobal, fill = age, color = age)) +
  geom_violinhalf(flip = 1, trim = F) +
  stat_summary(fun.data = "mean_sdl",  fun.args = list(mult = 1), 
    geom = "pointrange", color = "grey", size = 1, shape = 20, position = position_dodge(width = 1)) +
  ylab("Global efficiency") +
  scale_fill_manual(values = c("#018571", "#52257a")) +
  scale_color_manual(values = c("#018571", "#52257a")) +
  apatheme +
  theme(axis.text.x = element_blank(),
        axis.ticks.x = element_blank())
eff_plot
```

![](OMST_files/figure-markdown_github/global%20efficiency-2.png)

# Merge global efficiency df with behavioral df

``` r
efficiency_results <- eff_OMST[,-c(4:6)]

# Merge efficiency df with behavioral df
behav_df_ge <- behav_df_ge[-c(15:16)]
behav_df_ge <- merge(behav_df_ge, efficiency_results, by = c("age", "sub"))

behav_df_ge$EffGlobal <- scale(behav_df_ge$EffGlobal, scale = F)
```

# Behavioral relevance of global efficiency

## Accuracy

``` r
ge_acc <- glmer(Correct ~ EffGlobal * AgeSum + Education + meanRMSD + (1|sub) + (1|Category), data = behav_df_ge, family = binomial(link = "logit"))
summary(ge_acc)
```

    ## Generalized linear mixed model fit by maximum likelihood (Laplace
    ##   Approximation) [glmerMod]
    ##  Family: binomial  ( logit )
    ## Formula: Correct ~ EffGlobal * AgeSum + Education + meanRMSD + (1 | sub) +  
    ##     (1 | Category)
    ##    Data: behav_df_ge
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##   4814.4   4871.9  -2399.2   4798.4     9829 
    ## 
    ## Scaled residuals: 
    ##      Min       1Q   Median       3Q      Max 
    ## -12.5668   0.1067   0.2063   0.3525   0.6371 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev.
    ##  sub      (Intercept) 0.2386   0.4885  
    ##  Category (Intercept) 1.7104   1.3078  
    ## Number of obs: 9837, groups:  sub, 31; Category, 20
    ## 
    ## Fixed effects:
    ##                    Estimate Std. Error z value            Pr(>|z|)    
    ## (Intercept)         2.93569    0.37093   7.914 0.00000000000000248 ***
    ## EffGlobal          13.61529    4.43001   3.073             0.00212 ** 
    ## AgeSum1             0.13521    0.14858   0.910             0.36282    
    ## Education          -0.09788    0.06771  -1.446             0.14830    
    ## meanRMSD            0.93530    1.42017   0.659             0.51016    
    ## EffGlobal:AgeSum1 -12.47509    7.86912  -1.585             0.11289    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Correlation of Fixed Effects:
    ##             (Intr) EffGlb AgeSm1 Eductn mnRMSD
    ## EffGlobal   -0.124                            
    ## AgeSum1      0.136  0.529                     
    ## Education   -0.058  0.018  0.489              
    ## meanRMSD    -0.519  0.297 -0.240  0.065       
    ## EffGlbl:AS1  0.275 -0.063 -0.008 -0.219 -0.311

``` r
drop1(ge_acc, test = "Chisq")
```

    ## Single term deletions
    ## 
    ## Model:
    ## Correct ~ EffGlobal * AgeSum + Education + meanRMSD + (1 | sub) + 
    ##     (1 | Category)
    ##                  npar    AIC     LRT Pr(Chi)
    ## <none>                4814.4                
    ## Education           1 4814.4 2.00615  0.1567
    ## meanRMSD            1 4812.8 0.40586  0.5241
    ## EffGlobal:AgeSum    1 4814.2 1.79350  0.1805

``` r
ge_acc_mainEffect <- glmer(Correct ~ EffGlobal + AgeSum + Education + meanRMSD + (1|sub) + (1|Category), data = behav_df_ge, family = binomial(link = "logit"))
drop1(ge_acc_mainEffect, test = "Chisq")
```

    ## Single term deletions
    ## 
    ## Model:
    ## Correct ~ EffGlobal + AgeSum + Education + meanRMSD + (1 | sub) + 
    ##     (1 | Category)
    ##           npar    AIC    LRT  Pr(Chi)   
    ## <none>         4814.2                   
    ## EffGlobal    1 4821.0 8.7936 0.003023 **
    ## AgeSum       1 4813.0 0.8363 0.360466   
    ## Education    1 4815.4 3.2854 0.069900 . 
    ## meanRMSD     1 4812.2 0.0342 0.853180   
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
emms <- ggpredict(ge_acc, terms = c("EffGlobal[all]"))
plot_acc <- plot(emms) +
  scale_fill_manual(values = c("#52257a", "#018571")) +
  scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
  coord_cartesian(ylim = c(0.85, 1)) +
  xlab("Global efficiency") +
  ylab("Accuracy") +
  apatheme +
  theme(plot.title = element_blank())
plot_acc
```

![](OMST_files/figure-markdown_github/unnamed-chunk-2-1.png)

## RT

``` r
# prepare df
behav_df_ge.RT <-  behav_df_ge[!is.na(behav_df_ge$Onset),] # Remove NAs from Onset column and create df
behav_df_ge.RT <- behav_df_ge.RT[behav_df_ge.RT$Correct != 0,] # Remove incorrect answers
behav_df_ge.RT <- droplevels(behav_df_ge.RT) # drop unused factor levels
contrasts(behav_df_ge.RT$AgeSum) <- contr.sum(2)/2 # re-apply sum coding to factor
behav_df_ge.RT$EffGlobal <- scale(behav_df_ge.RT$EffGlobal, scale = F)

# model
ge_RT <- lmer(log(Onset) ~ EffGlobal * AgeSum + Education + meanRMSD + (1|sub) + (1|Category), data = behav_df_ge.RT, REML = F)
summary(ge_RT)
```

    ## Linear mixed model fit by maximum likelihood  ['lmerMod']
    ## Formula: log(Onset) ~ EffGlobal * AgeSum + Education + meanRMSD + (1 |  
    ##     sub) + (1 | Category)
    ##    Data: behav_df_ge.RT
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##   6194.4   6258.4  -3088.2   6176.4     9060 
    ## 
    ## Scaled residuals: 
    ##     Min      1Q  Median      3Q     Max 
    ## -9.8170 -0.6473 -0.1226  0.5190  4.9113 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev.
    ##  sub      (Intercept) 0.009756 0.09877 
    ##  Category (Intercept) 0.002434 0.04934 
    ##  Residual             0.113852 0.33742 
    ## Number of obs: 9069, groups:  sub, 31; Category, 20
    ## 
    ## Fixed effects:
    ##                    Estimate Std. Error t value
    ## (Intercept)        6.393982   0.029416 217.363
    ## EffGlobal          0.658398   0.443514   1.485
    ## AgeSum1            0.044719   0.015042   2.973
    ## Education          0.007501   0.006376   1.176
    ## meanRMSD           0.629236   0.144655   4.350
    ## EffGlobal:AgeSum1 -5.756678   0.884886  -6.506
    ## 
    ## Correlation of Fixed Effects:
    ##             (Intr) EffGlb AgeSm1 Eductn mnRMSD
    ## EffGlobal   -0.230                            
    ## AgeSum1      0.220  0.496                     
    ## Education    0.016 -0.041  0.483              
    ## meanRMSD    -0.670  0.399 -0.307 -0.092       
    ## EffGlbl:AS1  0.367  0.000  0.052 -0.276 -0.298

``` r
drop1(ge_RT, test = "Chisq")
```

    ## Single term deletions
    ## 
    ## Model:
    ## log(Onset) ~ EffGlobal * AgeSum + Education + meanRMSD + (1 | 
    ##     sub) + (1 | Category)
    ##                  npar    AIC    LRT         Pr(Chi)    
    ## <none>                6194.4                           
    ## Education           1 6193.8  1.384          0.2394    
    ## meanRMSD            1 6211.3 18.885 0.0000138865203 ***
    ## EffGlobal:AgeSum    1 6234.2 41.787 0.0000000001018 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
emms <- ggpredict(ge_RT, terms = c("EffGlobal[all]", "AgeSum"))
plot_RT <- plot(emms) +
          scale_fill_manual(values = c("#52257a", "#018571")) +
          scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
          coord_cartesian(ylim = c(500, 800)) +
          xlab("Global efficiency") +
          ylab("Reaction time") +
          apatheme +
          theme(plot.title = element_blank())
plot_RT
```

![](OMST_files/figure-markdown_github/RT-1.png)

``` r
NP_df <- NP_df[-16]
NP_df <- merge(NP_df, efficiency_results, by = c("age", "sub"))


NP_df$Education <- scale(NP_df$Education, scale = F)
NP_df$EffGlobal <- scale(NP_df$EffGlobal, scale = F)
contrasts(NP_df$AgeSum) <- contr.sum(2)/2

# center all Npsy tests
NP_df$Reading_span <- scale(NP_df$Reading_span, scale = F)
NP_df$WST <- scale(NP_df$WST, scale = F)
NP_df$RWT_Vornamen_korrekt <- scale(NP_df$RWT_Vornamen_korrekt, scale = F)
NP_df$RWT_Hobbys_korrekt <- scale(NP_df$RWT_Hobbys_korrekt, scale = F)
NP_df$DSST <- scale(NP_df$DSST, scale = F)
NP_df$TMT_A <- scale(NP_df$TMT_A, scale = F)
NP_df$TMT_B <- scale(NP_df$TMT_B, scale = F)
```

## Analyses with NPsy data and mean efficiency values

``` r
#### Factor analysis ####
NP_factors <- NP_df[,c(6,7,8,9,10,12,13)] # df only with behavioral tests
NP_factors$TMT_A <- ifelse(NP_factors$TMT_A > 0, NP_factors$TMT_A * (-1), abs(NP_factors$TMT_A))
NP_factors$TMT_B <- ifelse(NP_factors$TMT_B > 0, NP_factors$TMT_B * (-1), abs(NP_factors$TMT_B))
NP.fa <- factanal(NP_factors, factors = 2, scores = "regression", rotation = "varimax") # run factor analysis, assume two factors --> looks good, third factor has eigenvalue < 1
NP.fa
```

    ## 
    ## Call:
    ## factanal(x = NP_factors, factors = 2, scores = "regression",     rotation = "varimax")
    ## 
    ## Uniquenesses:
    ##         Reading_span                  WST RWT_Vornamen_korrekt 
    ##                0.798                0.745                0.005 
    ##   RWT_Hobbys_korrekt                 DSST                TMT_A 
    ##                0.714                0.226                0.314 
    ##                TMT_B 
    ##                0.462 
    ## 
    ## Loadings:
    ##                      Factor1 Factor2
    ## Reading_span          0.449         
    ## WST                          -0.501 
    ## RWT_Vornamen_korrekt  0.158   0.985 
    ## RWT_Hobbys_korrekt    0.300   0.443 
    ## DSST                  0.734   0.485 
    ## TMT_A                 0.798   0.219 
    ## TMT_B                 0.714   0.168 
    ## 
    ##                Factor1 Factor2
    ## SS loadings      2.006   1.729
    ## Proportion Var   0.287   0.247
    ## Cumulative Var   0.287   0.534
    ## 
    ## Test of the hypothesis that 2 factors are sufficient.
    ## The chi square statistic is 14.04 on 8 degrees of freedom.
    ## The p-value is 0.0807

``` r
plot(NP.fa$loadings[,1], # plot factors
     NP.fa$loadings[,2],
     xlab = "Factor 1",
     ylab = "Factor 2",
     ylim = c(-1,1),
     xlim = c(-1,1),
     main = "Varimax rotation")
text(NP.fa$loadings[,1]-0.08, 
     NP.fa$loadings[,2]-0.08,
      colnames(NP_factors),
      col="blue")
abline(h = 0, v = 0)
```

![](OMST_files/figure-markdown_github/Analyses%20with%20NPsy%20data%20and%20segregation%20values-1.png)

``` r
NP_df <- cbind(NP_df, NP.fa$scores) # bind factor score columns with df for correlation analyses

NP_df$AgeBin <- 1 # create binarised column for age to run partial correlation controlling for age group
NP_df$AgeBin <- ifelse(NP_df$age == "YA", 0, 1)
NP_df$AgeBin <- as.numeric(NP_df$AgeBin)

#### Partial correlation controlling for age ####
pcc <- with(NP_df, pcor.test(EffGlobal, Factor1, AgeBin)) 
pcc
```

    ##    estimate   p.value statistic  n gp  Method
    ## 1 0.1057782 0.4335592 0.7888979 58  1 pearson

``` r
pcc2 <- with(NP_df, pcor.test(EffGlobal, Factor2, AgeBin)) 
pcc2
```

    ##      estimate  p.value  statistic  n gp  Method
    ## 1 -0.09344761 0.489315 -0.6960719 58  1 pearson

``` r
############# Only OA
NP_df_OA <- NP_df[NP_df$age == "OA", ] # filter df for OA
NP_df_OA = droplevels(NP_df_OA) # drop unused factor levels

fac1_OA <- with(NP_df_OA, cor.test(Factor1, EffGlobal))
fac1_OA
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  Factor1 and EffGlobal
    ## t = 0.073291, df = 26, p-value = 0.9421
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.3606386  0.3853826
    ## sample estimates:
    ##        cor 
    ## 0.01437201

``` r
fac2_OA <- with(NP_df_OA, cor.test(Factor2, EffGlobal))
fac2_OA
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  Factor2 and EffGlobal
    ## t = -1.981, df = 26, p-value = 0.05827
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.64770085  0.01265717
    ## sample estimates:
    ##        cor 
    ## -0.3621298

``` r
############# only YA
NP_df_YA <- NP_df[NP_df$age == "YA", ] # filter df for YA
NP_df_YA = droplevels(NP_df_YA) # drop unused factor levels

fac1_YA <- with(NP_df_YA, cor.test(Factor1, EffGlobal))
fac1_YA
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  Factor1 and EffGlobal
    ## t = 1.1026, df = 28, p-value = 0.2796
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.1686723  0.5256323
    ## sample estimates:
    ##       cor 
    ## 0.2039931

``` r
fac2_YA <- with(NP_df_YA, cor.test(Factor2, EffGlobal))
fac2_YA
```

    ## 
    ##  Pearson's product-moment correlation
    ## 
    ## data:  Factor2 and EffGlobal
    ## t = 0.7533, df = 28, p-value = 0.4576
    ## alternative hypothesis: true correlation is not equal to 0
    ## 95 percent confidence interval:
    ##  -0.2310617  0.4769894
    ## sample estimates:
    ##     cor 
    ## 0.14094
