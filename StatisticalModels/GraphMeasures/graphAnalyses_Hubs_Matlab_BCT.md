# Load packages

``` r
library(psych)
library(reshape2)
library(plyr)
library(tidyverse)
library(paletteer) 
library(rstatix)
library(lme4)
library(emmeans)
library(ggeffects)
library(igraph)
library(ForceAtlas2)
library(RColorBrewer)
library(ppcor)
library(here)
```

# Load data

``` r
# load df with normalized PC from BCT in Matlab
load(here("GraphMeasures/RData/PC_norm_Matlab_BCT.RData"))

# Load ROI labels
load(here("cPPI/RData/ROI_labels104.RData"))
load(here("cPPI/RData/ROI_labels104_sorted.RData"))

# Load average OMSTs per age group
load(here("cPPI/RData/YA_OMST_104ROIs.RData"))
load(here("cPPI/RData/OA_OMST_104ROIs.RData"))

# Color palette for spring-embedded plots
load(here::here("GraphMeasures/RData/colorPalette_springEmbeddedPlots.RData"))
```

# Normalized participation coefficient

``` r
PC$sub <- seq(1, 58, by = 1)
YA_subs <- seq(29, 58, by = 1)
PC$age <- "OA"
PC$age[PC$sub %in% YA_subs] <- "YA"

PC_long <- gather(PC, key = "ROI", value = "PC_norm", 1:104)

PC_summary <- PC_long %>% 
  group_by(age, ROI) %>% 
  summarise(mean_PC = mean(PC_norm))

OA_df_PC <- filter(PC_summary, age == "OA")
YA_df_PC <- filter(PC_summary, age == "YA")

OA_df_PC <- OA_df_PC %>%
  mutate(ROI = factor(ROI, levels = ROI_labels104_sorted[,4])) %>%
  arrange(ROI)

YA_df_PC <- YA_df_PC %>%
  mutate(ROI = factor(ROI, levels = ROI_labels104_sorted[,4])) %>%
  arrange(ROI)

#### Linear model to test age effect on nodal efficiency/closeness centrality ####
# After MCC, only significantly higher PC nodes for OA than YA
PC_long$age <- as.factor(PC_long$age)

lm_PC <- PC_long %>% 
  nest(data = -ROI) %>% 
  mutate(model = map(data, ~lm(PC_norm ~ age, data = .)), tidied = map(model, tidy)) %>% 
  unnest(tidied)

lm_PC_corr <- lm_PC[lm_PC$term == "ageYA",]
p_corr <- p.adjust(lm_PC_corr$p.value, method = "holm")
lm_PC_corr <- cbind(lm_PC_corr, p_corr)

PC_sigNodes <- lm_PC_corr[lm_PC_corr$p_corr < 0.05, ]
PC_sigNodes <- PC_sigNodes[-c(2,3)]
PC_sigNodes
```

    ##                                                   ROI  term   estimate
    ## 12                           Control_B__Frontal_Pole3 ageYA -0.1335146
    ## 78  SemNet__Superior_Temporal_Gyrus_anterior_division ageYA -0.1554682
    ## 102        DAN_A__Temporal_Occipital_Fusiform_Cortex1 ageYA -0.2136598
    ## 104        DAN_A__Temporal_Occipital_Fusiform_Cortex2 ageYA -0.1713402
    ##      std.error statistic       p.value      p_corr
    ## 12  0.03408371 -3.917253 0.00024583897 0.024829736
    ## 78  0.03630902 -4.281806 0.00007335020 0.007555070
    ## 102 0.04545555 -4.700412 0.00001732874 0.001802189
    ## 104 0.04273097 -4.009742 0.00018171662 0.018535095

# Hubs: Nodes with highest PC_norm

``` r
# Based on 1SD above mean
OA_hubs_PC <- OA_df_PC[OA_df_PC$mean_PC > (mean(OA_df_PC$mean_PC) + SD(OA_df_PC$mean_PC)), ]
OA_hubs_PC
```

    ## # A tibble: 15 × 3
    ## # Groups:   age [1]
    ##    age   ROI                                                   mean_PC
    ##    <chr> <fct>                                                   <dbl>
    ##  1 OA    DMN_A__Lateral_Occipital_Cortex_superior_division1      0.558
    ##  2 OA    DMN_A__Angular_Gyrus3                                   0.538
    ##  3 OA    DMN_A_C__Lateral_Occipital_Cortex_superior_division1    0.562
    ##  4 OA    DMN_A_C__Lateral_Occipital_Cortex_superior_division5    0.589
    ##  5 OA    DMN_B__Angular_Gyrus                                    0.562
    ##  6 OA    DMN_B__Middle_Temporal_Gyrus_posterior_division2        0.545
    ##  7 OA    SemNet__Paracingulate_Gyrus2                            0.561
    ##  8 OA    SemNet__Inferior_Temporal_Gyrus_temporooccipital_part   0.534
    ##  9 OA    Control_B__Frontal_Pole1                                0.537
    ## 10 OA    Control_B__Frontal_Pole3                                0.556
    ## 11 OA    Control_B__Middle_Frontal_Gyrus3                        0.549
    ## 12 OA    Control_B__Angular_Gyrus1                               0.556
    ## 13 OA    Control_B__Angular_Gyrus2                               0.613
    ## 14 OA    Control_B__Superior_Frontal_Gyrus1                      0.543
    ## 15 OA    Control_B__Paracingulate_Gyrus                          0.574

``` r
YA_hubs_PC <- YA_df_PC[YA_df_PC$mean_PC > (mean(YA_df_PC$mean_PC) + SD(YA_df_PC$mean_PC)), ]
YA_hubs_PC
```

    ## # A tibble: 13 × 3
    ## # Groups:   age [1]
    ##    age   ROI                                                    mean_PC
    ##    <chr> <fct>                                                    <dbl>
    ##  1 YA    DMN_A__Lateral_Occipital_Cortex_superior_division1       0.633
    ##  2 YA    DMN_A__Angular_Gyrus3                                    0.625
    ##  3 YA    DMN_A_C__Lateral_Occipital_Cortex_superior_division1     0.540
    ##  4 YA    DMN_A_C__Lateral_Occipital_Cortex_superior_division5     0.512
    ##  5 YA    DMN_B__Angular_Gyrus                                     0.581
    ##  6 YA    DMN_B__Middle_Temporal_Gyrus_posterior_division2         0.515
    ##  7 YA    DMN_B__Middle_Temporal_Gyrus_posterior_division4         0.514
    ##  8 YA    SemNet__Paracingulate_Gyrus2                             0.524
    ##  9 YA    Control_B__Middle_Frontal_Gyrus3                         0.532
    ## 10 YA    Control_B__Angular_Gyrus1                                0.584
    ## 11 YA    Control_B__Angular_Gyrus2                                0.532
    ## 12 YA    Control_B__Lateral_Occipital_Cortex_superior_division2   0.575
    ## 13 YA    Control_B__Frontal_Pole6                                 0.516

``` r
## Common hubs
hubs <- inner_join(OA_hubs_PC, YA_hubs_PC, by = "ROI")
hubs
```

    ## # A tibble: 10 × 5
    ##    age.x ROI                                           mean_PC.x age.y mean_PC.y
    ##    <chr> <fct>                                             <dbl> <chr>     <dbl>
    ##  1 OA    DMN_A__Lateral_Occipital_Cortex_superior_div…     0.558 YA        0.633
    ##  2 OA    DMN_A__Angular_Gyrus3                             0.538 YA        0.625
    ##  3 OA    DMN_A_C__Lateral_Occipital_Cortex_superior_d…     0.562 YA        0.540
    ##  4 OA    DMN_A_C__Lateral_Occipital_Cortex_superior_d…     0.589 YA        0.512
    ##  5 OA    DMN_B__Angular_Gyrus                              0.562 YA        0.581
    ##  6 OA    DMN_B__Middle_Temporal_Gyrus_posterior_divis…     0.545 YA        0.515
    ##  7 OA    SemNet__Paracingulate_Gyrus2                      0.561 YA        0.524
    ##  8 OA    Control_B__Middle_Frontal_Gyrus3                  0.549 YA        0.532
    ##  9 OA    Control_B__Angular_Gyrus1                         0.556 YA        0.584
    ## 10 OA    Control_B__Angular_Gyrus2                         0.613 YA        0.532

# Force Atlas2 layout

``` r
# Plots are based on mean OMST matrices in each age group
# Hub selection is based on strongest PC node in each age group (both in AG, albeit different networks)
#### OA ####
OA_thr <- OA
OA_thr <- as.matrix(OA_thr)
OA_thr[OA_thr < quantile(OA_thr, 0.95)] <- 0

net_OA <- graph.adjacency(adjmatrix = OA_thr, mode = "undirected", diag = F, weighted = T) # make graph from matrix
V(net_OA)$community <- ROI_labels104_sorted[,2] # set network labels for plotting as attribute of nodes
V(net_OA)$PC <- OA_df_PC$mean_PC
net_OA <- delete.vertices(net_OA, igraph::degree(net_OA) == 0) # remove nodes that have no edges

layout <- layout.forceatlas2(net_OA, iterations=2000, plotstep=2000, nohubs = T, k = 2000)
```

![](graphAnalyses_Hubs_Matlab_BCT_files/figure-markdown_github/force%20atlas2%20plot-1.png)

``` r
fine <- 8
pale <- colorRampPalette(palet.positive)
V(net_OA)$color <- pale(fine)[cut(as.numeric(V(net_OA)$PC), breaks = fine)]
net_OA <- set_edge_attr(net_OA, "curved", value = 0.3)
sizes <- ifelse(V(net_OA)$PC > (mean(V(net_OA)$PC) + SD(V(net_OA)$PC)), V(net_OA)$PC*20, V(net_OA)$PC*15) 

## Plot OMST (5%) with selected hub
plot(net_OA, layout=layout, vertex.label = NA, vertex.shape="circle", vertex.size = sizes)
```

![](graphAnalyses_Hubs_Matlab_BCT_files/figure-markdown_github/force%20atlas2%20plot-2.png)

``` r
# find all neighbors of selected connector hub 
hub_id <- which(V(net_OA)$name == "Control_B__Angular_Gyrus2")
neighbors <- neighbors(net_OA, hub_id)
neighbors_id <- match(neighbors$name, V(net_OA)$name)
sub_OA <- induced_subgraph(net_OA, vids = c(hub_id, neighbors_id))
size <- ifelse(V(sub_OA)$name == "Control_B__Angular_Gyrus2", 14, 7)
V(sub_OA)$color = pal[factor(vertex_attr(sub_OA, "community"), levels = c("DMN_A", "DMN_A_C", "DMN_B", "SemNet", "Control_B", "VAN_B", "DAN_A"))]
edge.end <- ends(sub_OA, es=E(sub_OA), names=F)[,1]
edge.col <- V(sub_OA)$color[edge.end]

## Subplot of hub with its neighbors
plot(sub_OA, vertex.shape="circle", vertex.size = size, vertex.label = NA, edge.color = edge.col)
```

![](graphAnalyses_Hubs_Matlab_BCT_files/figure-markdown_github/force%20atlas2%20plot-3.png)

``` r
#### YA ####
YA_thr <- YA
YA_thr <- as.matrix(YA_thr)
YA_thr[YA_thr < quantile(YA_thr, 0.95)] <- 0

net_YA <- graph.adjacency(adjmatrix = YA_thr, mode = "undirected", diag = F, weighted = T) # make graph from matrix
V(net_YA)$community <- ROI_labels104_sorted[,2] # set network labels for plotting as attribute of nodes
V(net_YA)$PC <- YA_df_PC$mean_PC
net_YA <- delete.vertices(net_YA, igraph::degree(net_YA) == 0) # remove nodes that have no edges

layout <- layout.forceatlas2(net_YA, iterations=2000, plotstep=2000, nohubs = T, k = 2000)
```

![](graphAnalyses_Hubs_Matlab_BCT_files/figure-markdown_github/force%20atlas2%20plot-4.png)

``` r
fine <- 8
pale <- colorRampPalette(palet.positive)
V(net_YA)$color <- pale(fine)[cut(as.numeric(V(net_YA)$PC), breaks = fine)]
net_YA <- set_edge_attr(net_YA, "curved", value = 0.3)
sizes <- ifelse(V(net_YA)$PC > (mean(V(net_YA)$PC) + SD(V(net_YA)$PC)), V(net_YA)$PC*20, V(net_YA)$PC*15)

## Plot OMST (5%) with selected hub
plot(net_YA, layout=layout, vertex.label = NA, vertex.shape="circle", vertex.size = sizes)
```

![](graphAnalyses_Hubs_Matlab_BCT_files/figure-markdown_github/force%20atlas2%20plot-5.png)

``` r
# find all neighbors of selected connector hub 
hub_id <- which(V(net_YA)$name == "DMN_A__Lateral_Occipital_Cortex_superior_division1")
neighbors <- neighbors(net_YA, hub_id)
neighbors_id <- match(neighbors$name, V(net_YA)$name)
sub_YA <- induced_subgraph(net_YA, vids = c(hub_id, neighbors_id))
size <- ifelse(V(sub_YA)$name == "DMN_A__Lateral_Occipital_Cortex_superior_division1", 14, 7)
V(sub_YA)$color = pal[factor(vertex_attr(sub_YA, "community"), levels = c("DMN_A", "DMN_A_C", "DMN_B", "SemNet", "Control_B", "VAN_B", "DAN_A"))]
edge.end <- ends(sub_YA, es=E(sub_YA), names=F)[,2]
edge.col <- V(sub_YA)$color[edge.end]

## Subplot of hub with its neighbors
plot(sub_YA, vertex.shape="circle", vertex.size = size, vertex.label = NA, edge.color = edge.col)
```

![](graphAnalyses_Hubs_Matlab_BCT_files/figure-markdown_github/force%20atlas2%20plot-6.png)
