# Load packages

``` r
library(here)
library(psych)
library(reshape2)
library(plyr)
library(tidyverse)
library(data.table)
library(scales)
library(lme4)
library(emmeans)
library(see)
```

# Load data

``` r
load(here::here('Behavioral/RData/all_subjects_edu.RData')) # This is the full behavioral df
load(here::here("Behavioral/RData/RT.RData")) # df with mean of RTs for each participant, condition, difficulty level
load(here::here('Behavioral/RData/df_perc.RData')) # This is a df with percentages per age group and SEMs for conditions for bar plots
```

# Linear mixed-effects models for accuracy and response time data

## GLMM (binomial mixed regression) for DV “Correct”

``` r
# Prepare df for model
all_subjects <- all_subjects_edu
all_subjects[is.na(all_subjects)] <- 0
all_subjects$Education_z <- scale(all_subjects$Education)
all_subjects$Difficulty <- factor(all_subjects$Difficulty, levels = c("Easy", "Difficult", "Counting_forw", "Counting_backw"))
all_subjects$Age <- as.factor(all_subjects$Age)
all_subjects$Subj <- as.factor(all_subjects$Subj)
all_subjects$Difficulty[all_subjects$Category == "Aufwaerts zaehlen von 1"] <- "Easy"
all_subjects$Difficulty[all_subjects$Category == "Abwaerts zaehlen von 9"] <- "Difficult"
all_subjects$Difficulty <- as.factor(all_subjects$Difficulty)
all_subjects <- droplevels(all_subjects)

# sum coding for categorical predictors
all_subjects <- mutate(all_subjects, AgeSum = Age)
contrasts(all_subjects$AgeSum) <- contr.sum(2)/2 
all_subjects <- mutate(all_subjects, ConditionSum = Condition)
contrasts(all_subjects$ConditionSum) <- contr.sum(2)/2

# model
m_acc <- glmer(Correct ~ AgeSum * ConditionSum + Education_z + (1|Subj) + (1|Category), family = binomial(link = "logit"), data = all_subjects, glmerControl(optimizer = "bobyqa")) # GLMM for age by condition
summary(m_acc)
```

    ## Generalized linear mixed model fit by maximum likelihood (Laplace
    ##   Approximation) [glmerMod]
    ##  Family: binomial  ( logit )
    ## Formula: Correct ~ AgeSum * ConditionSum + Education_z + (1 | Subj) +  
    ##     (1 | Category)
    ##    Data: all_subjects
    ## Control: glmerControl(optimizer = "bobyqa")
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##   5192.6   5247.8  -2589.3   5178.6    19703 
    ## 
    ## Scaled residuals: 
    ##      Min       1Q   Median       3Q      Max 
    ## -23.0528   0.0452   0.0679   0.2081   0.6175 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev.
    ##  Subj     (Intercept) 0.1886   0.4343  
    ##  Category (Intercept) 1.5201   1.2329  
    ## Number of obs: 19710, groups:  Subj, 31; Category, 22
    ## 
    ## Fixed effects:
    ##                       Estimate Std. Error z value             Pr(>|z|)    
    ## (Intercept)            4.61105    0.47653   9.676 < 0.0000000000000002 ***
    ## AgeSum1               -0.50194    0.21735  -2.309              0.02093 *  
    ## ConditionSum1         -3.05507    0.93802  -3.257              0.00113 ** 
    ## Education_z           -0.11040    0.06344  -1.740              0.08181 .  
    ## AgeSum1:ConditionSum1  0.74604    0.40979   1.821              0.06868 .  
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Correlation of Fixed Effects:
    ##             (Intr) AgeSm1 CndtS1 Edctn_
    ## AgeSum1     -0.058                     
    ## ConditinSm1 -0.805  0.060              
    ## Education_z  0.000  0.324  0.000       
    ## AgSm1:CndS1  0.063 -0.875 -0.062  0.000

``` r
# LRTs:
drop1(m_acc, test = "Chisq") # drop three-way interaction
```

    ## Single term deletions
    ## 
    ## Model:
    ## Correct ~ AgeSum * ConditionSum + Education_z + (1 | Subj) + 
    ##     (1 | Category)
    ##                     npar    AIC    LRT Pr(Chi)  
    ## <none>                   5192.6                 
    ## Education_z            1 5193.5 2.9695 0.08485 .
    ## AgeSum:ConditionSum    1 5193.9 3.3199 0.06844 .
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
m_acc_mainEffects <- glmer(Correct ~ AgeSum + ConditionSum + Education_z + (1|Subj) + (1|Category), family = binomial(link = "logit"), data = all_subjects, glmerControl(optimizer = "bobyqa")) # model with main effects
drop1(m_acc_mainEffects, test = "Chisq") # drop main effects
```

    ## Single term deletions
    ## 
    ## Model:
    ## Correct ~ AgeSum + ConditionSum + Education_z + (1 | Subj) + 
    ##     (1 | Category)
    ##              npar    AIC    LRT  Pr(Chi)   
    ## <none>            5193.9                   
    ## AgeSum          1 5194.1 2.1843 0.139425   
    ## ConditionSum    1 5199.9 8.0587 0.004529 **
    ## Education_z     1 5194.9 2.9720 0.084716 . 
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

## LMM for reaction times

``` r
all_subjects$Onset <- as.numeric(all_subjects$Onset)
df_onsets <- all_subjects[all_subjects$Onset > 0, ] # exclude rows with 0 because these should be NAs
df_onsets <- df_onsets[!is.na(df_onsets$Onset),] # exclude NAs
df_onsets <- df_onsets[df_onsets$Correct != 0,] # Remove incorrect answers 
df_onsets <- droplevels(df_onsets)

# Prepare df for model
df_onsets$Age <- as.factor(df_onsets$Age)
df_onsets$Subj <- as.factor(df_onsets$Subj)
df_onsets$Difficulty[df_onsets$Category == "Aufwaerts zaehlen von 1"] <- "Easy"
df_onsets$Difficulty[df_onsets$Category == "Abwaerts zaehlen von 9"] <- "Difficult"
df_onsets <- droplevels(df_onsets)
df_onsets$Education_z <- scale(df_onsets$Education)

# sum coding for categorical predictors
df_onsets <- mutate(df_onsets, AgeSum = Age)
contrasts(df_onsets$AgeSum) <- contr.sum(2)/2
df_onsets <- mutate(df_onsets, ConditionSum = Condition)
contrasts(df_onsets$ConditionSum) <- contr.sum(2)/2


# model
m_RT <- lmer(log(Onset) ~ AgeSum * ConditionSum + Education_z + (1|Subj) + (1|Category), data = df_onsets, lmerControl(optimizer = "bobyqa"), REML = F)
summary(m_RT)
```

    ## Linear mixed model fit by maximum likelihood  ['lmerMod']
    ## Formula: log(Onset) ~ AgeSum * ConditionSum + Education_z + (1 | Subj) +  
    ##     (1 | Category)
    ##    Data: df_onsets
    ## Control: lmerControl(optimizer = "bobyqa")
    ## 
    ##      AIC      BIC   logLik deviance df.resid 
    ##   8237.7   8300.5  -4110.8   8221.7    18869 
    ## 
    ## Scaled residuals: 
    ##      Min       1Q   Median       3Q      Max 
    ## -10.9726  -0.6457  -0.0908   0.5438   5.6506 
    ## 
    ## Random effects:
    ##  Groups   Name        Variance Std.Dev.
    ##  Subj     (Intercept) 0.007726 0.08790 
    ##  Category (Intercept) 0.002168 0.04657 
    ##  Residual             0.089654 0.29942 
    ## Number of obs: 18877, groups:  Subj, 31; Category, 22
    ## 
    ## Fixed effects:
    ##                        Estimate Std. Error t value
    ## (Intercept)            6.433073   0.023510 273.632
    ## AgeSum1                0.014037   0.006120   2.294
    ## ConditionSum1          0.147825   0.034811   4.247
    ## Education_z           -0.007078   0.003751  -1.887
    ## AgeSum1:ConditionSum1  0.078127   0.008740   8.939
    ## 
    ## Correlation of Fixed Effects:
    ##             (Intr) AgeSm1 CndtS1 Edctn_
    ## AgeSum1      0.012                     
    ## ConditinSm1 -0.596  0.001              
    ## Education_z  0.007  0.671  0.000       
    ## AgSm1:CndS1  0.000  0.029  0.007  0.001

``` r
# LRTs
drop1(m_RT, test = "Chisq") # drop three-way interaction
```

    ## Single term deletions
    ## 
    ## Model:
    ## log(Onset) ~ AgeSum * ConditionSum + Education_z + (1 | Subj) + 
    ##     (1 | Category)
    ##                     npar    AIC    LRT              Pr(Chi)    
    ## <none>                   8237.7                                
    ## Education_z            1 8239.3  3.560              0.05918 .  
    ## AgeSum:ConditionSum    1 8315.4 79.733 < 0.0000000000000002 ***
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
m_RT_mainEffects <- lmer(log(Onset) ~ AgeSum + ConditionSum + Education_z + (1|Subj) + (1|Category), data = df_onsets, REML = F, lmerControl(optimizer = "bobyqa")) # model with main effects
drop1(m_RT_mainEffects, test = "Chisq") # drop main effects
```

    ## Single term deletions
    ## 
    ## Model:
    ## log(Onset) ~ AgeSum + ConditionSum + Education_z + (1 | Subj) + 
    ##     (1 | Category)
    ##              npar    AIC     LRT  Pr(Chi)    
    ## <none>            8315.4                     
    ## AgeSum          1 8317.5  4.1176 0.042438 *  
    ## ConditionSum    1 8325.9 12.5183 0.000403 ***
    ## Education_z     1 8317.0  3.5623 0.059106 .  
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

``` r
# post-hoc tests
emmp <- emmeans(m_RT, pairwise ~ AgeSum|ConditionSum, adjust = "bonferroni", type = "response")
emmp
```

    ## $emmeans
    ## ConditionSum = Categories:
    ##  AgeSum response   SE  df asymp.LCL asymp.UCL
    ##  OA          688 13.5 Inf       662       715
    ##  YA          652 12.7 Inf       628       678
    ## 
    ## ConditionSum = Counting:
    ##  AgeSum response   SE  df asymp.LCL asymp.UCL
    ##  OA          571 21.0 Inf       531       613
    ##  YA          585 21.5 Inf       544       629
    ## 
    ## Degrees-of-freedom method: asymptotic 
    ## Confidence level used: 0.95 
    ## Intervals are back-transformed from the log scale 
    ## 
    ## $contrasts
    ## ConditionSum = Categories:
    ##  contrast ratio      SE  df null z.ratio p.value
    ##  OA / YA  1.055 0.00804 Inf    1   6.966  <.0001
    ## 
    ## ConditionSum = Counting:
    ##  contrast ratio      SE  df null z.ratio p.value
    ##  OA / YA  0.975 0.00723 Inf    1  -3.375  0.0007
    ## 
    ## Degrees-of-freedom method: asymptotic 
    ## Tests are performed on the log scale


# Plots for Accuracy - bar plots with individual data points on top

``` r
# Plot for accuracy in tasks in both age groups
accuracy_tasks <- 
  ggplot(data = df_perc, aes(x = Condition, y = Perc)) +
  geom_bar(aes(fill = Condition), stat = "identity") +
  geom_errorbar(aes(ymin = lowerE, ymax = upperE, width = 0.1)) +
  scale_fill_manual(values = c('grey35','grey75')) +
  labs(y = "Accuracy") +
  facet_wrap(~ Age, ncol = 2) +
  apatheme +
  theme(legend.position=("none"), 
        legend.title=element_blank(), 
        axis.title.x = element_blank(),
        strip.text.x = element_blank(), # This removes labels from facets
        axis.text.x = element_blank(),
        axis.ticks.x = element_blank(),
        aspect.ratio = 4/1)
accuracy_tasks
```

![](MDN_LANG_behavioral_CovEducation_files/figure-markdown_github/Plot%20for%20Accuracy-1.png)

# Plots for Response time - violin plots with boxplots

``` r
# Plot for RT in both tasks in both age groups
rt_tasks <- ggplot(data = RT, aes(x = Condition, y = Mean_onset, fill = Condition)) +
  geom_violinhalf(flip = 1, trim = F) +
  #geom_boxplot(data = RT, aes(x = Condition, y = Mean_onset, fill = Condition), width=0.3, color="grey55", alpha=0.1) +
  facet_wrap(~ Age) +
  ylab("Response time") +
  scale_fill_manual(values = c('grey35','grey75')) +
  apatheme +
  theme(legend.position=("none"),
        legend.title=element_blank(), 
        strip.text.x = element_blank(), # This removes labels from facets
        axis.title.x = element_blank(),
        axis.text.x = element_blank(),
        axis.ticks.x = element_blank())
rt_tasks
```

![](MDN_LANG_behavioral_CovEducation_files/figure-markdown_github/Response%20time-1.png)
