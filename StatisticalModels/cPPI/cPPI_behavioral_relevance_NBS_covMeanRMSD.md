# Load packages

``` r
library(psych)
library(reshape2)
library(plyr)
library(tidyverse)
library(RColorBrewer)
library(rstatix)
library(lme4)
library(mgsub)
library(ggeffects)
library(egg)
library(sjPlot)
library(ppcor)
library(ggpubr)
library(here)
```

# Load data

``` r
load(here("cPPI/RData/all_subjects_cPPI_wholeNets_7ROIs_confRMSD_indivMatrices_1df.RData")) # df with cPPI matrices
load(here::here("cPPI/RData/behav_df_confRMSD_03_2022.RData")) # df with in-scanner task behavior results
```

# Merge cPPI df with behavioral df for Accuracy and RT data

``` r
all_subjects <- all_subjects[-c(1:2)]
all_subjects <- all_subjects %>% 
  spread(networks, corr)

behav_df <- merge(df_categories, all_subjects, by = c("age", "sub")) 

connections <- colnames(behav_df)[17:37]
behav_df <- behav_df %>% 
  mutate_at(connections, scale, scale = F)
behav_df$Education <- scale(behav_df$Education, scale = F)
contrasts(behav_df$AgeSum) <- contr.sum(2)/2
behav_df$meanFD <- scale(behav_df$meanFD, scale = F)
behav_df$meanRMSD <- scale(behav_df$meanRMSD, scale = F)
```

## MMs only for Connections with NBS Age Effects - with motion RMS covariate

### GLMM Accuracy

``` r
behav_df_NBSAgeEffects <- behav_df[-c(17:20,23:33,35)]

alpha_bonferroni <- 0.05/5 # 5 significant connections from age differences

# create data frame to store results
results_accuracy_NBSAgeEffects <- data.frame()

# loop through the columns
for(var in names(behav_df_NBSAgeEffects)[c(17:length(behav_df_NBSAgeEffects))]){
        # dynamically generate formula
        fmla <- as.formula(paste("Correct ~", paste(var), "* AgeSum + Education + meanRMSD + (1|sub) + (1|Category)"))

        # fit glm model
        fit <- glmer(fmla, data = behav_df_NBSAgeEffects, family = binomial(link = "logit"))

        # get coefficents of fit
        cfit <- coef(summary(fit))

        assign("v1", get("var"))
        drop_int <- drop1(fit, test = "Chisq") # drop two-way interactions

        # create temporary data frame
        df <- data.frame(var = var, 
                         intercept = cfit[1], 
                         beta_network <- cfit[2],
                         beta_age <- cfit[3],
                         beta_edu <- cfit[4],
                         beta_rms <- cfit[5],
                         beta_network_age_int <- cfit[6],
                         std.error_intercept <- cfit[7],
                         std.error_network <- cfit[8],
                         std.error_age <- cfit[9],
                         std.error_edu <- cfit[10],
                         std.error_rms <- cfit[11],
                         std.error_network_age_int <- cfit[12],
                         z.value_intercept <- cfit[13],
                         z.value_network <- cfit[14],
                         z.value_age <- cfit[15],
                         z.value_edu <- cfit[16],
                         z.value_rms <- cfit[17],
                         z.value_network_age_int <- cfit[18],
                         p.value_intercept <- cfit[19],
                         p.value_network <- cfit[20],
                         p.value_age <- cfit[21],
                         p.value_edu <- cfit[22],
                         p.value_rms <- cfit[23],
                         p.value_network_age_int <- cfit[24],
                         AIC = AIC(fit),
                         Deviance = deviance(fit),
                         AIC_drop1_edu <- drop_int[2,2],
                         AIC_drop1_rms <- drop_int[3,2],
                         AIC_drop1_network_age_int <- drop_int[4,2],
                         LRT_drop1_edu <- drop_int[2,3],
                         LRT_drop1_rms <- drop_int[3,3],
                         LRT_drop1_network_age_int <- drop_int[4,3],
                         p.value_drop1_edu <- drop_int[2,4],
                         p.value_drop1_rms <- drop_int[3,4],
                         p.value_drop1_network_age_int <- drop_int[4,4],
                         p.value_drop1_network_age_int_corrected <- (p.value_drop1_network_age_int < alpha_bonferroni),
                         stringsAsFactors = F)

        # bind rows of temporary data frame to the results data frame
        results_accuracy_NBSAgeEffects <- rbind(results_accuracy_NBSAgeEffects, df)
        
        name_emms <- paste(v1, "emms", sep = "_")
        assign(name_emms, ggpredict(fit, terms = c(paste(v1, "[all]"), "AgeSum")))
}

p_holm <- p.adjust(results_accuracy_NBSAgeEffects$p.value_drop1_network_age_int....drop_int.4..4., method = "holm")
results_accuracy_NBSAgeEffects <- cbind(results_accuracy_NBSAgeEffects, p_holm)
```

#### Arrange plots

``` r
p1 <- plot(DMN_A_C__VAN_B_emms) +
        scale_fill_manual(values = c("#52257a", "#018571")) +
        scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
        coord_cartesian(ylim = c(0.7, 1)) +
        apatheme +
        ylab("Accuracy")

p2 <- plot(VAN_B__DAN_A_emms) +
        scale_fill_manual(values = c("#52257a", "#018571")) +
        scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
        coord_cartesian(ylim = c(0.7, 1)) +
        apatheme

## Arrange all accuracy plots in one plot
plot_Acc_cPPI <- egg::ggarrange(p1 +
            theme(legend.position = "none",
                  axis.title.x = element_blank()),
          p2 +
            theme(axis.text.y = element_blank(),
                  axis.ticks.y = element_blank(),
                  axis.title.y = element_blank(),
                  axis.title.x = element_blank(),
                  legend.position = "none"),
          nrow = 1)
```

![](cPPI_behavioral_relevance_NBS_covMeanRMSD_files/figure-markdown_github/Arrange%20plots%20in%20one%20plot%20-%20accuracy-1.png)

### LMM RT

``` r
# prepare df
behav_df.RT_NBSAgeEffects = behav_df_NBSAgeEffects[!is.na(behav_df_NBSAgeEffects$Onset),] # Remove NAs from Onset column and create df
behav_df.RT_NBSAgeEffects <- behav_df.RT_NBSAgeEffects[behav_df.RT_NBSAgeEffects$Correct != 0,] # Remove incorrect answers 
behav_df.RT_NBSAgeEffects <- droplevels(behav_df.RT_NBSAgeEffects) # drop unused factor levels
contrasts(behav_df.RT_NBSAgeEffects$AgeSum) <- contr.sum(2)/2 # re-apply sum coding to factor

# create data frame to store results
results_RT_NBSAgeEffects <- data.frame()

# loop through the columns
for(var in names(behav_df.RT_NBSAgeEffects)[c(17:length(behav_df.RT_NBSAgeEffects))]){
        # dynamically generate formula
        fmla <- as.formula(paste("log(Onset) ~", paste(var), "* AgeSum + Education + meanRMSD + (1|sub) + (1|Category)"))

        # fit glm model
        fit <- lmer(fmla, data = behav_df.RT_NBSAgeEffects, REML = F)
        
        # get coefficents of fit
        cfit <- coef(summary(fit))

        assign("v1", get("var"))
        drop_int <- drop1(fit, test = "Chisq") # drop two-way interactions

        # create temporary data frame
        df <- data.frame(var = var, 
                         intercept = cfit[1], 
                         beta_network <- cfit[2],
                         beta_age <- cfit[3],
                         beta_edu <- cfit[4],
                         beta_rms <- cfit[5],
                         beta_network_age_int <- cfit[6],
                         std.error_intercept <- cfit[7],
                         std.error_network <- cfit[8],
                         std.error_age <- cfit[9],
                         std.error_edu <- cfit[10],
                         std.error_rms <- cfit[11],
                         std.error_network_age_int <- cfit[12],
                         z.value_intercept <- cfit[13],
                         z.value_network <- cfit[14],
                         z.value_age <- cfit[15],
                         z.value_edu <- cfit[16],
                         z.value_rms <- cfit[17],
                         z.value_network_age_int <- cfit[18],
                         p.value_intercept <- cfit[19],
                         p.value_network <- cfit[20],
                         p.value_age <- cfit[21],
                         p.value_edu <- cfit[22],
                         p.value_rms <- cfit[23],
                         p.value_network_age_int <- cfit[24],
                         AIC = AIC(fit),
                         Deviance = deviance(fit),
                         AIC_drop1_edu <- drop_int[2,2],
                         AIC_drop1_rms <- drop_int[3,2],
                         AIC_drop1_network_age_int <- drop_int[4,2],
                         LRT_drop1_edu <- drop_int[2,3],
                         LRT_drop1_rms <- drop_int[3,3],
                         LRT_drop1_network_age_int <- drop_int[4,3],
                         p.value_drop1_edu <- drop_int[2,4],
                         p.value_drop1_rms <- drop_int[3,4],
                         p.value_drop1_network_age_int <- drop_int[4,4],
                         p.value_drop1_network_age_int_corrected <- (p.value_drop1_network_age_int < alpha_bonferroni),
                         stringsAsFactors = F)

        # bind rows of temporary data frame to the results data frame
        results_RT_NBSAgeEffects <- rbind(results_RT_NBSAgeEffects, df)
        
        name_emms <- paste(v1, "emms", sep = "_")
        assign(name_emms, ggpredict(fit, terms = c(paste(v1, "[all]"), "AgeSum")))
}

p_holm <- p.adjust(results_RT_NBSAgeEffects$p.value_drop1_network_age_int....drop_int.4..4., method = "holm")
results_RT_NBSAgeEffects <- cbind(results_RT_NBSAgeEffects, p_holm)
```

#### Arrange plots

``` r
p1 <- plot(DMN_A_C__VAN_B_emms) +
        scale_fill_manual(values = c("#52257a", "#018571")) +
        scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
        coord_cartesian(ylim = c(400, 1000)) +
        apatheme

p2 <- plot(Default_B__DAN_A_emms) +
        scale_fill_manual(values = c("#52257a", "#018571")) +
        scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
        coord_cartesian(ylim = c(400, 1000)) +
        apatheme

p3 <- plot(SemNet__VAN_B_emms) +
        scale_fill_manual(values = c("#52257a", "#018571")) +
        scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
        coord_cartesian(ylim = c(400, 1000)) +
        apatheme

p4 <- plot(VAN_B__DAN_A_emms) +
        scale_fill_manual(values = c("#52257a", "#018571")) +
        scale_colour_manual(name = "Age", labels = c("Older adults", "Young adults"), values = c("#52257a", "#018571")) +
        coord_cartesian(ylim = c(400, 1000)) +
        apatheme

## Arrange all accuracy plots in one plot
plot_RT_cPPI <- egg::ggarrange(p1 +
            theme(legend.position = "none",
                  axis.title.x = element_blank()),
          p2 +
            theme(axis.text.y = element_blank(),
                  axis.ticks.y = element_blank(),
                  axis.title.y = element_blank(),
                  axis.title.x = element_blank(),
                  legend.position = "none"),
          p3 +
            theme(axis.text.y = element_blank(),
                  axis.ticks.y = element_blank(),
                  axis.title.y = element_blank(),
                  axis.title.x = element_blank(),
                  legend.position = "none"),
          p4 +
            theme(axis.text.y = element_blank(),
                  axis.ticks.y = element_blank(),
                  axis.title.y = element_blank(),
                  axis.title.x = element_blank(),
                  legend.position = "none"),
          nrow = 1)
```

![](cPPI_behavioral_relevance_NBS_covMeanRMSD_files/figure-markdown_github/Arrange%20plots%20in%20one%20plot%20-%20RT-1.png)
