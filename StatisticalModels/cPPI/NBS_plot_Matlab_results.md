# Load packages

``` r
library(psych)
library(reshape2)
library(plyr)
library(tidyverse)
library(RColorBrewer)
library(rstatix)
library(circlize)
library(ComplexHeatmap)
library(here)
```

# Load data

``` r
# 7 ROIs
load(here("cPPI/RData/ROI_labels_wholeNets_7ROIs.RData"))
load(here("cPPI/RData/OA_cPPI_correlation_wholeNets_7ROIs_FisherTransform_confRMSD.RData"))
load(here("cPPI/RData/YA_cPPI_correlation_wholeNets_7ROIs_FisherTransform_confRMSD.RData"))
load(here("cPPI/RData/OA_NBS_res_7ROIs_t2.67_CovMeanRMSD.RData")) # with RMSD covariate --> no file for YA since no sig results

# 121 ROIs
load(here("cPPI/RData/ROI_labels_121ROIs_sorted.RData"))
load(here("cPPI/RData/OA_cPPI_correlation_121ROIs_FisherTransform_confRMSD.RData"))
load(here("cPPI/RData/YA_cPPI_correlation_121ROIs_FisherTransform_confRMSD.RData"))
load(here("cPPI/RData/OA_NBS_res_121ROIs_t2.67_covMeanRMSD.RData"))
load(here("cPPI/RData/YA_NBS_res_121ROIs_t2.67_covMeanRMSD.RData"))
```

# Plots

## Chord diagram NBS results - age group differences - 7 ROIs - adapted to show stronger decoupling for YA - covariate for MeanRMSD

``` r
#### Preparations for plot #### 
# Create df for corr values of both age groups
allGroups <- OA_7
allGroups[3,6] <- YA_7[3,6] # DMN A C - VAN B
allGroups[4,7] <- YA_7[4,7] # DMN B - DAN A

diag(allGroups) <- NA
allGroups[lower.tri(allGroups)] <- NA

names = c("Default A", "Control B", "Default A+C", "Default B", "Semantic", "VentAtt B", "DorsAtt A")
colnames(allGroups) <- names
rownames(allGroups) <- names

# Make matrix into long df
allGroups_long <- cor_gather(allGroups)

# Create age column
allGroups_long$age <- 'OA'
allGroups_long$age[allGroups_long$var1 == "Default A+C" & allGroups_long$var2 == "VentAtt B"] <- "YA"
allGroups_long$age[allGroups_long$var1 == "Default B" & allGroups_long$var2 == "DorsAtt A"] <- "YA"

# Create df for NBS results of both age groups
allGroups_NBS <- OA_NBS_res_7
allGroups_NBS[3,6] <- 1 # DMN A C - VAN B
allGroups_NBS[4,7] <- 1 # DMN B - DAN A

diag(allGroups_NBS) <- NA
allGroups_NBS[lower.tri(allGroups_NBS)] <- NA

colnames(allGroups_NBS) <- names
rownames(allGroups_NBS) <- names

# Make matrix into long df
allGroups_NBS_long <- cor_gather(allGroups_NBS)

# Create age column
allGroups_NBS_long$age <- 'OA'
allGroups_NBS_long$age[allGroups_NBS_long$var1 == "Default A+C" & allGroups_NBS_long$var2 == "VentAtt B"] <- "YA"
allGroups_NBS_long$age[allGroups_NBS_long$var1 == "Default B" & allGroups_NBS_long$var2 == "DorsAtt A"] <- "YA"


# Create df with combined network and age values for thresholding
allGroups_NBS_combined <- data.frame(from = paste(allGroups_NBS_long[[1]], allGroups_NBS_long[[4]], sep = "|"),
                 to = paste(allGroups_NBS_long[[2]], allGroups_NBS_long[[4]], sep = "|"),
                 value = allGroups_NBS_long[[3]], stringsAsFactors = FALSE)
allGroups_NBS_combined <- allGroups_NBS_combined[-c(2:11,14,16:18,20),]


# Assign colors for age groups and networks
all_age <- unique(allGroups_long[[4]])
color_age <- structure(c("#52257a", "#018571"), names = all_age)
color_networks <- "#CFCFCF"

# Create df grouped by networks
df2 <- data.frame(from = paste(allGroups_long[[1]], allGroups_long[[4]], sep = "|"),
                 to = paste(allGroups_long[[2]], allGroups_long[[4]], sep = "|"),
                 value = allGroups_long[[3]], stringsAsFactors = FALSE)

combined <- unique(data.frame(networks = c(allGroups_long[[1]], allGroups_long[[2]]), 
    age = c(allGroups_long[[4]], allGroups_long[[4]]), stringsAsFactors = FALSE))
combined <- combined[-c(3,4), ]

combined = combined[order(combined$networks, combined$age), ]
order = paste(combined$networks, combined$age, sep = "|")

df2 <- df2[(df2$from %in% order), ]
df2 <- df2[(df2$to %in% order), ]

comb <- paste(df2$from, df2$to, sep = "__")
comb2 <- paste(allGroups_NBS_combined$from, allGroups_NBS_combined$to, sep = "__")
connections2keep <- which(comb %in% comb2)
df2 <- df2[connections2keep, ]
df2[1,2] <- "Default A|OA"
df2[1,3] <- 0.15
df2[6,1] <- "DorsAtt A|OA"
df2[6,2] <- "VentAtt B|OA"

# Prepare colors and graphics for plot
grid.col = structure(color_age[combined$age], names = order)

col_fun = colorRamp2(c(-0.3, 0, 0.3), c("#396eab", "white", "#B72D2D"), transparency = 0.2)(df2[[3]])
col_fun[allGroups_NBS_combined[[3]] < 1] <- "#FFFFFF00" # threshold color matrix according to p values matrix from cPPI calculation

par(family = "sans")
order = c("Default A|OA", "Default A+C|YA", "Default B|OA", "Default B|YA", "Semantic|OA", "Control B|OA", "VentAtt B|OA", "VentAtt B|YA", "DorsAtt A|OA", "DorsAtt A|YA")

col_fun2 = colorRamp2(c(-0.4, 0, 0.45), c("#396eab", "white", "#B72D2D"), transparency = 0.2) # color function for legend
lgd = Legend(col_fun = col_fun2, title = "r", legend_height = unit(3, "cm"))


#### Plot ####

circos.par(start.degree = 180, gap.degree = 1.5)
chordDiagram(df2, order = order, annotationTrack = "grid",
    grid.col = grid.col, col = col_fun, 
    preAllocateTracks = list(
        track.height = 0.15,
        track.margin = c(0.05, 0)
    )
)
for(net in unique(combined$networks)) {
    l = combined$networks == net
    sn = paste(combined$networks[l], combined$age[l], sep = "|")
    highlight.sector(sn, track.index = 1, col = color_networks, facing = "bending.inside", text = net, cex = 1.45)
}
circos.clear()
 
legend("bottomright", pch = 15, col = color_age,
       legend = names(color_age), cex = 1, box.lty = 0)

draw(lgd, x = unit(2, "cm"), y = unit(1, "cm"), just = c("right", "bottom"))
```

![](NBS_plot_Matlab_results_files/figure-markdown_github/unnamed-chunk-1-1.png)

## Heatmap NBS results - age group differences - 121 ROIs

``` r
ROI_labels <- ROI_labels_sorted

#### Prepare df for plot ####
diag(OA) <- NA
OA_long <- cor_gather(OA)
OA_long$idx <- seq.int(nrow(OA_long))
OA_long$var1 <- factor(OA_long$var1, levels = ROI_labels[,4])
OA_long$var2 <- factor(OA_long$var2, levels = ROI_labels[,4])

diag(YA) <- NA
YA_long <- cor_gather(YA)
YA_long$idx <- seq.int(nrow(YA_long))
YA_long$var1 <- factor(YA_long$var1, levels = ROI_labels[,4])
YA_long$var2 <- factor(YA_long$var2, levels = ROI_labels[,4])

diag(OA_NBS_res) <- NA
OA_NBS_res_long <- cor_gather(OA_NBS_res)
OA_NBS_res_long$idx <- seq.int(nrow(OA_NBS_res_long))
OA_NBS_res_long$var1 <- factor(OA_NBS_res_long$var1, levels = ROI_labels[,4])
OA_NBS_res_long$var2 <- factor(OA_NBS_res_long$var2, levels = ROI_labels[,4])

diag(YA_NBS_res) <- NA
YA_NBS_res_long <- cor_gather(YA_NBS_res)
YA_NBS_res_long$idx <- seq.int(nrow(YA_NBS_res_long))
YA_NBS_res_long$var1 <- factor(YA_NBS_res_long$var1, levels = ROI_labels[,4])
YA_NBS_res_long$var2 <- factor(YA_NBS_res_long$var2, levels = ROI_labels[,4])


plot_df <- OA_long
plot_df$age <- NA
plot_df$cor <- ifelse(OA_NBS_res_long$cor == 1, plot_df$cor, 
                      ifelse(YA_NBS_res_long$cor == 1, YA_long$cor, NA))
plot_df$age <- ifelse(OA_NBS_res_long$cor == 1, "OA", 
                      ifelse(YA_NBS_res_long$cor == 1, "YA", NA))
plot_df$cor <- ifelse(plot_df$cor < 0 & plot_df$age == "OA", YA_long$cor, plot_df$cor)
plot_df$age <- ifelse(plot_df$cor < 0 & plot_df$age == "OA", "YA", plot_df$age)

#### Plot ####
na_subset = subset(plot_df, is.na(cor))

allSubs_grey <- ggplot(plot_df, aes(var1, var2)) +
  geom_tile(aes(fill = cor)) +
  geom_tile(aes(color = age == "OA"), fill = NA, size = 0.5) + 
  scale_fill_gradientn(colours = palet, values = scales::rescale(c(-0.2, 0, 0.6)), limits = c(-0.2, 0.6), breaks = c(-0.2, 0, 0.2, 0.4, 0.6), oob = scales::squish, name = "r", na.value = "white") +
  scale_color_manual(values = c("#018571", "#52257a"), na.value = NA, name = "Age", labels = c("YA", "OA")) +
  geom_tile(data = na_subset, aes(color = NA), linetype = 0, fill = "lightgrey", alpha = 0.5) +
  scale_x_discrete(labels = plot_df$idx) +
  scale_y_discrete(labels = plot_df$idx) + 
  apatheme +
  theme(
    axis.text.x = element_text(angle = 45, vjust = 1, hjust = 1, size = 7),
    axis.text.y = element_text(size = 7),
    axis.line = element_blank(),
    axis.ticks = element_blank(),
    axis.title = element_blank())
allSubs_grey
```

![](NBS_plot_Matlab_results_files/figure-markdown_github/Heatmap%20plot%20OA-1.png)
