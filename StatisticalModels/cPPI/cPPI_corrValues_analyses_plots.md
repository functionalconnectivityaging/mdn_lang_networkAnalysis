# Load packages

``` r
library(psych)
library(reshape2)
library(plyr)
library(tidyverse)
library(RColorBrewer)
library(rstatix)
library(circlize)
library(here)
```

# Load data

``` r
# Whole networks - 7 ROIs
load(here("cPPI/RData/OA_cPPI_correlation_wholeNets_7ROIs_FisherTransform_confRMSD.RData"))
load(here("cPPI/RData/YA_cPPI_correlation_wholeNets_7ROIs_FisherTransform_confRMSD.RData"))
load(here("cPPI/RData/OA_cPPI_pvalues_wholeNets_7ROIs_confRMSD.RData"))
load(here("cPPI/RData/YA_cPPI_pvalues_wholeNets_7ROIs_confRMSD.RData"))
 
# 121 ROIs
load(here("cPPI/RData/OA_cPPI_correlation_121ROIs_FisherTransform_confRMSD.RData"))
load(here("cPPI/RData/YA_cPPI_correlation_121ROIs_FisherTransform_confRMSD.RData"))
load(here("cPPI/RData/OA_cPPI_pvalues_121ROIs_confRMSD.RData"))
load(here("cPPI/RData/YA_cPPI_pvalues_121ROIs_confRMSD.RData"))
load(here("cPPI/RData/ROI_labels_121ROIs_sorted.RData"))
```

# Plots

## Heatmap OA

``` r
#### Prepare df for plot ####
diag(OA) <- NA
OA_long <- cor_gather(OA)
OA_long$idx <- seq.int(nrow(OA_long))
OA_long$var1 <- factor(OA_long$var1, levels = ROI_labels_sorted[,4])
OA_long$var2 <- factor(OA_long$var2, levels = ROI_labels_sorted[,4])

diag(OA_p) <- NA
OA_p_long <- cor_gather(OA_p)
OA_p_long$var1 <- factor(OA_p_long$var1, levels = ROI_labels_sorted[,4])
OA_p_long$var2 <- factor(OA_p_long$var2, levels = ROI_labels_sorted[,4])

## How many significant values? ##
length(OA_p_long[OA_p_long < 0.05])
```

    ## Warning in Ops.factor(left, right): '<' not meaningful for factors

    ## Warning in Ops.factor(left, right): '<' not meaningful for factors

    ## [1] 414

``` r
#### Plot ####

OA_plot <- ggplot(OA_long, aes(var1, var2)) +
  geom_tile(aes(fill = cor)) +
  geom_tile(aes(color = OA_p_long$cor < 0.05), fill = NA, size = 0.5) + 
  scale_fill_gradientn(colours = palet, values = scales::rescale(c(-0.2, 0, 0.6)), limits = c(-0.2, 0.6), breaks = c(-0.2, 0, 0.2, 0.4, 0.6), oob = scales::squish, name = "r", na.value = "white") +
  scale_color_manual(guide = "none", values = c(NA, "black"), na.value = NA) +
  scale_x_discrete(labels = OA_long$idx) +
  scale_y_discrete(labels = OA_long$idx) + 
  apatheme +
  theme(
    axis.text.x = element_text(angle = 45, vjust = 1, hjust = 1, size = 7),
    axis.text.y = element_text(size = 7),
    axis.line = element_blank(),
    axis.ticks = element_blank(),
    axis.title = element_blank())
OA_plot
```

![](cPPI_corrValues_analyses_plots_files/figure-markdown_github/Heatmap%20plot%20OA-1.png)

## Heatmap YA

``` r
#### Prepare df for plot ####
diag(YA) <- NA
YA_long <- cor_gather(YA)
YA_long$idx <- seq.int(nrow(YA_long))
YA_long$var1 <- factor(YA_long$var1, levels = ROI_labels_sorted[,4])
YA_long$var2 <- factor(YA_long$var2, levels = ROI_labels_sorted[,4])

diag(YA_p) <- NA
YA_p_long <- cor_gather(YA_p)
YA_p_long$var1 <- factor(YA_p_long$var1, levels = ROI_labels_sorted[,4])
YA_p_long$var2 <- factor(YA_p_long$var2, levels = ROI_labels_sorted[,4])

## How many significant values? ##
length(YA_p_long[YA_p_long < 0.05])
```

    ## Warning in Ops.factor(left, right): '<' not meaningful for factors

    ## Warning in Ops.factor(left, right): '<' not meaningful for factors

    ## [1] 1560

``` r
#### Plot ####

YA_plot <- ggplot(YA_long, aes(var1, var2)) +
  geom_tile(aes(fill = cor)) +
  geom_tile(aes(color = YA_p_long$cor < 0.05), fill = NA, size = 0.5) + 
  scale_fill_gradientn(colours = palet, values = scales::rescale(c(-0.2, 0, 0.6)), limits = c(-0.2, 0.6), breaks = c(-0.2, 0, 0.2, 0.4, 0.6), oob = scales::squish, name = "r", na.value = "white") +
  scale_color_manual(guide = "none", values = c(NA, "black"), na.value = NA) +
  scale_x_discrete(labels = YA_long$idx) +
  scale_y_discrete(labels = YA_long$idx) + 
  apatheme +
  theme(
    #axis.text = element_blank(),
    axis.text.x = element_text(angle = 45, vjust = 1, hjust = 1, size = 7),
    axis.text.y = element_text(size = 7),
    axis.line = element_blank(),
    axis.ticks = element_blank(),
    axis.title = element_blank())
YA_plot
```

![](cPPI_corrValues_analyses_plots_files/figure-markdown_github/unnamed-chunk-1-1.png)

## Chord diagram OA - 7 ROIs

``` r
OA_chord <- OA_7 # Copy df for plot
diag(OA_chord) <- NA # Make diagonal NA
OA_chord <- as.matrix(OA_chord) # transform

names = c("Default A", "Control B", "Default A+C", "Default B", "Semantic", "VentAtt B", "DorsAtt A")
colnames(OA_chord) <- names
rownames(OA_chord) <- names

diag(OA_p_7) <- NA # Make diagonal NA in p values df
OA_p_7 <- as.matrix(OA_p_7) # transform


###### Colors for Chord Diagram ########
# Grid color
grid_col = "#CFCFCF"

# Links color
col_fun = colorRamp2(c(-0.3, 0, 0.3), c("#396eab", "white", "#B72D2D"), transparency = 0.2)(OA_chord) # create palette with blue for negative values and red for positive values
col_fun[OA_p_7 > 0.05] <- "#FFFFFF00" # threshold color matrix according to p values matrix from cPPI calculation

##### Plot #####
circos.par(start.degree = 180, gap.degree = 1.5)
chordDiagram(OA_chord, symmetric = T, annotationTrack = "grid", annotationTrackHeight = 0.15, grid.col = grid_col, col = col_fun, order = c("Default A", "Default A+C", "Default B", "Semantic", "Control B", "VentAtt B", "DorsAtt A")) #order = c("DMN A", "DMN A+C", "DMN B", "Semantic", "Control B", "VAN B", "DAN A")

for(si in get.all.sector.index()) 
  {
  xlim = get.cell.meta.data("xlim", sector.index = si, track.index = 1)
  ylim = get.cell.meta.data("ylim", sector.index = si, track.index = 1)
  circos.text(mean(xlim), mean(ylim), si, sector.index = si, track.index = 1,
  facing = "bending.inside", col = "black", cex = 1.45)
}
```

    ## Note: 2 points are out of plotting region in sector 'DorsAtt A', track
    ## '1'.

![](cPPI_corrValues_analyses_plots_files/figure-markdown_github/Chord%20diagram%20OA%20-%207%20ROIs-1.png)

``` r
circos.clear()
```

## Chord diagram YA - 7 ROIs

``` r
YA_chord <- YA_7 # Copy df for plot
diag(YA_chord) <- NA # Make diagonal NA
YA_chord <- as.matrix(YA_chord) # transform

names = c("Default A", "Control B", "Default A+C", "Default B", "Semantic", "VentAtt B", "DorsAtt A")
colnames(YA_chord) <- names
rownames(YA_chord) <- names

diag(YA_p_7) <- NA # Make diagonal NA in p values df
YA_p_7 <- as.matrix(YA_p_7) # transform


###### Colors for Chord Diagram ########
# Grid color
grid_col = "#CFCFCF"

# Links color
col_fun_YA = colorRamp2(c(-0.3, 0, 0.3), c("#396eab", "white", "#B72D2D"), transparency = 0.2)(YA_chord) # create palette with blue for negative values and red for positive values
col_fun_YA[YA_p_7 > 0.05] <- "#FFFFFF00" # threshold color matrix according to p values matrix from cPPI calculation

##### Plot #####
circos.par(start.degree = 180, gap.degree = 1.5)
chordDiagram(YA_chord, symmetric = T, annotationTrack = "grid", annotationTrackHeight = 0.15, grid.col = grid_col, col = col_fun_YA, order = c("Default A", "Default A+C", "Default B", "Semantic", "Control B", "VentAtt B", "DorsAtt A"))
#chordDiagram(df, symmetric = T, annotationTrack = "grid", annotationTrackHeight = 0.15)
for(si in get.all.sector.index()) 
  {
  xlim = get.cell.meta.data("xlim", sector.index = si, track.index = 1)
  ylim = get.cell.meta.data("ylim", sector.index = si, track.index = 1)
  circos.text(mean(xlim), mean(ylim), si, sector.index = si, track.index = 1,
  facing = "bending.inside", col = "black", cex = 1.45)
}
```

![](cPPI_corrValues_analyses_plots_files/figure-markdown_github/unnamed-chunk-2-1.png)

``` r
circos.clear()
```
