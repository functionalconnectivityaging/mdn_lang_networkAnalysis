## Content of folder Statistical Models

* Behavioral  
Contains script and RData for analyses of behavioral results for in-scanner task. See [https://gitlab.gwdg.de/functionalconnectivityaging/mdn_lang/-/tree/master/04_stats/Behavioral](https://gitlab.gwdg.de/functionalconnectivityaging/mdn_lang/-/tree/master/04_stats/Behavioral) for raw data.

* cPPI  
Contains scripts and raw data for analyses of cPPI results. Age differences were investigated via Network-Based Statistics (NBS) Toolbox. Behavioral relevance was only analyzed for those cPPI results that showed significant age difference according to NBS.

* Graph Measures  
Contains scripts and raw data for graph-theoretical analyses. Results for orthogonal minumum spanning tree (OMST) and normalized participation coefficient were extracted from Matlab. Segregation was calculated on unthresholded and weighted connectivity matrices. Efficiency and normalized participation coefficient were calculated on OMST. 
